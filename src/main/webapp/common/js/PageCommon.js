function goMoveList(cpage , rowsPerPage) {
	if(pagingFieldData!=null && pagingFieldData.length > 0 ) {
		for(var i=0;i<pagingFieldData.length;i++)
		{
			var inputTag = document.createElement("INPUT");
			inputTag.type = "hidden";
			inputTag.name=pagingFieldData[i].fieldName;
			inputTag.value=pagingFieldData[i].fieldValue;
			document.pageNaviForm.appendChild(inputTag);
		}
	}
	document.pageNaviForm.currentPage.value = cpage;
	if(rowsPerPage != null) {
		document.pageNaviForm.rowsPerPage.value = parseInt(rowsPerPage);
	}

	 try {
	 	if(pagingProperty.urlPath != null){
	 		document.pageNaviForm.action = pagingProperty.urlPath;
	 	}
	 } catch(e) {
	 	document.pageNaviForm.action = pagingProperty.searchForm.action;
	 }

	if(pagingProperty.modalName!=null) {
		document.pageNaviForm.target = pagingProperty.modalName;
	}
	document.pageNaviForm.submit();
}

function goMoveListPopUp(cpage , rowsPerPage) {

	document.pageNaviForm.currentPage.value = cpage;
	if(rowsPerPage != null) {
		document.pageNaviForm.rowsPerPage.value = parseInt(rowsPerPage);
	}
	try {
		if(pagingProperty.urlPath != null){
			document.pageNaviForm.action = pagingProperty.urlPath;
		}
	} catch(e) {
		document.pageNaviForm.action = pagingProperty.searchForm.action;
	}


	 document.pageNaviForm.target = window.name;

	 document.pageNaviForm.submit();
}
