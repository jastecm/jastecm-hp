function LPAD(s, c, n) {    
    if (! s || ! c || s.length >= n) {
        return s;
    }
 
    var max = (n - s.length)/c.length;
    for (var i = 0; i < max; i++) {
        s = c + s;
    }
 
    return s;
}
 
function RPAD(s, c, n) {  
    if (! s || ! c || s.length >= n) {
        return s;
    }
 
    var max = (n - s.length)/c.length;
    for (var i = 0; i < max; i++) {
        s += c;
    }
 
    return s;
}

function replaceAll(str, searchStr, replaceStr) {
    return str.split(searchStr).join(replaceStr);
}
function replaceNumber(str) {
    return replaceAll(str,/[^0-9]/g,'');
}
/*
 * d date 객체 or 숫자8자리 or 문자열10자리(yyyy/mm/dd , yyyy-mm-dd)
 * ex) d = new Date(d); 
 * ex) d = new Number(i) 
 * ex) d = new String('string')
 * 
 * calcType - d:일 , m:월
 * v - 증감일
 * 
 * returnType - instance Date,Number,String(구분자)[String("/") = yyyy/mm/dd]
 */
function calcDate(d , calcType , v , returnType){
	var c;
	
	if(d instanceof Date){
		c = d;
	}else if(d instanceof Number){
		d = new String(d);
		c = new Date(d.substring(0,4),Number(d.substring(4,6))-1,Number(d.substring(6,8)));
	}else if(d instanceof String){
		var d = replaceNumber(d);
		c = new Date(d.substring(0,4),Number(d.substring(4,6))-1,Number(d.substring(6,8)));
	}
	
	if(calcType == "d") c.setDate(c.getDate()+v);
	if(calcType == "m") c.setMonth(c.getMonth()+v);
	
	if(returnType instanceof Date){
		return c;
	}else if(returnType instanceof Number){
		var y = c.getFullYear();
		var m = LPAD(Number(c.getMonth()+1)+"",'0',2);
		var d = LPAD(Number(c.getDate())+"",'0',2);
		return new Number(y+""+m+""+d);
	}else if(returnType instanceof String){
		var sep = returnType;
		var y = c.getFullYear();
		var m = LPAD(Number(c.getMonth()+1)+"",'0',2);
		var d = LPAD(Number(c.getDate())+"",'0',2);
		return new String(y+sep+m+sep+d);
	}
	
}
 
//3자리마다 콤마를 찍어줌
function number_format(data) {
 data = new String(data);
 exp_data = data.split('.');
 tmp = '';
 number = '';
 cutlen = 3;
 comma = ',';
 len = exp_data[0].length;
 mod = (len % cutlen);
 k = cutlen - mod;
 for (NF_i=0; NF_i<len; NF_i++) {
  number = number + data.charAt(NF_i);
  if (NF_i < len - 1) {
   k++;
   if ((k % cutlen) == 0) {
    number = number + comma;
    k = 0;
   }
  }
 }
 if (exp_data[1] != undefined) number += '.' + exp_data[1];
 return number;
}

function createNumonlyInput($el){
	$el.keydown(function (e) {
	    // Allow: backspace, delete, tab, escape, enter and .
	    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	         // Allow: Ctrl+A
	        (e.keyCode == 65 && e.ctrlKey === true) ||
	         // Allow: Ctrl+C
	        (e.keyCode == 67 && e.ctrlKey === true) ||
	         // Allow: Ctrl+X
	        (e.keyCode == 88 && e.ctrlKey === true) ||
	         // Allow: home, end, left, right
	        (e.keyCode >= 35 && e.keyCode <= 39)) {
	             // let it happen, don't do anything
	             return;
	    }
	    // Ensure that it is a number and stop the keypress
	    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	        e.preventDefault();
	    }
	});
}
