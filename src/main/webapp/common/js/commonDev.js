
$(function(){
	$(document).ready(function(){
		$('.sel').sSelect({ddMaxHeight: '300px'});
		
		$(".callPage").on("click",function(){
			var p = $(this).data("page");
			var url = "/index.do";
			$JBIZ.instance_post(url,{callPage:p});						
		});
		
		$(".callPageSubmit").on("click",function(){
			var p = $(this).data("page");
			var f = $(this).data("form");
			var frmParam = $("#"+f).serializeArray();
			var param = {};
			for(var i= 0 ; i < frmParam.length ; i++){
				param[frmParam[i].name] = frmParam[i].value; 
			}
			
			param.callPage = p;
			var url = "/index.do";
			$JBIZ.instance_post(url,param);
		});
		
		$(".ichk").on('ifCreated ifClicked ifChanged ifChecked ifUnchecked ifDisabled ifEnabled ifDestroyed', function(event){
			
			if(event.type == "ifChecked") $(this).attr("checked",true);
			
			else if(event.type == "ifUnchecked") $(this).attr("checked",false);
			
        }).iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
		
	});
		
});


$(window).load(function(e) {
	
});

$(window).resize(function(e) {
	
});


$(window).scroll(function(e) {
	
});







function ajaxCreateSelect(url,param,target,listType,defaultVal,initVal,afterFunk){
	if(typeof afterFunk == "undefined"){
		afterFunk = function(){};
	}
	
	$.ajax({
		type : 'POST',
		dataType : 'json',
		url : url,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		data : param,
		success: function(result){
			if(listType) createSingleSelect(target,result.result,defaultVal,initVal,afterFunk);
			else createCodeValueSelect(target,result.result,defaultVal,initVal,afterFunk);
		},
		error:function(result){
		}
	});
}

function createSingleSelect(target,valList,defaultVal,initVal,afterFunk){
	$("#"+target).html("");
	
	var strHtml = "";
	
	if(defaultVal == "A"){
		strHtml += "<option value=''>전체</option>";				
	}else if(defaultVal == "S"){
		strHtml += "<option value=''>선택</option>";				
	}
	
	for(var i = 0 ; i < valList.length ; i++){
		if(initVal == valList[i]) strHtml += "<option value='"+valList[i]+"' selected>"+valList[i]+"</option>";
		else strHtml += "<option value='"+valList[i]+"'>"+valList[i]+"</option>";
	}
	
	$("#"+target).html(strHtml);
	if(typeof afterFunk != "undefined"){
		afterFunk();
		
	}
	//$("#"+target).trigger("change");
	$("#"+target).getSetSSValue(initVal);
	
}

function createCodeValueSelect(target,valList,defaultVal,initVal,afterFunk){
	$("#"+target).html("");
	
	var strHtml = "";
	
	if(defaultVal == "A"){
		strHtml += "<option value=''>전체</option>";				
	}else if(defaultVal == "S"){
		strHtml += "<option value=''>선택</option>";				
	}else if(defaultval){
		strHtml += "<option value=''>"+defaultval+"</option>";
	}
	
	for(var i = 0 ; i < valList.length ; i++){
		if(initVal == valList[i]) strHtml += "<option value='"+valList[i].code+"' selected>"+valList[i].codeName+"</option>";
		else strHtml += "<option value='"+valList[i].code+"'>"+valList[i].codeName+"</option>";
	}
	
	$("#"+target).html(strHtml);
	
	if(typeof afterFunk != "undefined"){
		afterFunk();
		
	}
	//$("#"+target).trigger("change");
	$("#"+target).getSetSSValue(initVal);
	
}

var $JBIZ = {};
(function(a){
	a.path = _defaultPath;
	a.http_post = function(u,d,c){
		$.ajax({
			url:a.path+u
			,dataType : 'json'
			,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
			,type:"POST"
			,data:d
			,cache:!1
			,success:function(t){null!=c&&c(t)}
			,error:function(t){alert("ajax error")}
		});
	}
	
	a.http_get = function(u,c){
		$.ajax({
			url:a.path+u
			,dataType : 'json'
			,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
			,type:"GET"
			,cache:!1
			,success:function(t){null!=c&&c(t)}
			,error:function(t){alert("ajax error")}
		});
	}
	
	a.submit_post = function(u,f){
		f.attr("action",a.path+u);
		f.attr("method","POST");
		f.submit();	
	}
	
	a.instance_post = function(u,p,o){
		var option = "";
		if(o) option = o;
		$("body").append("<form id='instanceFrm' "+option+"></form>");
		for (key in p) {
			var strHtml = '<input type="text" name="'+key+'" value="'+p[key]+'" >';
			$("#instanceFrm").append(strHtml);
		}
		$("#instanceFrm").attr("action",a.path+u);
		$("#instanceFrm").attr("method","POST");
		$("#instanceFrm").submit();	
		$("#instanceFrm").remove();
	}
	
	a.instance_post2 = function(u,p){
		$("body").append("<form id='instanceFrm2'></form>");
		for (key in p) {
			var strHtml = '<input type="text" name="'+key+'" value="'+p[key]+'" >';
			$("#instanceFrm2").append(strHtml);
		}
		$("#instanceFrm2").attr("action",a.path+u);
		$("#instanceFrm2").attr("method","POST");
		$("#instanceFrm2").submit();	
		$("#instanceFrm2").remove();
	}
})($JBIZ);


