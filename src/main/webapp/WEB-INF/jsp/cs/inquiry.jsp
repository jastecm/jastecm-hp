<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script src='${pageContext.request.contextPath}/common/js/jquery.MultiFile.js' type="text/javascript" language="javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM08");
	initMenuSel("LM08_02"); 
	
	$('#frm').ajaxForm({
		beforeSubmit: function (data,form,option) {
			return true;
		},
		success: function(res,status){
	    	
			var rtvCd = res.rtvCd;
			if(rtvCd == "0"){
				alert("error");
			}else{
				var url = "/index.do";
				res.rtvMap.callPage = "/cs/inquiryReceipt";
				$JBIZ.instance_post(url,res.rtvMap);
			}
		},
		error: function(){
	    	alert("error");
		}                               
	});
	
	$("#frm").submit(function(){return false});
	
	$("#btn_send").on("click",function(){
		if(!($("#input-1").is(":checked"))){ 
			alert("개인정보 수집 이용안내에 동의해 주세요.");
			return false;
		}
		$('#frm').submit();
	});
});
</script>

<div class="sub_layout"> 
		<div class="sub_title1">
			<span></span>
			1:1 이용문의 
		</div>
		<div class="sub_title4">
			자주하는 질문을 통해 찾지 못한 궁금한사항에대해 문의해주세요.<br>
			친절하고 빠르게 답변드리겠습니다.
		</div>
		<div class="sub06_box1">
			<p>1:1 문의 전에 자주하는 질문에서 궁금한 사항을 검색해보시면 좀 더 빠른 해결이 가능합니다.</p>
			<p>입력하신 개인정보는 본인확인 및 답변 전송 용도로 사용됩니다.</p>
		</div> 
		<form id="frm" action="${pageContext.request.contextPath}/com/fileUpload.do" method="post" enctype="multipart/form-data">		
		<div class="sub_tit1">문의내용</div>
		<table class="table2 mab76">
			<colgroup>
				<col width="73" />
				<col width="345" />
				<col width="52" />
				<col width="*" />
			</colgroup>
			<tbody>
				<tr>
					<th>이름</th>
					<td><input type="text" class="w316" name="name" placeholder="문의자분의 이름을 입력해주세요." /></td>
					<th>이메일</th>
					<td><input type="text" class="w331" name="email" placeholder="답변받으실 이메일을 입력해주세요." /></td>
				</tr>
				<tr>
					<th>제목(필수)</th>
					<td colspan="3"><input type="text" class="w728" name="title" placeholder="제목을 입력해주세요." /></td>
				</tr>
				<tr>
					<th>내용(필수)</th>
					<td colspan="3"> 
						<textarea name="contents"></textarea>
					</td>
				</tr>
				<tr>
					<th>첨부파일</th>
					<td colspan="3">
						<div class="btn_file"><input type="file" name="file" class="multi" maxlength="3"/></div>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="sub_tit1">개인정보 수집 및 이용에 대한 안내</div>
		<div class="sub06_box2">
			자스텍M은 1:1문의 답변에 대한 개인정보를 수집하고 있습니다.<br>
			1. 수집 개인정보 항목 : [필수] 이름, 이메일<br>
			2. 개인정보의 수집 및 이용목적 : 서비스이용 문의에 따른 본인확인 및 답변 경로 확보<br>
			3. 개인정보의 이용기간 : 모든 답변이 완료된 후 해당정보를 지체없이 파기합니다.<br>
			4. 동의 거부권리 안내 추가 : 위와 같은 개인정보 수집 동의를 거부할 수 있습니다. 다만 동의를 거부하는 경우 문의답변이 어려울수 있습니다.<br>
			그 밖의 사항은 각 사별 개인정보처리방침을 준수합니다.
		</div>
		<div class="agree_box demo-list mab63">
			<input tabindex="1" type="checkbox" id="input-1"><label for="input-1">개인정보 수집 및 이용에 동의합니다.</label>
		</div>
		</form> 
        
     	<div class="btn_area1">
			<a href="${pageContext.request.contextPath}/cs/inquiry.do" class="callPage">취소</a>
			<a href="#none" id="btn_send">접수하기</a>
		</div>
	</div>
	