<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM08");
	initMenuSel("LM08_01");
});

var pageUrl =  "/cs/faq";
$(document).ready(function(){
	/*
	$(".menu").on("click",function(){
	
		$(".tabbable").find(".f").hide();
	 	
		if($(this).data("menu")=="#total"){
	 		$(".tabbable").find(".f").show();
	 	}
			
		if($(this).data("menu")=="#IoT"){
			 $('tr[id=IoT]').show();
		}
			
		if($(this).data("menu")=="#service"){
			 $('tr[id=service]').show();
		}
		
		if($(this).data("menu")=="#solution")	{
			 $('tr[id=solution]').show();
		}
			 
	});
	*/
	$(".menu").each(function(){
		if($(this).data("menu") == "${reqVo.searchContentType}") $(this).addClass("on");
	})
	
		
	$("#btn_search").on("click",function(){
		var p = pageUrl;
		var f = "frm";
		var frmParam = $("#"+f).serializeArray();
		var param = {};
		for(var i= 0 ; i < frmParam.length ; i++){
			param[frmParam[i].name] = frmParam[i].value; 
		}
		
		param.callPage = p;
		var url = "/index.do";
		$JBIZ.instance_post(url,param);
	});
	
	$(".menu").on("click",function(){
		var menu = $(this).data("menu");
		$("#frm input[name=searchContentType]").val(menu);
		
		$("#btn_search").trigger("click");		
	});
});





</script>

<div class="sub_layout">


	<div class="sub_title1">
		<span></span>
		자주하는 질문
	</div>
	<div class="sub_title3">
		자주하시는 질문에 대한 답변을 모아두었습니다. 
	</div>
	
	<form id="frm">
		<input type="hidden" name="searchBoardType" value="1" />
		<input type="hidden" name="searchContentType" value="" />
		<div class="search_box">
			<input type="text" name="searchText" placeholder="검색으로 빠르게 궁금한 점을 해결해보세요." value="${reqVo.searchText}"/><button id="btn_search">검색</button>
		</div>
	</form>
	
	<div class="tabbable"> 
		<ul class="tabs">
			<div class="sub_tab">
				<a data-menu="" class="menu" style="width: 198px">전체</a><a data-menu="1" class="menu" style="width: 198px">자동차 IoT 제품</a><a data-menu="2" class="menu" style="width: 198px">솔루션</a><a data-menu="3" class="menu" style="width: 198px">서비스</a>
			</div>
		</ul>
		<table id="table" class="table1 table1_faq">
			<colgroup>
				<col width="139" />
				<col width="*" />
			</colgroup>
			<thead>
				<tr>
					<th>분류</th>
					<th>질문</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${fn:length(rtvList) eq 0}">
					<tr class="f">
						<td colspan="2">검색결과가 없습니다.</td>
					</tr>
				</c:if>
				<c:if test="${fn:length(rtvList) > 0 }">
					<c:forEach var="vo" items="${rtvList}" varStatus="i">
						<tr class="f">
							<th>${vo.contentTypeNm }</th>
							<td>${vo.title }</td>
						</tr>
						<tr class="a">
							<th></th>
							<td>
								${vo.contents }
							</td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
		<script type="text/javascript">	
		var pagingFieldData = new Array (
				{fieldName:"searchBoardType",fieldValue:"${reqVo.searchBoardType}"}
				,{fieldName:"searchContentType",fieldValue:"${reqVo.searchContentType}"}
				,{fieldName:"searchText",fieldValue:"${reqVo.searchText}"}
				,{fieldName:"callPage",fieldValue:pageUrl}
		);
		var pagingProperty = {
			urlPath: "${pageContext.request.contextPath}/index.do" 	//required
		}	
		</script>
		<%@ include file="/WEB-INF/jsp/common/PagingNavi.jsp"%>
	</div>
</div>