<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM08");
	initMenuSel("LM08_03");
});
var pageUrl = "/cs/download";
$(document).ready(function(){
/* $(document).ready(function(){
	$(".menu").on("click",function(){
	 
		$(".tabbable").find(".f").hide();
	 	
			
		if($(this).data("menu")=="#da"){
			 $('tr[id=da]').show();
		}
		if($(this).data("menu")=="#ca"){
			 $('tr[id=ca]').show();
		}
				
		
			 
	});
}); */

	$(".menu").each(function(){
		if($(this).data("menu") == "${reqVo.searchContentType}") $(this).addClass("on");
	})
	
		
	$("#btn_search").on("click",function(){
		var p = pageUrl;
		var f = "frm";
		var frmParam = $("#"+f).serializeArray();
		var param = {};
		for(var i= 0 ; i < frmParam.length ; i++){
			param[frmParam[i].name] = frmParam[i].value; 
		}
		
		param.callPage = p;
		var url = "/index.do";
		$JBIZ.instance_post(url,param);
	});
	
	$(".menu").on("click",function(){
		var menu = $(this).data("menu");
		$("#frm input[name=searchContentType]").val(menu);
		
		$("#btn_search").trigger("click");		
	});
});



</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			다운로드 자료실 
		</div>
		<div class="sub_title6">
			<span>스마트폰 어플 다운로드</span><strong></strong>
		</div>
		<div class="sub06_box3">
			<img src="${pageContext.request.contextPath}/common/images/sub06_img1.jpg" alt="" class="img" />
			<h1>스마트 드라이빙 뷰카 VIEWCAR</h1>
			<p>앱 마켓 검색창에서 ‘뷰카’를 검색하거나 아래 원하시는 앱 마켓의 버튼을 클릭하여 앱을 다운받으실 수 있습니다.</p>
			<div class="btn">
				<a href="https://play.google.com/store/apps/details?id=kr.co.infinityplus.viewcar&hl=ko"><img src="${pageContext.request.contextPath}/common/images/sub06_btn1.jpg" alt="" /></a>
				<a href="https://www.tstore.co.kr/userpoc/main"><img src="${pageContext.request.contextPath}/common/images/sub06_btn2.jpg" alt="" /></a>
			</div>
		</div>
		<div class="sub_title6">
			<span>자료 다운로드</span><strong></strong>
		</div>
<div class="tabbable"> 
	<ul class="tabs">
		<div class="sub_tab">
			<a data-menu="1" class="menu">자료</a><a data-menu="2" class="menu">카다로그&브로셔</a>
		</div>
	</ul>
		<table id="table" class="table1 table1_faq">
			<colgroup>
				<col width="80" />
				<col width="*" />
				<col width="100" />
				<col width="100" />
			</colgroup>
			<thead>
				<tr>
					<th>분류</th>
					<th>제목</th>
					<th>작성일</th>
					<th>다운로드</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${fn:length(rtvList) eq 0}">
				<tr class="f">
						<td colspan="2" style='text-align:center'>검색결과가 없습니다.</td>
				</tr>
				</c:if>	
				<c:if test="${fn:length(rtvList) > 0 }">
					<c:forEach var="vo" items="${rtvList}" varStatus="i">
						<tr class="f">
							<th>${vo.contentTypeNm }</th>
							<td style='text-align:center'>${vo.title }</td>
							<th><fmt:parseDate var="d" value="${vo.regDate }" pattern="yyyy-MM-dd HH:mm:ss.S"/>
							<fmt:formatDate value="${d}" pattern="yyyy-MM-dd"/></th>
							<th><a href="#"><img src="${pageContext.request.contextPath}/common/images/btn_down.jpg" alt="" /></a></th>
						</tr>
						<tr class="a">
							<th></th>
							<td style='text-align:center'>
								${vo.contents }
							</td>
						</tr>
					</c:forEach>
				</c:if>	
					
			</tbody>
		</table>
		<%-- <div class="paging">
			<a href="#">
				<img src="${pageContext.request.contextPath}/common/images/first.jpg" alt="" />
			</a><a href="#">
				<img src="${pageContext.request.contextPath}/common/images/prev2.jpg" alt="" />
			</a><span>
				<a href="#" class="on">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a><a href="#">6</a><a href="#">7</a><a href="#">8</a><a href="#">9</a><a href="#">10</a>
			</span><a href="#">
				<img src="${pageContext.request.contextPath}/common/images/next2.jpg" alt="" />
			</a><a href="#">
				<img src="${pageContext.request.contextPath}/common/images/last.jpg" alt="" />
			</a>
		</div> --%>
	<form id="frm">
		<input type="hidden" name="searchBoardType" value="" />
		<input type="hidden" name="searchContentType" value="${reqVo.searchContentType}" />
		<div class="search_box mat53">
			<input type="text" name="searchText" placeholder="제목 또는 내용의 일부를  입력하세요." value="${reqVo.searchText}"><button id="btn_search">검색</button>
		</div>
	</form>
	
	<script type="text/javascript">	
		var pagingFieldData = new Array (
				{fieldName:"searchBoardType",fieldValue:"${reqVo.searchBoardType}"}
				,{fieldName:"searchContentType",fieldValue:"${reqVo.searchContentType}"}
				,{fieldName:"searchText",fieldValue:"${reqVo.searchText}"}
				,{fieldName:"callPage",fieldValue:pageUrl}
		);
		var pagingProperty = {
			urlPath: "${pageContext.request.contextPath}/index.do" 	//required
		}	
		</script>
		<%@ include file="/WEB-INF/jsp/common/PagingNavi.jsp"%>
	
	</div>
</div>	