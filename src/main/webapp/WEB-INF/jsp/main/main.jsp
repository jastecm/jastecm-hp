<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM01");
});
</script>

<div class="main_banner_area">
	<div class="main_banner1"><img src="${pageContext.request.contextPath}/common/images/main_banner1.jpg" alt="" /></div>
	<ul class="main_banner2">
		<li>
			<a href="${pageContext.request.contextPath}/corporate/partnership.do" class="callPage">
				<h1 class="main_tit1">
					<span></span>
					제휴제안
				</h1>
				<p>자스텍M은 Partner 기업(개인)과의<br>가치 공유 및 상생협력을 존중합니다.</p>
			</a>
		</li>
		<li>
			<a href="${pageContext.request.contextPath}/cs/inquiry.do" class="callPage">
				<h1 class="main_tit1">
					<span></span>
					이용문의
				</h1>
				<p>서비스 이용에 어려운 점이있으신가요?<br>친절하고 신속하게 해결해드리겠습니다.</p>
			</a>
		</li>
	</ul>
</div>
<div class="main_con_area">
	<div class="main_con_1">
		<h1 class="main_tit1">
			<span></span>
			제품
		</h1>
		<ul class="main-slide2">
			<li class="slide_1">
				<a href="${pageContext.request.contextPath}/product/type.do" class="callPage"><img src="${pageContext.request.contextPath}/common/images/main_img1.jpg" alt="" /></a>
				<div class="txt">
					<h1>모델명 von-S21 JTWM 1000 KT</h1>
					<p>GPG와 G-Sensor를 추가하여 사용자 편의성을 극대화한 제품입니다. </p>
				</div>
			</li>
			<li class="slide_1">
				<a href="${pageContext.request.contextPath}/product/type.do" class="callPage"><img src="${pageContext.request.contextPath}/common/images/main_img2.jpg" alt="" /></a>
				<div class="txt">
					<h1>모델명 von-STD JTBT 2200</h1>
					<p>GPG와 G-Sensor를 추가하여 사용자 편의성을 극대화한 제품입니다. </p>
				</div>
			</li>
		</ul>
	</div>
	<div class="main_con_2">
		<h1 class="main_tit1">
			<span></span>
			새소식
		</h1>
		<div class="news_area mab52">
			<h1><a href="${pageContext.request.contextPath}/news/news.do" class="callPage">자스텍M 홈페이지가 개설되었습니다.</a></h1>
			<p><a href="${pageContext.request.contextPath}/news/news.do" class="callPage">자스텍M 홈페이지가 새롭게 개설되었습니다. 자스텍M 홈페이지가 새롭게 개설되었습니다. 자스텍M 홈페이지가 새롭게 개설 되었습니다.</a></p>
			<span>2016.06.01</span>
		</div>
		<div class="news_area">
			<h1><a href="${pageContext.request.contextPath}/news/news.do" class="callPage">자스텍M 홈페이지가 개설되었습니다.</a></h1>
			<p><a href="${pageContext.request.contextPath}/news/news.do" class="callPage">자스텍M 홈페이지가 새롭게 개설되었습니다. 자스텍M 홈페이지가 새롭게 개설되었습니다. 자스텍M 홈페이지가 새롭게 개설 되었습니다.</a></p>
			<span>2016.06.01</span>
		</div>
	</div>
	<div class="main_con_3">
		<img src="${pageContext.request.contextPath}/common/images/main_banner2.jpg" alt="" />
		<div class="news_area">
			<h1><a href="${pageContext.request.contextPath}/employ/today.do" class="callPage">자스텍M 제1차 전사회의 개최</a></h1>
			<p><a href="${pageContext.request.contextPath}/employ/today.do" class="callPage">5월 6일 화요일 자스텍M 본사에서 전사회의를 개최하였습니다. 5월6일 화요일 자스텍M 본사에서 전사회의를 개최하였습니다. 5월 6일화요일 자스텍M 본사에서 전사회의를 개최하였습니다. </a></p>
			<span>2016.06.01</span>
		</div>
	</div>
</div>
<div class="main_con_area2">
	<div class="main_con_4">
		<h1>RECRUIT</h1>
		<p>
			<a href="${pageContext.request.contextPath}/employ/noti.do" class="callPage">
				자스텍M과 함께 성장해나갈 인재를 모십니다.<br>
				자스텍M은 함께 역량을 높히고 성장할 수 있는<br>
				프로그램들을 운영하고 있으며 사기증진과 안정된<br>
				생활을 위해 다양한 복지제도를 운영합니다.
			</a>
		</p>
	</div>
	<div class="main_con_5">
		<h1>PARTNERS</h1>
		<ul class="main-slide2">
			<li class="slide_1">
				<a><img src="${pageContext.request.contextPath}/common/images/site_img1.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img2.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img3.jpg" alt="" /></a>
			</li>
			<li class="slide_1">
				<a><img src="${pageContext.request.contextPath}/common/images/site_img4.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img5.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img6.jpg" alt="" /></a>
			</li>
			<li class="slide_1">
				<a><img src="${pageContext.request.contextPath}/common/images/site_img7.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img8.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img9.jpg" alt="" /></a>
			</li>
			<li class="slide_1">
				<a><img src="${pageContext.request.contextPath}/common/images/site_img10.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img11.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img12.jpg" alt="" /></a>
			</li>
			<li class="slide_1">
				<a><img src="${pageContext.request.contextPath}/common/images/site_img13.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img14.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img15.jpg" alt="" /></a>
			</li>
			<li class="slide_1">
				<a><img src="${pageContext.request.contextPath}/common/images/site_img16.jpg" alt="" /></a>
			</li>
		</ul>
	</div>
</div>