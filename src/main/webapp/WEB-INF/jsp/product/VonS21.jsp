<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
</script>
<div class="sub_layout">
		<div class="sub_title5 mab68">
			상세정보
		</div>
		<div class="sub_tit1">기본 정보</div>
		<div class="sub03_box3">
			<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img5.jpg" alt="" /></div>
			<h1>von-S21</h1>
			<p>von-S21은 국내 서비스 환경에 맞게 개발된 OBD단말기로 승용차 관리 / 관제 및 승용차 UBI용도로 사용되고 있다</p>
		</div>
		<div class="sub_tit1">제품 사양</div>
		<ul class="sub03_box4">
			<li>
				<span>프로세스(Processor)</span>32-bit ARM Cortex-M3
			</li>
			<li>
				<span>메모리(Memory)</span>8MB Serial Flash Memory
			</li>
			<li>
				<span>상태 표시(Status)</span>3-LEDs (Power, Status, Modem)

			</li>
			<li>
				<span>차량통신</span>ISO 9141-2, ISO 14230-4(KWP-2000), ISO 15765-4(CAN), SAE J1850(PWM/VPWM)
			</li>
			<li>
				<span>지원기능</span>SAE J1979 Mode $01, $03, $07, $09, $0A
			</li>
			<li>
				<span>차량 Interface</span>SAE J1962 Standard OBD-II 16 PIN Connector
			</li>
			<li>
				<span>USB Port</span>USB 2.0 Compatible, VCP(Vitural COM Port) Supported

			</li>
			<li>
				<span>UMTS</span>HSPA (UL 5.76Mbps/DL 7.2Mbps) 2100MHz 

			</li>
			<li>
				<span>G-Sensor</span>Bosch 3-Axis 10-Bit Resolution, BMA250
			</li>
			<li>
				<span>GPS</span>A-GPS Support
			</li>
			
			<li>
				<span>전압</span>DC 8V ~ 16V or USB 5V(Only Firmware Upload)
			</li>
			<li>
				<span>소비전류</span>Max Power : ≤6000mW, Sleep: ≤60mW
			</li>
			<li>
				<span>크기(Dimensions)</span>Max. 46mm(W) x 25mm(D) x 66mm(H)
			</li>
			<li>
				<span>무게(Weight)</span>64g

			</li>
			<li>
				<span>동작온도</span>-25 ~ +70 ℃
				
			</li>
			<li>
				<span>보관온도</span>-40 ~ +80 ℃
			</li>
		</ul>
		<div class="sub_tit1">제공 서비스</div>
		<div class="sub03_box5">
			<ul class="sub03_box4">
				<li>
					<span>차량고장진단<br>(Vehicle Diagnostics)</span>
					ECU 결함코드 점검<br>
					ECU 결함코드 (DTC) 소거<br>
					Emissions 관련 항목 점검<br>
					냉각수 온도 상시 점검<br>
					배터리/발전기 상태 점검<br>
					운행정보 점검
				</li>
			</ul>
			<ul class="sub03_box4">
				<li>
					<span>안전 경제운전<br>(Safety & Eco Driving)</span>
					급가속/감속  알림<br>
					엔진 과열 알림<br>
					운행거리/일시, 운행데이타<br>
					운행요약정보, 이력<br>
					소모품교환주기 관리<br>
					사고감지 시스템<br>
					위치기반서비스(Location Based Service)<br>
					운전성향분석<br>
					도난 추적 (Battery  내장)

				</li>
			</ul>
		</div>
		<div class="sub03_box6">
			<a href="#"><img src="${pageContext.request.contextPath}/common/images/btn1.jpg" alt="" /></a>
		</div>
		<div class="bo_view_btn">
			<a href="#"><img src="${pageContext.request.contextPath}/common/images/prev3.jpg" alt=""></a><a href="#"><img src="${pageContext.request.contextPath}/common/images/next3.jpg" alt=""></a>
			<span><a data-page="/product/type" class="callPage">목록</a></span>
		</div>
	</div>