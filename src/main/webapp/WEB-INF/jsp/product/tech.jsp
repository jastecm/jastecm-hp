<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM04");
	initMenuSel("LM04_01");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			제품의 주요 특화 기술
		</div>
		<div class="sub_title7_2">
			자스텍M의 수준높은 기술로 <span>세계적인 차량 IoT제품</span>을 만들어나갑니다. 
		</div> 
		<ul class="sub03_box1">
			<li>
				<span>MQTT<br>Establishment</span>
				ㆍ M2M과 IoT 용도의 경량 프로토콜 : 환경적합성이 높음<br>
				ㆍ 낮은 전력·낮은 대역폭의 서비스 구현<br>
				ㆍ OBD 단말기 부터 ~ 플랫폼 데이터까지 표준 MQTT화를 통한 IoT 융합 서비스 구현 적합<br>
				ㆍ e-Call과 같은 긴급한 출동·대응 서비스 적합<br>
				ㆍ 지능형 네트워킹 기술 구현<br>
				ㆍ 최적화된 데이터 Real Time 처리 성능<br>
				ㆍ 단말·서비스 內 고객사 서비스 프로그램 탑재 가능 
				<div><img src="${pageContext.request.contextPath}/common/images/sub03_img1.jpg" alt="" /></div>
				<br><br><br><br>
			</li>
			<li>
				<span>모듈형 OBD<br>제품 설계를 통한 <br>비즈니스 유연성</span>
				ㆍ 비즈니스 고객사의 다양한 요구사항에 대한 신속하고·저렴한 개발비용으로 대응 가능<br>
				ㆍ 사업 방향·용도에 따라 적합한 센서 적용 가능<br>
				ㆍ 국내 IoT 시장 점유할 LTE-MTC (KT/LG U+) 와 LoRa (SKT) 통신 모듈 모두 적용 가능<br>
				&nbsp;&nbsp;&nbsp;- 저전력·낮은 대역폭으로 장거리 통신기술 활용 가능<br>
				&nbsp;&nbsp;&nbsp;- 저렴한 초기 망 구축 비용<br>
				&nbsp;&nbsp;&nbsp;- 모듈별 얼라이언스 업체들과 다양한 콘텐츠 사업 창출
				<div><img src="${pageContext.request.contextPath}/common/images/sub03_img2.jpg" alt="" /></div>
			</li>
		</ul>
		
	</div>