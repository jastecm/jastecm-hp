<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM04");
	initMenuSel("LM04_02");
});

$(document).ready(function(){
    $(".tabbable").find(".tab").hide();
    $(".tabbable").find(".tab").first().show();
    $(".tabbable").find(".tabs li").first().find("a").addClass("active");
    $(".tabbable").find(".tabs").find("a").click(function(){
        tab = $(this).attr("href");
        $(".tabbable").find(".tab").hide();
        $(".tabbable").find(".tabs").find("a").removeClass("active");
        $(tab).show();
        $(this).addClass("active");
        return false;
    });
});

</script>
	<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			자동차 IoT 단말
		</div>
		<div class="sub03_img3">
			<img src="${pageContext.request.contextPath}/common/images/sub03_img3.jpg" alt="" />
		</div> 
		
		
		<div class="tabbable">
    <ul class="tabs">
    <div class="sub_tab">
			<a href="#On-board형" class="on">On-Board형</a><a href="#On-Line형">On-Line형</a>
		</div>
    	
    </ul>
    <div class="tabcontent">
        <div id="On-board형" class="tab">
            <ul class="sub03_box2">
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img11.jpg" alt="" /></div>
						<h1>모델명 : JTBT-2200 / JTWF-1100 (Von-STD)</h1>
						<p>Wifi, Bluetooth를 통한 무선통신</p>
						<p>
							차량 센서, 운행 정보 수집<br>
						    - Trip/일별/월별 운행기록 조회, 운전 성향 분석, 차량 고장진단, 소모품 관리
						</p>
						<p>
							경제운전 유도<br>
							- 연비, CO2 배출량, 급가속/감속, 급정지 등 분석 
						</p>
						<p>
							승용차요일제/마일리지보험 지원<br>
							- 보험사 제출용 Data 생성
						</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>특징</b></dt>
								<dd class="d2">승용차 UBI, eCall, FMS 지원 단말</dd>
							</dl>
							<dl>
								<dt class="d3"><b>프로세스</b></dt>
								<dd class="d4">32-bit ARM Cortex-M3(256KB Flash, 64KB SRAM)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>차량통신</b></dt>
								<dd class="d4">
									ISO 9141-2, ISO 14230-4(KWP-2000), ISO 15765-4(CAN),<br>SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonSTD" class="callPage">상세정보</a></div>
					</li>
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img12.jpg" alt="" /></div>
						<h1>모델명 : JTBT-3100 (Von-G)</h1>
						<p>
							Von-STD 단말에 G-Sensor 추가<br>
							- 차량 충격 감지, 차량 급회전 감지를 하여 e-Call 및 UBI에 특화
						</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>특징</b></dt>
								<dd class="d2">승용차 UBI, eCall, FMS 지원 단말</dd>
							</dl>
							<dl>
								<dt class="d3"><b>프로세스</b></dt>
								<dd class="d4">32-bit ARM Cortex-M3(256KB Flash, 64KB SRAM)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>차량통신</b></dt>
								<dd class="d4">
									ISO 9141-2, ISO 14230-4(KWP-2000), ISO 15765-4(CAN),<br>SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonG" class="callPage">상세정보</a></div>
					</li>
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img13.jpg" alt="" /></div>
						<h1>모델명 : JTBT-4100 (Von-G2)</h1>
						<p>
							GPG, G-Sensor를 추가하여사용자 편의성 극대화<br>
							- 차량 위치정보 수집, 충격 감지시 위치정보 전송<br>
							- 센서정보를 통한 차량 급회전 정확성 향상, 12/24Volt 동시 지원
						</p>
						<p>FMS, e-Call의 정밀도 및 편의성 개선</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>특징</b></dt>
								<dd class="d2">승용차 UBI, eCall, FMS 지원 단말</dd>
							</dl>
							<dl>
								<dt class="d3"><b>프로세스</b></dt>
								<dd class="d4">32-bit ARM Cortex-M3(256KB Flash, 64KB SRAM)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>차량통신</b></dt>
								<dd class="d4">
									ISO 9141-2, ISO 14230-4(KWP-2000), ISO 15765-4(CAN),<br>SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonG2" class="callPage">상세정보</a></div>
					</li>					
				</ul>
			</div>
        </div>
        <div id="On-Line형" class="tab">
            <ul class="sub03_box2">
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img4.jpg" alt="" /></div>
						<h1>Von-S2</h1>
						<p>Air Update</p>
						<p>차량 센서 ∙ 운행정보 수집</p>
						<p>OBDII 지원</p>
						<p>차량관리 / 위치 관제 / 운행 성향 분석</p>
						<p>Digital I/O 확장</p>
						<p>긴급구난/도난추적 등 위치기반 서비스</p>
						<p>해외 자동차 환경  대응</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>특징</b></dt>
								<dd class="d2">수출용 단말</dd>
							</dl>
							<dl>
								<dt class="d3"><b>프로세스</b></dt>
								<dd class="d4">32-bit ARM Cortex-M3 (4M Memory)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>차량통신</b></dt>
								<dd class="d4">
									ISO 9141-2, ISO 14230-4(KWP-2000), ISO 15765-4(CAN),<br>SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonS2" class="callPage">상세정보</a></div>
					</li>
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img5.jpg" alt="" /></div>
						<h1>Von-S21</h1>
						<p>Air Update</p>
						<p>차량 센서 ∙ 운행정보 수집</p>
						<p>KT 흥국화재, 매리츠화재 UBI 시범사업용 단말</p>
						<p>차량관리 / 위치 관제 / 운행 성향 분석</p>
						<p>국내 차량 서비스 환경, 이동통신, 고객 비즈니스 요구에 부합되도록 개량 모델 통신사 IOT 인증</p>
						<p>긴급구난/도난추적 등 위치 서비스(A-GPS  Support)</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>특징</b></dt>
								<dd class="d2">승용차 전용 단말</dd>
							</dl>
							<dl>
								<dt class="d3"><b>프로세스</b></dt>
								<dd class="d4">32-bit ARM Cortex-M3 (8M Memory)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>차량통신</b></dt>
								<dd class="d4">
									ISO 9141-2, ISO 14230-4(KWP-2000), ISO 15765-4(CAN),<br>SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonS21" class="callPage">상세정보</a></div>
					</li>
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img6.jpg" alt="" /></div>
						<h1>Von-S3</h1>
						<p>12/24 Volt 동시 지원</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>특징</b></dt>
								<dd class="d2">국내용 단말</dd>
							</dl>
							<dl>
								<dt class="d3"><b>프로세스</b></dt>
								<dd class="d4">32-bit ARM Cortex-M3 (8M Memory)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>차량통신</b></dt>
								<dd class="d4">
									ISO 9141-2, ISO 14230-4(KWP-2000), ISO 15765-4(CAN),<br>SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonS3" class="callPage">상세정보</a></div>
					</li>
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img7.jpg" alt="" /></div>
						<h1>Von-F</h1>
						<p>J1939/J1708 지원</p>
						<p>차량관리 / 위치 관제</p>
						<p>운행이력/분석</p>
						<p>Digital I/O 확장</p>
						<p>긴급구난/도난추적</p>
						<p>12/24 Volt 동시 지원</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>특징</b></dt>
								<dd class="d2">DTG (버스 ∙ 화물차)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>프로세스</b></dt>
								<dd class="d4">32-bit ARM Cortex-M3 (8M Memory)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>차량통신</b></dt>
								<dd class="d4">
									ISO 9141-2, ISO 14230-4(KWP-2000), ISO 15765-4(CAN),<br>SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonF" class="callPage">상세정보</a></div>
					</li>
				</ul>
			</div>
        </div>
      
   	
			<br><br><br><br><br>
			<div class="sub05_box3">
				<h1>제품ㆍ구매 문의</h1>
				<ul>
					<li>
					<span class="img"><img src="${pageContext.request.contextPath}/common/images/sub05_img8.jpg" alt="" /></span>
					<h2>솔루션 구매 문의</h2>
					<div>
						<span class="w209">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img5.jpg" alt="" />박영민(전략마케팅팀/과장)
						</span><span class="w146">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img6.jpg" alt="" />02-452-9710
						</span><span>
							<img src="${pageContext.request.contextPath}/common/images/sub05_img7.jpg" alt="" />youngmin.park@jastecm.com
						</span>
					</div>
				</li>
					<li>
						<span class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img9.jpg" alt=""></span>
							<h2>제품ㆍ구매 문의</h2>
						<div>
							<span class="w209">
								<img src="${pageContext.request.contextPath}/common/images/sub05_img5.jpg" alt="">장승부(전략마케팅팀/대리)
							</span><span class="w146">
								<img src="${pageContext.request.contextPath}/common/images/sub05_img6.jpg" alt="">031-8060-0321
							</span><span>
								<img src="${pageContext.request.contextPath}/common/images/sub05_img7.jpg" alt="">jasmes.jang@jastecm.com
							</span>
						</div>
					</li>
				</ul>
			</div>
		</div>