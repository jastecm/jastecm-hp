<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script src='${pageContext.request.contextPath}/common/js/jquery.MultiFile.js' type="text/javascript" language="javascript"></script>



<style>
    .MultiFile-applied {
        background:url(../images/btn_file.jpg) no-repeat right top;
        height: 44px;
        cursor: pointer;
    }                  
</style>
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02"); 
	initMenuSel("LM02_08");
	
	$('#frm').ajaxForm({
		beforeSubmit: function (data,form,option) {
			return true;
		},
		success: function(res,status){
	    	
			var rtvCd = res.rtvCd;
			if(rtvCd == "0"){
				alert("error");
			}else{
				var url = "/index.do";
				res.rtvMap.callPage = "/corporate/partnershipReceipt";
				$JBIZ.instance_post(url,res.rtvMap);
			}
		},
		error: function(){
	    	alert("error");
		}                               
	});
	
	$("#frm").submit(function(){return false});
	
	$("#btn_send").on("click",function(){
		if(!($("#input-1").is(":checked"))){ 
			alert("개인정보 수집 이용안내에 동의해 주세요.");
			return false;
		}
		$('#frm').submit();
	});
});
</script>

	<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			제휴ㆍ제안
		</div>
		<div class="sub_txt2 mab18">자스텍M과 Win-Win 전략을 함께 할 기업 및 개인의 다양한 제휴제안은 항상 열려있습니다.</div>
		<div class="sub06_box1"> 
			<p>관련문의는 하단의 해당 부서 전화나 이메일을 이용해주시기 바랍니다.</p>
			<p>등록된 제휴제안 내용은 5일(영업일 기준)이내 처리를 기본으로 하고 제안내용 및 관련자료는 제휴검토 목적으로만 이용됩니다.</p>
			<p>제휴 제안내용에 대한 검토 결과를 담당자가 회신드리겠습니다.</p>
		</div>
		<form id="frm" action="${pageContext.request.contextPath}/com/fileUpload.do" method="post" enctype="multipart/form-data">
			<div class="sub_tit1">제휴제안 내용</div>
			<table class="table2 table2_3 mab76">
				<colgroup>
					<col width="73" />
					<col width="*" />
					<col width="260" />
				</colgroup>
				<tbody>
					<tr>
						<th>제목(필수)</th>
						<td><input type="text" class="w456" name="name" placeholder="제목을 입력해주세요.(필수)" /></td>
						<td>
							<div class="select_type1">
								<div class="select_type1_1">
									<select name="partnershipType" class="sel">									
										<option value="">제휴구분 선택</option>
										<option value="차량IoT단말부문">차량IoT단말부문</option>
										<option value="솔루션부문">솔루션부문</option>
										<option value="파트너쉽부문">파트너쉽부문</option>
									</select>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th>내용(필수)</th>
						<td colspan="2">
							<textarea name="contents"></textarea>
							<p class="t1">* 제안자의 권리보호를 위해 저작권이 보호되는 않는 핵심기술은 상세한 설명을 제외하고 작성해주십시오.</p>
						</td>
					</tr>
					<tr>
						<th>제안서<br>첨부파일</th>						
						<td colspan="2">
							<div class="file_txt2">한글(*.hwp),MS워드(*.doc),프리젠테이션파일(*.ppt)만 업로드 가능합니다.</div>
							<div class="btn_file"><input type="file" name="file" class="multi2" accept="hwp|doc|ppt" maxlength="3"/></div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="sub_tit1">제휴제안자 정보</div>
			<table class="table2 table2_3">
				<colgroup>
					<col width="73" />
					<col width="*" />
					<col width="59" />
					<col width="200" />
					<col width="260" />
				</colgroup>
				<tbody>
					<tr>
						<th>회사명</th>
						<td><input type="text" class="w188" name="corpo" placeholder="회사명을 입력해주세요." /></td>
						<th>제안자명</th>
						<td><input type="text" class="w188" name="person" placeholder="이름을 입력해주세요." /></td>
						<td>
							<div class="select_type1">
								<div class="select_type1_1">
									<select name="personType" class="sel">
										<option value="">제안자 구분 선택</option>
										<option value="구매기업">구매기업</option>
										<option value="SI(시스템 통합)">SI(시스템 통합)</option>
										<option value="연구기관">연구기관</option>
										<option value="대학교,학생">대학교,학생</option>
									</select>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<table class="table2 table2_3">
				<colgroup>
					<col width="73" />
					<col width="*" />
					<col width="71" />
					<col width="304" />
				</colgroup>
				<tbody>
					<tr>
						<th>전화번호</th>
						<td><input type="text" class="w304" name="phone" placeholder="전화번호를 입력해주세요." /></td>
						<th>이메일주소</th>
						<td><input type="text" class="w304" name="email" placeholder="이메일을 입력해주세요." /></td>
					</tr>
				</tbody>
			</table>
			<table class="table2 table2_3 mab76">
				<colgroup>
					<col width="73" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th>회사소개서<br>첨부파일</th>
						<td>
							<div class="file_txt">한글(*.hwp),MS워드(*.doc),프리젠테이션파일(*.ppt)만 업로드 가능합니다.</div>
							<div class="btn_file"><input type="file" name="file2" class="multi" accept="" accept="hwp|doc|ppt" maxlength="3"/></div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="sub_tit1">개인정보 수집 및 이용에 대한 안내</div>
			<div class="sub06_box2">
				자스텍M은 1:1문의 답변에 대한 개인정보를 수집하고 있습니다.<br>
				1. 수집 개인정보 항목 : [필수] 이름, 이메일<br>
				2. 개인정보의 수집 및 이용목적 : 서비스이용 문의에 따른 본인확인 및 답변 경로 확보<br>
				3. 개인정보의 이용기간 : 모든 답변이 완료된 후 해당정보를 지체없이 파기합니다.<br>
				4. 동의 거부권리 안내 추가 : 위와 같은 개인정보 수집 동의를 거부할 수 있습니다. 다만 동의를 거부하는 경우 문의답변이 어려울수 있습니다.<br>
				그 밖의 사항은 각 사별 개인정보처리방침을 준수합니다.
			</div>
			<div class="agree_box demo-list mab63">
				<input tabindex="1" type="checkbox" id="input-1" class="ichk"><label for="input-1">개인정보 수집 및 이용에 동의합니다.</label>
			</div>
		</form>
		<div class="btn_area1">
			<a href="${pageContext.request.contextPath}/corporate/partnership.do" class="callPage">취소</a>
			<a id="btn_send">접수하기</a>
		</div>
		<br><br><br><br><br><br><br>
		<div class="sub05_box3">
			<h1>제휴제안 담당 부서 안내</h1>
			<ul>				
				<li>
					<span class="img"><img src="${pageContext.request.contextPath}/common/images/sub05_img8.jpg" alt=""></span>
					<h2>제휴/서비스 문의</h2>
					<div>
						<span class="w209">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img5.jpg" alt="">박영민(전략마케팅팀/과장)
						</span><span class="w146">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img6.jpg" alt="">02-452-9710
						</span><span>
							<img src="${pageContext.request.contextPath}/common/images/sub05_img7.jpg" alt="">youngmin.park@jastecm.com
						</span>
					</div>
				</li>
				<li>
					<span class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img9.jpg" alt=""></span>
					<h2>해외/기타 문의</h2>
					<div>
						<span class="w209">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img5.jpg" alt="">장승부(전략마케팅팀/대리)
						</span><span class="w146">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img6.jpg" alt="">031-8060-0321
						</span><span>
							<img src="${pageContext.request.contextPath}/common/images/sub05_img7.jpg" alt="">jasmes.jang@jastecm.com
						</span>
					</div>
				</li>
			</ul>
		</div>
	</div>