<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02");
	initMenuSel("LM02_02");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			연혁
		</div>
		<div class="sub_title7_2">
			차량 IT전문기업 자스텍과 IoT 솔루션 전문기업 인피니티플러스가 만나,<br>
			<b>이동하는 사람과 사물을 위한 전문기업 <span>자스텍M</span>으로 새롭게 출발합니다.</b>
		</div>
		<div class="history_area">
			<h1><img src="${pageContext.request.contextPath}/common/images/his_img1.jpg" alt="" /></h1>
			<div class="history_area2">
				<div class="history_area2_1">
					<ul>
						<li>
							<h2><img src="${pageContext.request.contextPath}/common/images/his_img2.jpg" alt="" /></h2>
							<h3>㈜인피니티플러스</h3>
						</li>
						<li>
							<p>도로교통공단 교통사고분석시스템(TAAS)기반 자동차<br>
							안전 ∙ 경제운전 지수 공동개발 및 사업화</p>
							<p>미래부 K-Global 스마트∙ 모바일 스타기업 자동차<br>
							ICT 융합스마트 드라이빙 콘텐츠 및 플랫폼 제작 지원</p>
							<p>국가기술표준원 2015 표준화 프레임워크<br>
							차량 긴급구난체계(e-Call) 표준화</p>
							<p>LG전자, LGU+ oneM2M 기반 스마트카 솔루션  개발</p>
							<p>현대해상 UBI 상품개발 협약 체결</p>
							<p>당사 특허기반 ISO/WD20530</p>
							<p>Connected Car 서비스 ViewCAR 상용화</p>
							<p>특허청 산하 지식재산전략원 ‘스마트카 분야<br>
							중점 IP’지원대상 선정</p>
							<p>세계 최초 ACN 자동차 에어백 진단 솔루션 개발</p>
							<p>2006.11 회사 설립</p>						
							</li>
					</ul>
				</div>
				<div class="history_area2_2">
					<ul class="mab258">
						<li>
							<h3>㈜자스텍엠</h3>
						</li>
						<li>
							<p>2016. 7 ㈜자스텍 & ㈜인피니티플러스 합병</p>
						</li>
					</ul>
					<ul>
						<li>
							<h2><img src="${pageContext.request.contextPath}/common/images/his_img3.jpg" alt="" /></h2>
							<h3>㈜자스텍 - <strong>자동차IoT 사업부문</strong></h3>
						</li>
						<li>
							<p>한국정보화진흥원 미래성장동력 플래쉽 프로젝트 참여기업</p>
							<p>KT UBI IoT 차량단말기 공급</p>
							<p>미국 Audiovox/Airtyme/Modus 社와 단말기 공급</p>
							<p>삼성전자 서비스 차량 FMS 관제용 단말 공급</p>
							<p>델파이 호주 UBI용 von-STD 단말기 공급</p>
							<p>교통안전공단 지정검사소 전자장치 진단기 개발</p>
							<p>한국도요타 캠리, 커뮤니케이터 적용</p>
							<p>삼성전자 갤럭시탭 연동 차량용 솔루션 기기 납품</p>
							<p>대전광역시 승용차요일제 OBD 사업자 선정</p>
							<p>KT금호렌터카 운행거리확인용 OBD 공급</p>
							<p>제2.3세대 자동차 진단기기 개발 공급</p>
							<p class="mab0">1990.5 회사 설립</p>
						</li>
					</ul>
					<span class="s1"></span>
					<span class="s2"></span>
					<span class="s3"></span>
				</div>
			</div>
		</div>
	</div>