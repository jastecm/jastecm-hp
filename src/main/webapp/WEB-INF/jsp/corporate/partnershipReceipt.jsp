<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
$(document).ready(function(){
	$("#btn_send").on("click",function(){
		var frmParam = $("#frm").serializeArray();
		var param = {};
		for(var i= 0 ; i < frmParam.length ; i++){
			if(param[frmParam[i].name]) param[frmParam[i].name] += ","+frmParam[i].value
			else param[frmParam[i].name] = frmParam[i].value
		}
		param.callPage = "/corporate/ReceiptFinish";
		var url = "/index.do";
		$JBIZ.instance_post(url,param);
	});
});
</script>
	<div class="sub_layout">
		<div class="sub_title5 mab68">
			접수 확인
		</div>
		<form id="frm">
		<div class="sub_tit1">제휴제안 내용</div>
		<table class="table2 table2_2 mab76">
			<colgroup>
				<col width="88" />
				<col width="*" />
				<col width="74" />
				<col width="130" />
			</colgroup>
			<tbody>
				<tr>
					<th>제목</th>
					<td>${reqVo.name }<input type="hidden" name="name" value="${reqVo.name}"></td>
					<th>제휴 구분</th>
					<td>${reqVo.partnershipType }<input type="hidden" name="partnershipType" value="${reqVo.partnershipType}"></td>
				</tr>
				</tr>
				<tr>
					<th>내용</th>
					<td colspan="3">${reqVo.contents}<input type="hidden" name="contents" value="${reqVo.contents}"></td>
				</tr>
				<tr>
					<th>제안서<br>첨부파일</th>
					<td colspan="3">
						<c:set var="fileList" value="${fn:split(reqVo.file,',')}" />
						<c:set var="fileNmList" value="${fn:split(reqVo.fileNm,',')}" />
						<c:forEach var="vo" items="${fileList}" varStatus="i">
							<input type="hidden" name="file" value="${vo}">
						</c:forEach>
						<c:forEach var="vo" items="${fileNmList}" varStatus="i">
							<c:if test="${i.index eq 0}">
								<a href="${pageContext.request.contextPath}/com/getFile.do?uuid=${fileList[i.index]}">${vo }</a>
							</c:if>
							<c:if test="${i.index ne 0}">
								</br><a href="${pageContext.request.contextPath}/com/getFile.do?uuid=${fileList[i.index]}">${vo }</a>
							</c:if>
						</c:forEach>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="sub_tit1">제휴제안자 정보</div>
		<table class="table2 table2_2 mab76">
			<colgroup>
				<col width="88" />
				<col width="177" />
				<col width="88" />
				<col width="*" />
				<col width="80" />
				<col width="120" />
			</colgroup>
			<tbody>
				<tr>
					<th>회사명</th>
					<td>${reqVo.corpo}<input type="hidden" name="corpo" value="${reqVo.corpo}"></td>
					<th>제안자명</th>
					<td>${reqVo.person}<input type="hidden" name="person" value="${reqVo.person}"></td>
					<th>제안자 구분</th>
					<td>${reqVo.personType}<input type="hidden" name="personType" value="${reqVo.personType}"></td>
				</tr>
				</tr>
				<tr>
					<th>전화번호</th>
					<td>${reqVo.phone}<input type="hidden" name="phone" value="${reqVo.phone}"></td>
					<th>이메일주소</th>
					<td colspan="3">${reqVo.email}<input type="hidden" name="email" value="${reqVo.email}"></td>
				</tr> 
				<tr>
					<th>회사소개서<br>첨부파일</th>
					<td colspan="5">
						<c:set var="fileList" value="${fn:split(reqVo.file2,',')}" />
						<c:set var="fileNmList" value="${fn:split(reqVo.file2Nm,',')}" />
						<c:forEach var="vo" items="${fileList}" varStatus="i">
							<input type="hidden" name="file" value="${vo}">
						</c:forEach>
						<c:forEach var="vo" items="${fileNmList}" varStatus="i">
							<c:if test="${i.index eq 0}">
								<a href="${pageContext.request.contextPath}/com/getFile.do?uuid=${fileList[i.index]}">${vo }</a>
							</c:if>
							<c:if test="${i.index ne 0}">
								</br><a href="${pageContext.request.contextPath}/com/getFile.do?uuid=${fileList[i.index]}">${vo }</a>
							</c:if>
						</c:forEach>
					</td>
				</tr>
			</tbody> 
		</table>
		</form>
		<div class="sub_txt1 mab50">
			<p>ㆍ등록된 제휴제안 내용은 5일(영업일 기준)이내 처리를 기본으로 하고 제안내용 및 관련자료는 제휴검토 목적으로만 이용됩니다.</p>
			<p>ㆍ제휴 제안내용에 대한 검토 결과를 담당자가 회신드리겠습니다.</p>
		</div>
		<div class="btn_area1">
			<a href="${pageContext.request.contextPath}/corporate/partnership.do" class="callPage">취소하기</a>
			<a id="btn_send">접수완료</a>
		</div>
	</div>
