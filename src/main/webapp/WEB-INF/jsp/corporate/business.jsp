<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02");
	initMenuSel("LM02_04");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			사업영역 및 전략
		</div>
		<div class="sub_title7_2 mab76">
			사물인터넷은 단말ㆍ네트워크에서 <span>솔루션ㆍ정보 중심으로 변화</span>하고 있습니다.<br>
			텔레매틱스와 개인휴대단말이 결합한 서비스는 다양한 부가가치를 생산할 수 있는
			잠재력을 보유하고 있으며 <span>자스텍M은 이러한 변화에 앞장서 나갈 것입니다.</span>
		</div>
		<div class="sub02_box1 mab100">
			<div class="tit">
				자스텍M의<br>
				기반 데이터와<br>
				기술분야
			</div>
			<p class="t1">
				In-Vehicle network를 통한 센서 ∙ 운행정보 수집 ∙ 분석 ∙ 가공에 대한 특화기술 보유하고 있으며
				시장에 따른 제품 ∙ 솔루션 개발하여 다양한 공공 데이터셋 활용 및 학습을 통한 서비스 진화를
				이루고 있습니다.
			</p>
			<div class="sub02_box2">
				<span><img src="${pageContext.request.contextPath}/common/images/sub02_img1.jpg" alt="" />
					<strong>
						In-Vehicle Networks부문<br>
						특화기술 보유
					</strong>
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img2.jpg" alt="" />
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img3.jpg" alt="" />
					<strong>
						특화기술기반 ICT융합<br>
						제품&솔루션 개발
					</strong>
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img2.jpg" alt="" />
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img4.jpg" alt="" />
					<strong>다양한 공공데이터와<br>학습을 통한 서비스 진화</strong>
					
				</span>
			</div>
		</div>
		<div class="sub02_box1 mab100">
			<div class="tit">
				시장요구와<br>
				사업방향
			</div>
			<p class="t1">
				텔레매틱스 미지원 차량을 보유한 운전자들이 편리하고 저렴하게 이용할 수 있는 범용적이고				
				개방적인 서비스와 운전자의 안전 및 편의ㆍ경제성, 즐거움을 누릴 수 있는 기술을 구현합니다.  
			</p>
			<ul class="sub02_box3">
				<li>
					<p>일부차량에만 한정된 텔레매틱스<br>서비스의 범용화 요구</p>
					<p>인프라와 플랫폼을 활용한<br>자동차 ICT 융합의 확대</p>
					<p>차량 사고 및 안전조치에대한<br>사회적 공감확대</p>
				</li>
				<li>
					<p>스마트폰의 활성화에 따른 운전자의<br>편의와 차량관리 요구</p>
					<p>다양한 사업에 적용 가능한<br>개방형 플랫폼 요구 </p>
					<p>V2X기술의 발달로 인한<br>Cooperative ITS로 진화</p>
				</li>
			</ul>
			<ul class="sub02_box4">
				<span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img5.jpg" alt="" />
					<strong>
					표준 개방형<br>플랫폼
					</strong>
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img5.jpg" alt="" />
					<strong>
						커넥티드카<br>적용차종 확장
					</strong>
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img5.jpg" alt="" />
					<strong>
							운전자<br>편의/경제/안전성 향상
					</strong>
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img5.jpg" alt="" />
					<strong>
					다양한 산업과<br>응용ㆍ결합
					</strong>
				</span>
			</ul>
		</div>
		<div class="sub02_box1">
			<div class="tit">
				기술추진전략				
			</div>
			<p class="t1">
				신서비스 산업 창출과 정확하고 신뢰성있는 세계 최고의 커넥티드카 제품과 솔루션 개발에				
				앞장서나가겠습니다.
			</p>			
			<img style="margin-left:70px" src="${pageContext.request.contextPath}/common/images/sub02_img6.jpg" alt="" />
		</div>

	</div>