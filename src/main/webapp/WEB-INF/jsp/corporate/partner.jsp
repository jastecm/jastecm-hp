<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02");
	initMenuSel("LM02_07");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			파트너 센터
		</div>
		<div class="sub_title7_2">
			자스텍M은 Partner 기업ㆍ개인과의 <span>가치 공유 및 상생협력</span>을 존중합니다. 
		</div>
		<br><br>
		<div class="sub02_tit1"><img src="${pageContext.request.contextPath}/common/images/arrow3.jpg" alt="" />해외기업</div>
		<div class="sub02_box7 mab0">
			<span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site1.jpg" alt="" /></strong>
				<strong>
					<b>DELPHI</b>글로벌 자동차부품 제조기업
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site2.jpg" alt="" /></strong>
				<strong>
					<b>MODUS</b>미국 자동차 IT기업 
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site3.jpg" alt="" /></strong>
				<strong>
					<b>AUDIO VOX</b>미국 IT기업 
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site4.jpg" alt="" /></strong>
				<strong>
					<b>Mobileye</b>글로벌 자동차부품 IT기업
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site5.jpg" alt="" /></strong>
				<strong>
					<b>TOYOTA</b>글로벌 자동차 제조기업
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site6.jpg" alt="" /></strong>
				<strong>
					<b>Agnik</b>미국 자동차부품 IT기업
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site7.jpg" alt="" /></strong>
				<strong>
					<b>XTOOLS</b>중국 자동차 IoT기업
				</strong>
			</span>
		</div>
		<div class="sub02_tit1"><img src="${pageContext.request.contextPath}/common/images/arrow3.jpg" alt="" />정부기관</div>
		<div class="sub02_box7 sub02_box7_2 mab0">
			<span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site8.jpg" alt="" /></strong>
				<strong>
					<b>대전 시청</b>대전시행정사무기관 
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site9.jpg" alt="" /></strong>
				<strong>
					<b>도로교통공단</b>경찰청 산하 준정부기관
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site10.jpg" alt="" /></strong>
				<strong>
					<b>교통안전공단</b>국토교통부 산하의 공단
				</strong>
			</span>
		</div>
		<div class="sub02_tit1"><img src="${pageContext.request.contextPath}/common/images/arrow3.jpg" alt="" />국내기업</div>
		<div class="sub02_box7 sub02_box7_3">
			<span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site11.jpg" alt="" /></strong>
				<strong>
					<b>케이티</b>대한민국 통신 서비스기업
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site12.jpg" alt="" /></strong>
				<strong>
					<b>엘지유플러스</b>대한민국 통신 서비스기업
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site13.jpg" alt="" /></strong>
				<strong>
					<b>삼성전자</b>글로벌 IT기업 
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site14.jpg" alt="" /></strong>
				<strong>
					<b>현대해상</b>대한민국 손해보험사
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site15.jpg" alt="" /></strong>
				<strong>
					<b>이네이블</b>O2O서비스 및 Mobile 마케팅 기업
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site16.jpg" alt="" /></strong>
				<strong>
					<b>일마일</b>대한민국 공간정보  IT기업
				</strong>
			</span><span>
				<strong class="img"><img src="${pageContext.request.contextPath}/common/images/site17.jpg" alt="" /></strong>
				<strong>
					<b>웨이티즈</b>대한민국 V2X IT 기업
				</strong>
			</span>
		</div>
	</div>