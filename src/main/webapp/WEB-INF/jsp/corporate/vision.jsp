<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02");
	initMenuSel("LM02_01");
});
</script>

<div class="sub_layout">
	<div class="sub_title1">
		<span></span>
		비전
	</div>
	<div class="sub_title2">
		FROM <strong><b>HABIT</b></strong> TO <strong><b>ECONOMIC</b></strong><span>습관에서 생활로의 진화</span>
	</div>
	<div class="sub01_box1">
		<h1>세상 모든 자동차와 연결되는 새로운 공간, 자스텍M</h1>
		<p>
			자스텍M은 이동하는 사람과 사물을 잇는 제품과 기술을 만듭니다. 보다 편리하고, 안전한 세상을 만들기 위한 <b>「패러다임의 쉬프트」</b>를 준비합니다.
			<br/>학습하고 진화하는 사물이 세상을 더욱 가깝고 새롭게 할 것을 자스텍M은 믿습니다.
		</p>
	</div>
	<div class="img_box1"><img src="${pageContext.request.contextPath}/common/images/sub01_img1.jpg" alt="" /></div>
</div>