<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02");
	initMenuSel("LM02_06");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			보유기술 현황
		</div>
		<div class="sub_title6">
			<span>기술 인증</span><strong></strong>
		</div>
		<div class="sub02_box6">
			<div class="tit">
				<p>미국 FCC 인증 획득</p>
				<p>보험개발원 인증 획득</p>
				<p>KCC 인증 획득</p>
			</div>
			<table class="table3">
				<colgroup>
					<col width="178">
					<col width="151">
					<col width="*">
				</colgroup>
				<thead>
					<tr>
						<th>Certification</th>
						<th>Model</th>
						<th>Certification No.</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>FCC</th>
						<th>JTBT-1100</th>
						<th>UK4JTBT1100</th>
					</tr>
					<tr>
						<th>FCC</th>
						<th>JTBT-2100</th>
						<th>UK4JTBT2100</th>
					</tr>
					<tr>
						<th>FCC</th>
						<th>JTWF-1100</th>
						<th>UK4JTWF1100</th>
					</tr>
					<tr>
						<th>FCC</th>
						<th>JTCM-1000</th>
						<th>UK4JTCM1000</th>
					</tr>
					<tr>
						<th>FCC</th>
						<th>JTGM-1100</th>
						<th>UK4JTGM-1100</th>
					</tr>
					<tr>
						<th>FCC</th>
						<th>JFCM-1000</th>
						<th>KCC-CCM-JS0-JTBT1100</th>
					</tr>
					<tr>
						<th>KC</th>
						<th>JTBT-1100</th>
						<th>KCC-CMM-JS0-JTBT2100</th>
					</tr>
					<tr>
						<th>KC</th>
						<th>JTBT-2100</th>
						<th>KCC-CCM-JS0-JTWF1100</th>
					</tr>
					<tr>
						<th>보험개발원</th>
						<th>JTBT-2100</th>
						<th>KIDI-11-07</th>
					</tr>
					<tr>
						<th>보험개발원</th>
						<th>JTUB-1000</th>
						<th>KIDI-11-10 (대전시청 요일제)</th>
					</tr>
				</tbody>
			</table>
			<img src="${pageContext.request.contextPath}/common/images/sub02_img9.jpg" alt="" class="mab97" />
		</div>
		<div class="sub_title6">
			<span>지적 재산권</span><strong></strong>
		</div>
		<div class="sub02_box6">
			<div class="tit">
				<p class="p1">
					당사 특허기반<br>e-Call ISO 국제표준
					<span>Working Draft 진행중</span>
				</p>
				<p class="p1">
					차량운전습관<br>시스템의 공공성확보 
					<span>도로교통공단과 공동저작권등록 </span>
				</p>
				<p>차량 센서/운행 정보<br>획득 원천기술 보유</p>
			</div>
			<table class="table3 table3_2">
				<colgroup> 
					<col width="105">
					<col width="160">
					<col width="*">
				</colgroup>
				<thead>
					<tr>
						<th>구분</th>
						<th>지적재산권 No.</th>
						<th>Title</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>상표</th>
						<th>4500481620000</th>
						<td style="padding-left:50px">뷰카(ViewCAR)</td>						
					</tr>
					<tr>
						<th>특허</th>
						<th>10-1382498</th>
						<td style="padding-left:50px">자동차 에어백 동작 감지 시스템 및 처리방법</td>
					</tr>
					<tr>
						<th>특허</th>
						<th> 10-1618118</th>
						<td style="padding-left:50px">
							외부장치를 이용한 차체제어모듈 제어시스템 및
							그 방법
						</td>
					</tr>
					<tr>
						<th>특허</th>
						<th>10-2016-0028454</th>
						<td style="padding-left:50px">
							차량의 차체 제어모듈 제어 방법 및 장치,
							그를 이용한 차량 제어 시스템
						</td>
					</tr>
					<tr>
						<th>저작권</th>
						<th>C-2014-007978</th>
						<td style="padding-left:50px">스마트 드라이빙, 뷰카(ViewCAR)</td>
					</tr>
					<tr>
						<th>저작권</th>
						<th>C-2016-008356</th>
						<td style="padding-left:50px">
							차량운전습관 분석시스템,
							VDAS(Vehicle Driving Analysis System)
						</td>
					</tr>
					<tr>
						<th>국제표준</th>
						<th>ISO/WD20530</th>
						<td style="padding-left:50px">
							Intelligent Transport Systems — Information for
							emergency service support via
							Personal ITS station —
							General requirements and technical definition
						</td>
					</tr>						
					<tr>
						<th>인증</th>
						<th>881</th>
						<td style="padding-left:50px">위치기반서비스 사업</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>