<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02");
	initMenuSel("LM02_05");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			자스텍M 소개
		</div>
		<div class="sub_title7_2">
			자스텍M은 자동차 ICT 제품과 솔루션을 보유한
			<span>IoT Total Solution & Service</span> 기업입니다. 			
		</div>
		<div class="sub01_box1 mab35">
			<p>
				자스텍M은 안전하고 편리한 스마트 드라이빙 서비스를 위해
				앞선 기술과 끊임없는 연구개발을 바탕으로 초일류 서비스로 발전해 나갈 것을 약속 드립니다.
			</p>
		</div>
		<div class="movie_area">
			<iframe width="800" height="452" src="https://www.youtube.com/embed/h4PhguSMvTk" frameborder="0" allowfullscreen></iframe>
		</div>
		<div class="sub02_box5 mab75">
			<h1>회사소개서 다운로드</h1>
			<p>자스텍M의 회사소개 및 사업목표, 제품ㆍ서비스 소개 및 추진 전략 등 자세한 내용을 만나 보실 수 있습니다.</p>
			<div class="btn">
				<a href="${pageContext.request.contextPath}/common/ref/JASTECM_Corporate Overview(B2B_ver1.1).pdf" target="_blank">KOREAN</a><a href="${pageContext.request.contextPath}/common/ref/JASTECM_Cooperative Overview(B2B_ver1.1_Eng).pdf" target="_blank">ENGLISH</a>
			</div>
		</div>
		<div class="sub02_box5 mab75">
			<h1>CI 다운로드</h1>
			<p>차량IT전문기업 ㈜자스텍의 로고와 ‘Mobile(통신∙자동차)’의 M이 결합된 CI를 소개합니다.</p>
			<img src="${pageContext.request.contextPath}/common/images/sub02_img7.jpg" alt="" class="mab14" />
			<div class="btn">
				<a href="${pageContext.request.contextPath}/common/ref/jastecM_logo.jpg" target="_blank">JPG</a><a href="${pageContext.request.contextPath}/common/ref/jastecM_ai.ai" target="_blank">AI</a>
			</div>
		</div>
		<div class="sub02_box5">
			<h1>슬로건 소개</h1>
			<p>
				고객의 안전하고 편리한 습관을 생활화 시키는 패러다임 쉬프트!<br>
				4차 과학혁명의 새로운 사고전환, 자스텍M이 함께 합니다.
			</p>
			<img src="${pageContext.request.contextPath}/common/images/sub02_img8.jpg" alt="" />
		</div>
	</div>