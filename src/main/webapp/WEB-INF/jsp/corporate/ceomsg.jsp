<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02");
	initMenuSel("LM02_03");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			CEO 인사말
		</div>
		<div class="sub_title7_2">
			늘 변화와 새로운 도전을 통해  <span>인류의 안전</span>과 <span>녹색성장</span>에 기여하는
			세계적인 IoT 전문기업이 되도록 모든 임직원이 함께 노력하겠습니다. 
		</div>
		<div class="intro_box1">
			1990년이후 줄곧 차량IT전문기업으로서의 축척된 기술과 노하우를 보유한 ㈜자스텍에서
			IoT(Internet of thing) 솔루션 전문기업인 ㈜인피니티플러스의 기술과 인력을 흡수하여
			과학혁명의 새로운 사고전환의 중심에 있는 ‘이동하는 사람과 사물을 위한 제품과 서비스’ 전문기업인
			㈜자스텍엠을 설립하여 고도화된 실천 전략과 함께 제 4차 산업혁명을 이끌겠습니다.
		</div>
		<div class="intro_box2">
			<p>자스텍에서 그동안 개발된 다양한 자동차 IoT 제품을 진화하는 사물인터넷 환경에 맞춰 보다 저렴하고 최적화된 자동차 IoT 제품으로
			세계 시장을 선도하겠습니다.</p>
			<p>커넥티드카에서 자율주행자동차까지 자스텍M의 모든 제품과 서비스는 사람의 안전과 편의를 최우선으로 FUN & WIT한
			서비스 구현을 통한 ‘머무를 공간’으로서 기술의 가치를 높히겠습니다.</p>
			<p>축척되는 기술과 데이터를 기반으로 학습하여 진화하고, 스스로 깨어나 판단 ∙ 코칭하는 제품 ∙ 서비스로 발전하겠습니다.</p>
			<p>사람의 실수까지도 사전 감지하는 Nudge(오류방지) 기능 제품 ∙ 서비스로 진화하여 사고율 ZERO에 도전하겠습니다.</p>
			<p>웨어러블을 넘어 생체인식까지 모든 스마트 단말과 연결되는 제품과 기술로 발전하여 새로운 가치창조를 이끄는 회사로 거듭 나겠습니다.</p>
		</div>
		<div class="intro_box3">
			자스텍M 대표이사<img src="${pageContext.request.contextPath}/common/images/sign.jpg" alt="" />
		</div>
	</div>