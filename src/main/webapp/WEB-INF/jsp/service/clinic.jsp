<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM06");
	initMenuSel("LM06_03");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			ViewCAR 클리닉
		</div>
		<div class="sub_title7">
			자동차 정비업소의 필수품, <span>ViewCAR 클리닉<span>
		</div>
		<div class="video_area">
			<img src="${pageContext.request.contextPath}/common/images/sub04_img6.jpg" alt="" />
		</div>
		<div class="sub04_box1">
			<h1>실시간 차량상태 확인</h1>
			<h2>
				간단하게 탈부착이 가능한 챠량단말(Von-시리즈)을 통한<br>
				고객차량의 상태, 주행거리에 따른 소모품상태, 고장유무를<br>
				실시간 확인할 수 있습니다.
			</h2>
			<div><img src="${pageContext.request.contextPath}/common/images/sub04_img7.jpg" alt="" /></div>
			<h2>
				<span class="s1">※ 비단말형 일반 정비고객차량까지 지원합니다.</span>
			</h2>
			<div class="sub04_box1_1 matm90">
				<h1>고객 스마트폰을 통한 업그레이드 서비스</h1>
				<h2>
					고객 스마트폰 App 마이카클리닉을 통한 정비예약 ∙ 상담, 정비내역
					전송 등 고객차량의 주치의가 되십시오. 고객과 더욱 가까워집니다.
				</h2>
				<div><img src="${pageContext.request.contextPath}/common/images/sub04_img8.jpg" alt="" /></div>
			</div>
			<div class="sub04_box1_2">
				<h1>차량 정비관리에 꼭 필요한 기능을 모아!</h1>
				<h2>
					웹 클라우드 기반 정비관리서비스를 통한 자동완성형<br>
					정비항목 입력, 결제수단별 장부관리 등 맞춤형<br>
					솔루션을 제공합니다.
				</h2>
			</div>
		</div>
		<div class="sub04_btn sub04_btn2">
			<a id="LM05_04" data-page="/solution/clinic" class="callPage">ViewCAR 클리닉 서비스 상세보기  ></a><a href="http://clinic.viewcar.co.kr/">ViewCAR 클리닉 사이트 바로가기  ></a>			
		</div>
	</div>
