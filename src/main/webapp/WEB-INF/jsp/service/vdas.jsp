<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM06");
	initMenuSel("LM06_02");
});
</script>

<div class="sub_layout">
	<div class="sub_title1">
		<span></span>
		ViewCAR VDAS
	</div>
	<div class="sub_title7">
		렌터차량부터 법인차량까지 차량관리 안심서비스, <span>ViewCAR VDAS</span>
	</div>
	<div class="video_area mab68">
		<img src="${pageContext.request.contextPath}/common/images/sub04_img3.jpg" alt="" />
	</div>
	<div class="sub04_box1">
		<h1>간단한 장착, Von-시리즈</h1>
		<h2>
			기업의 니즈에 특화된 차량단말기를 차량 표준진단포트인<br>OBD-II 단자에 장착만으로 원하는 차량관리 할 수 있습니다.
			<span>※ 통신모듈, 릴레이박스 등의 설치를 위해 별도 개조ㆍ설치작업이 필요 없습니다.</span>
		</h2>
		<div><img src="${pageContext.request.contextPath}/common/images/sub04_img4.jpg" alt="" /></div>
		<div class="sub04_box1_1">
			<h1>낮은 이용료, 높은 퀄리티</h1> 
			<h2>
				월정액 부담이 발생하는 통신형 제품은 꼭 필요한 경우에 설치!<br>
				기업의 특성에 맞는 서비스를 저렴한 비용과 고급화된 서비스로<br>
				가치를 높혔습니다.
			</h2>
			<div><img src="${pageContext.request.contextPath}/common/images/sub04_img5.jpg" alt="" /></div>
		</div>
		<div class="sub04_box1_2">
			<h1>차량안전  강화  ∙ 업무효율 증가</h1>
			<h2>
				운전습관 개선을 통한 <strong>법인차량의 높은 사고율 감소</strong> 및 배차,<br>
				차량관리 간소화를 통한 업무효율 증가효과까지.<br>
				1석9조 차량관리서비스, VDAS!
			</h2>
		</div>
	</div>
	<div class="sub04_btn sub04_btn2">	
		<a id="LM05_03" data-page="/solution/vdas" class="callPage">ViewCAR VDAS 서비스 상세보기  ></a><a href="http://vdas.viewcar.co.kr/vdas">ViewCAR VDAS 사이트 바로가기  ></a>
	</div>
</div>