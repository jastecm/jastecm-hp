<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="description" content="생활에서~경제로! 자동차Iot전문기업">

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
var _defaultPath = "${path}";
</script>

<script charset="utf-8" src="${pageContext.request.contextPath}/common/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/icheck.js"></script>
<script src="${pageContext.request.contextPath}/common/js/jquery.bxslider.min.js"></script>
<script src="${pageContext.request.contextPath}/common/js/base.js" language="javascript" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content.css" />

</head>




<script type="text/javascript">
$(document).ready(function(){
	
	$(".lev2,.lev3,.lev4,.lev5,.lev6,.lev7").hide();
	$(".result").hide();
	$(".sel1").on('change', function () { //device
		$(".lev2,.lev3,.lev4,.lev5,.lev6,.lev7,.result").hide();
	
        if($(this).val().length == 0) return;
        
		var url = "https://vdaspro.viewcar.co.kr/vdasPro/api/vehicleDb/ko/manufacture";
		var param = {lev:"2"};
		var target = "sel2";
		var listType = "1";
		var defaultVal = "S";
		
		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
			
	        $(".sel2").on('change', function () { //제조사
	    		var url = "https://vdaspro.viewcar.co.kr/vdasPro/api/vehicleDb/ko/manufacture/"+$(".sel2").val()+"/modelMaster";
	        	var param = {lev:"3",param1 : $(".sel2").val()};
	    		var target = "sel3";
	    		var listType = "1";
	    		var defaultVal = "S";
	    		
	    		$(".lev3,.lev4,.lev5,.lev6,.lev7,.result").hide();
    	        if($(this).val().length == 0) return;
    	        
	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){ 
	    			
	    	        $(".sel3").on('change', function () {//차종
	    	        	var url = "https://vdaspro.viewcar.co.kr/vdasPro/api/vehicleDb/ko/manufacture/"+$(".sel2").val()+"/modelMaster/"+$(".sel3").val()+"/year";
	    	    		var param = {lev:4,param1 : $(".sel3").val()};
	    	    		var target = "sel4";
	    	    		var listType = "1";
	    	    		var defaultVal = "S";
	    	    		
	    	    		$(".lev4,.lev5,.lev6,.lev7,.result").hide();
    	    	        if($(this).val().length == 0) return;
    	    	        
	    	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
	    	    			
	    	    			
	    	    	        $(".sel4").on('change', function () { //연식
	    	    	        	var url = "https://vdaspro.viewcar.co.kr/vdasPro/api/vehicleDb/ko/manufacture/"+$(".sel2").val()+"/modelMaster/"+$(".sel3").val()+"/year/"+$(".sel4").val()+"/modelHeader";
	    	    	    		var param = {lev:5,param1 : $(".sel3").val(),param2 : $(".sel4").val()};
	    	    	    		var target = "sel5";
	    	    	    		var listType = "1";
	    	    	    		var defaultVal = "S";
	    	    	    		
	    	    	    		$(".lev5,.lev6,.lev7,.result").hide();
    	    	    	        if($(this).val().length == 0) return;
    	    	    	        
	    	    	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
	    	    	    			
	    	    	    	        $(".sel5").on('change', function () { //헤더
	    	    	    	        	var url = "https://vdaspro.viewcar.co.kr/vdasPro/api/vehicleDb/ko/manufacture/"+$(".sel2").val()+"/modelMaster/"+$(".sel3").val()+"/year/"+$(".sel4").val()+"/modelHeader/"+$(".sel5").val()+"/fuelType";
	    	    	    	    		var param = {lev:6,param1 : $(".sel5").val(),param2 : $(".sel4").val()};
	    	    	    	    		var target = "sel6";
	    	    	    	    		var listType = "1";
	    	    	    	    		var defaultVal = "S";
	    	    	    	    		
	    	    	    	    		$(".lev6,.lev7,.lev8,.result").hide();
    	    	    	    	        if($(this).val().length == 0) return;
    	    	    	    	        
	    	    	    	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
	    	    	    	    			
	    	    	    	    			
	    	    	    	    	        $(".sel6").on('change', function () { //연료
	    	    	    	    	        	var url = "https://vdaspro.viewcar.co.kr/vdasPro/api/vehicleDb/ko/manufacture/"+$(".sel2").val()+"/modelMaster/"+$(".sel3").val()+"/year/"+$(".sel4").val()+"/modelHeader/"+$(".sel5").val()+"/volume";
	    	    	    	    	    		var param = {lev:7,param1 : $(".sel5").val(),param2 : $(".sel4").val()};
	    	    	    	    	    		var target = "sel7";
	    	    	    	    	    		var listType = "1";
	    	    	    	    	    		var defaultVal = "S";
	    	    	    	    	    		
	    	    	    	    	    		$(".lev7,.result").hide();
    	    	    	    	    	        if($(this).val().length == 0) return;
    	    	    	    	    	       
    	    	    	    	    	        
	    	    	    	    	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
	    	    	    	    	    			
	    	    	    	    	    	        $(".sel7").on('change', function () { //배기량
    	    	    	    	    	    	        if($(this).val().length == 0) return;
	    	    	    	    	    	    		var param = {
	    	    	    	    	    	    				param1 : $(".sel2").val()
        	    	    	    	    	    				,param2 : $(".sel3").val()
        	    	    	    	    	    				,param3 : $(".sel4").val()
        	    	    	    	    	    				,param4 : $(".sel5").val()
        	    	    	    	    	    				,param5 : $(".sel6").val()
        	    	    	    	    	    				,param6 : '자동'
        	    	    	    	    	    				,param7 : $(".sel7").val()
	    	    	    	    	    	    		};
	    	    	    	    	    	    		var url = "https://vdaspro.viewcar.co.kr/vdasPro/api/vehicleDbCode/ko/"+$(".sel2").val()+"/"+$(".sel3").val()+"/"+$(".sel4").val()+"/"+$(".sel5").val()+"/"+$(".sel6").val()+"/자동/"+$(".sel7").val();
	    	    	    	    	    	    		
	    	    	    	    	    	    		$.ajax({
	    	    	    	    	    	    			url:url
	    	    	    	    	    	    			,dataType : 'json'
	    	    	    	    	    	    			,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
	    	    	    	    	    	    			,type:"POST"
	    	    	    	    	    	    			,data:{}
	    	    	    	    	    	    			,cache:!1
	    	    	    	    	    	    			,success:function(rtv){
	    	    	    	    	    	    				console.log(rtv);
	    	    	    	    	    	    				var bcm = rtv.result.BCM_SUPPORT;
		    	    	    	    	    	    			
		    	    	    	    	    	    			if(bcm=="Y"){
		    	    	    	    	    	    				$(".resultP").text("지원되는 차량입니다.");
		    	    	    	    	    	    				$(".resultPSub").text("ViewCAR PRO와 함께 스마트한 차량관리를 시작해보세요.");
		    	    	    	    	    	    			}else{
		    	    	    	    	    	    				$(".resultP").text("지원되는 차량입니다.");
		    	    	    	    	    	    				$(".resultPSub").text("ViewCAR PRO와 함께 스마트한 차량관리를 시작해보세요.");
		    	    	    	    	    	    			}
		    	    	    	    	    	    			
		    	    	    	    	    	    			$(".result").show();
	    	    	    	    	    	    			}
	    	    	    	    	    	    			,error:function(t){alert("ajax error")}
	    	    	    	    	    	    		});
	    	    	    	    	    	    	}); 
	    	    	    	    	    			$(".lev7").show();
	    	    	    	    	    		});
	    	    	    	    	    	}); 
	    	    	    	    			$(".lev6").show();
	    	    	    	    	        $(".lev7").hide();
	    	    	    	    		});
	    	    	    	    	});	    	    	    	        
	    	    	    			$(".lev5").show();
	    	    	    	        $(".lev6,.lev7").hide();
	    	    	    		});
	    	    	    	});
	    	    			$(".lev4").show();
	    	    	        $(".lev5,.lev6,.lev7").hide();
	    	    		});
	    	    	});
	    			$(".lev3").show();
	    			$(".lev4,.lev5,.lev6,.lev7").hide();
	    		});
	    	});
			$(".lev2").show();
			$(".lev3,.lev4,.lev5,.lev6,.lev7").hide();
		});
	});
	$(".car_com").css({
		"width":"100%"
		, "height":"34px"
		, "background-color":"#AAAAAA"
		, "text-align":"-webkit-center"
		, "font-family":"initial"
		, "font-weight":"900"
		, "color":"white"
		, "font-size":"130%"
		, "white-space":"nowrap"
		, "overflow":"hidden"
		, "text-overflow":"ellipsis"
		, "line-height":"200%"}).trigger("create");
	
	$(".resultDiv").css({
		"width":"80%"
		, "height":"100%"
		, "margin":"auto"
		, "line-height":"200%"}).trigger("create");
	
	$(".resultP").css({
		"width":"100%"
		, "text-align":"-webkit-center"
		, "font-family":"initial"
		, "font-weight":"900"
		, "font-size":"125%"
//		, "white-space":"nowrap"
//		, "overflow":"hidden"
//		, "text-overflow":"ellipsis"
		, "background-color":"#0F84E7"
		, "color":"white"}).trigger("create");
	
	$(".resultPSub").css({
		"width":"100%"
		, "text-align":"-webkit-center"
		, "font-family":"initial"
		, "font-weight":"900"
		, "font-size":"120%"
//		, "white-space":"nowrap"
//		, "overflow":"hidden"
//		, "text-overflow":"ellipsis"
		, "background-color":"#0F84E7"
		, "color":"white"}).trigger("create");
	
	$(".selector").css({
		"width":"100%"
		, "height":"34px"
		, "background-color":"white"}).trigger("create");
});
</script>

<body style="background-color:transparent">
	<div class="sub04_box1" style="width:100%;height:300px;" >		
		<div style="float:left;width:50%;">
			<div class="lev1" style="text-align:right;width:100%;height:50px;">
				<div class="car_com" title="단말기 종류">단말기 종류</div>
			</div>
			<div class="lev2" style="text-align:right;height:50px;display:none;">
				<div class="car_com" title="차량 제조사">차량 제조사</div>
			</div>
			<div class="lev3" style="text-align:right;height:50px;display:none;">
				<div class="car_com" title="차종">차종</div>
			</div>
			<div class="lev4" style="text-align:right;height:50px;display:none;">
				<div class="car_com" title="연식">연식</div>
			</div>
			<div class="lev5" style="text-align:right;height:50px;display:none;">
				<div class="car_com" title="모델명">모델명</div>
			</div>
			<div class="lev6" style="text-align:right;height:50px;display:none;">
				<div class="car_com" title="연료 타입">연료 타입</div>
			</div>
			<div class="lev7" style="text-align:right;height:50px;display:none;">
				<div class="car_com" title="배기량">배기량</div>
			</div>
		</div>						
		<div style="float:left;width:50%">
			<div class="lev1" style="height:50px;width:100%;">
				<select class="sel1 selector" id="sel1">
					<option value="" selected>선택</option>
					<option value="VC200" >VC-200</option>
					<option value="von-S31" >von-S31</option>
				</select>
			</div>
			<div class="lev2" style="height:50px;width:100%;">
				<select class="sel2 selector" id="sel2"><option value="">선택</option></select>
			</div>
			<div class="lev3" style="height:50px;width:100%;">
				<select class="sel3 selector" id="sel3"><option value="">선택</option></select>
			</div>
			<div class="lev4" style="height:50px;width:100%;">
				<select class="sel4 selector" id="sel4"><option value="">선택</option></select>
			</div>
			<div class="lev5" style="height:50px;width:100%;">
				<select class="sel5 selector" id="sel5"><option value="">선택</option></select>
			</div>
			<div class="lev6" style="height:50px;width:100%;">
				<select class="sel6 selector" id="sel6"><option value="">선택</option></select>
			</div>
			<div class="lev7" style="height:50px;width:100%;">
				<select class="sel7 selector" id="sel7"><option value="">선택</option></select>
			</div>
		</div>
	</div>
	<div class="result" style="width:100%;height:44px;text-align:center;">
		<div class="resultDiv">
			<p class="resultP"></p>
			<p class="resultPSub"></p>
		</div>
	</div>
</body>
</html>
