<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM06");
	initMenuSel("LM06_01");
});
</script>

<div class="sub_layout">
	<div class="sub_title1">
		<span></span>
		ViewCAR
	</div>
	<div class="sub_title7">
		내손안의 스마트 드라이빙 솔루션 <span>ViewCAR</span>
	</div>
	<div class="video_area mab68">
		<iframe width="800" height="452" src="https://www.youtube.com/embed/h4PhguSMvTk" frameborder="0" allowfullscreen></iframe>		
	</div>
	<div class="sub04_box1">
		<h1>차량단말, Von-시리즈</h1>
		<h2>차량단말기를 표준 차량진단포트인 OBD-II 단자에 장착만으로<br>놀라운 커넥티드카 서비스를 경험하실 수 있습니다.</h2>
		<div><img src="${pageContext.request.contextPath}/common/images/sub04_img1.jpg" alt="" /></div>
		<div class="sub04_box1_1">
			<h1>커넥티드카 App, ViewCAR</h1>
			<h2>차량단말과 연결되어 당신의 스마트폰을 통해 보다 쉽고 편리하게 운전습관 모니터링과 자동차관리를 위한 경험을 하실 수 있습니다.</h2>
			<p>
				지금, 구글플레이에서 바로 다운받을 수 있습니다!
				<a href="https://play.google.com/store/apps/details?id=kr.co.infinityplus.viewcar&hl=ko"><img src="${pageContext.request.contextPath}/common/images/btn_down2.jpg" alt="" /></a>
			</p>
			<div><img src="${pageContext.request.contextPath}/common/images/sub04_img2.jpg" alt="" /></div>
		</div>
		<div class="sub04_box1_2">
			<h1>스마트폰과 자동차와의 커뮤니케이션</h1>
			<h2>
				차량의 센서∙운행∙고장 정보를 학습 진화형 A.I 자동차<br>
				플랫폼과 커뮤니케이션하여 안전하고 편리한 정보와<br>
				기능을 제공합니다.
			</h2>
		</div>
	</div>
	<div class="sub04_btn">
		<a href="http://www.viewcar.co.kr/viewcar-clinic/wwwroot/viewCarFeature.do#pos_top">ViewCAR 서비스 상세보기  ></a><a href="http://www.viewcar.co.kr/viewcar-clinic/wwwroot/index.do">ViewCAR 사이트 바로가기  ></a>
	</div>
</div>