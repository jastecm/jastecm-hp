<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_08");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			V Comm
		</div>
		<div class="sub_title7_2">
			V2X 기반 솔루션, V COMM
		</div>
		<div><img src="${pageContext.request.contextPath}/common/images/sub05_img54.jpg" alt="" /></div>
		<div class="sub_tit1">서비스 개요</div>
		<div class="sub05_box6">
			<p>차량과 도로, 차량과 차량간 무선통신기술 WAVE(Wireless Access in Vehicular Environment )는 IEEE 기술표준이며 자동차 사전 사고예방을 위한 차세대 핵심기술입니다.</p>
			<p>고속이동성(최대 200km/h)과 통신 교환기술이 짧은 차량 도로에서 차량간 통신을 위해 5.8GHz 대역 RF, OFDM 모뎀, 차량간 통신 MAC과 라우팅 기술이 필요하며 IEEE 802.11a/g 무선랜 기술을 차량 환경에 맞도록 개량된 Broadcasting 통신기술을 기반으로 합니다.</p>
		</div>
		<br><br><br><br><br>
		<div class="sub_tit1">핵심 추진 영역</div>
		<img style="margin-left: 120px" src="${pageContext.request.contextPath}/common/images/sub05_img55.jpg" alt="" />
		<br><br><br><br><br>
		<div class="sub_tit1">서비스 특징</div>
		<img style="margin-left: 120px" src="${pageContext.request.contextPath}/common/images/sub05_img56.jpg" alt="" />
		<br><br><br><br><br><br>
		<div class="sub_tit1">VCOMM의 솔루션 영역 ∙ 장점</div>
		<div class="sub05_box6">
			<p><strong class="s1">표준 OBD-II 진단포트를 통해 차량크기, 차량속도, RPM. 변속, 주행방향, 브레이크 상태, 차량 이상(고장), 차량사고, 차량위험상황정보(에어백 전개, 비상등 점멸), 탑승 등  10~100 msec 단위의 다양한 자동차 센서/운행정보를 수집/분석/가공</strong></p>
			<p>2006년 이후 한국 출시 전차종을 지원 (미국, 유럽의 경우 OBD-II 표준화가 이뤄진 2001년이후 전차종 지원)</p> 
			<p>SAE 국제표준 차량통신 지원 - ISO 9141-2, ISO 14230-4(KWP-2000), ISO 15765-4(CAN), SAE J1850(PWM/VPWM)</p>
			<p>WAVE Chipset 제조사와 긴밀한 유대관계</p>
			<p>유럽 및 국내외 Cooperative ITS Service Develop & evaluation Provider와의 긴밀한 네트워크 구축</p>
		</div>
	</div>