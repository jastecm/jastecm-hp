<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_01");
});
</script>
	<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			솔루션 소개
		</div>
		<div class="sub_title7_2">
			당사의 제품과 서비스는 <b>무선통신으로 차량의 센서 ∙ 운행 ∙ 고장 정보</b>를<br>
			수집/분석/가공하여 기존 출시된 자동차를 <span>최신 커넥티드카로 구성</span>할 수 있는<br>
			제품 ∙ 스마트 디바이스 어플리케이션, 융합 서비스 솔루션 입니다.
		</div>
		<ul class="sub05_box1">
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img1.jpg" alt="" />
				<h1>커넥티드카 솔루션,<br>운전하는 즐거움과 차량 관리의 합리적 제안 !</h1>
				<p>스마트폰 사용자 4500만 명, 자동차 등록대수 2,000만대 시대!<br>자동차-ICT 융합 서비스는 시대적  필수가 되었습니다.</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img2.jpg" alt="" />
				<h1>내손안의 스마트 드라이빙,<br>차량에 단말 장착만으로 준비 끝 ~</h1>
				<p>자동차-ICT 단말과 스마트폰 앱만 있다면<br>쉽고 편하게 차량관리와 운전습관을 개선할 수 있습니다!</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img3.jpg" alt="" />
				<h1>자동차-ICT 융합 서비스,<br>스마트폰과 자동차와의 커뮤니케이션!</h1>
				<p>학습 진화형 A.I 자동차 플랫폼과 커뮤니케이션하며<br>안전하고 편리한 솔루션을 제공합니다.</p>
			</li>
		</ul>
		<div class="sub_title6 mab13">
			<span>솔루션 바로가기</span><strong></strong>
		</div>
		<ul class="sub05_box2">
			<li>
				<h1>차량관리 스마트폰 App 솔루션, VSAPP</h1>
				<p>차량단말과 스마트폰 어플리케이션을 연동하여 보다 쉽고 편리하게 운전습관 모니터링과<br>자동차 관리를 할 수 있습니다.</p>
				<a data-page="/solution/apps" class="callPage">바로가기</a>
			</li>
			<li>
				<h1>FMS (Fleet Management System) 솔루션, VDAS</h1>
				<p>차량관제 서비스 시장의 확대 및 회사차량 과속방지, 운전습관 분석 Needs에 부응하기 위해 개발된<br>국세청 업무용 승용차 운행기록부 완벽 대응을 위한 차량관제서비스 특화 솔루션입니다.</p>
				<a data-page="/solution/vdas" class="callPage">바로가기</a>
			</li>
			<li>
				<h1>차량 정비관리 솔루션, VClinic</h1>
				<p>차량 IoT단말 보유 고객 및 일반고객의 차량정비와 고객관리를 위한 웹기반 자동차정비<br>관리 프로그램입니다.</p>
				<a data-page="/solution/clinic" class="callPage">바로가기</a>
			</li>
			<li>
				<h1>카쉐어링 솔루션, VShare</h1>
				<p>카쉐어링 솔루션 VShare로 손쉬운 카쉐어링 솔루션 구축 : OBD-II 단자 장착만으로 카쉐어링<br>서비스 구현합니다.</p>
				<a data-page="/solution/share" class="callPage">바로가기</a>
			</li>
			<li>
				<h1>UBI(Usage Based Insurance) 솔루션, VScoring</h1>
				<p>차량이용을 모니터할 수 있는 OBD를 설치하거나 스마트폰을 이용하여 운행관련 데이터를 수집하고<br>분석하여 보험료  할인 범위를 결정하게 되며 본 솔루션은 고객의 운전습관을 수집/분석/가공하여<br>운전습관을 스코어링하는 솔루션입니다.</p>
				<a data-page="/solution/scoring" class="callPage">바로가기</a>
			</li>
			<li>
				<h1>e-Call(Emergency Call) 솔루션, VCall</h1>
				<p>심각한 사고 또는 위급상황 발생시 자동 및 수동으로 긴급구난을 요청하여 탑승자를 빠르게 구난하는<br>교통관련  핵심안전 서비스로서 당사의 IoT 제품 및 솔루션은 After Market에 특화되어 있습니다.</p>
				<a data-page="/solution/call" class="callPage">바로가기</a>
			</li>
			<li>
				<h1>V2X 기반 솔루션, VCOMM</h1>
				<p>차량과 도로, 차량과 차량간 무선통신기술 WAVE(Wireless Access in Vehicular Environment )는<br>IEEE 기술표준이며 자동차 사전 사고예방을 위한 차세대 핵심기술입니다.</p>
				<a data-page="/solution/comm" class="callPage">바로가기</a>
			</li>
		</ul>
		<div class="sub05_box3">
			<h1>솔루션 구매 문의</h1>
			<ul>
				<li>
					<span class="img"><img src="${pageContext.request.contextPath}/common/images/sub05_img8.jpg" alt="" /></span>
					<h2>솔루션 구매 문의</h2>
					<div>
						<span class="w209">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img5.jpg" alt="" />박영민(전략마케팅팀/과장)
						</span><span class="w146">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img6.jpg" alt="" />02-452-9710
						</span><span>
							<img src="${pageContext.request.contextPath}/common/images/sub05_img7.jpg" alt="" />youngmin.park@jastecm.com
						</span>
					</div>
				</li>
			</ul>
		</div>
	</div>
	