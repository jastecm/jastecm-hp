<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_06");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			V Scoring
		</div>
		<div class="sub_title7_2">
			UBI(Usage Based Insurance) 솔루션, V Scoring
		</div>
		<div><img src="${pageContext.request.contextPath}/common/images/sub05_img48.jpg" alt="" /></div>
		<div class="sub_tit1">서비스 개요</div>
		<div class="sub05_box6">
			<p>차량이용을 모니터 할 수 있는 OBD를 설치하거나 스마트폰을 이용하여 운행관련 데이터를 수집하고 분석하여 보험료 할인 범위를 결정하게 되며 본 솔루션은 고객의 운전습관을 수집/분석/가공하여 운전습관을 스코어링하는 솔루션입니다.</p>
			<p>도로교통공단과 교통사고분석시스템(TAAS)기반한 UBI를 위한 위험운전 분석 솔루션 공동개발하였습니다.</p>
		</div>
		<br><br><br><br><br>
		<div class="sub_tit1">자동차 보험비 책정 추이 및 할인율</div>
		<img style="margin-left:120px;" src="${pageContext.request.contextPath}/common/images/sub05_img49.jpg" alt="" />
		<div class="sub05_box6">
			<p>미국·영국의 경우 <strong class="s1">UBI 보험가입율 8.4%</strong> (2014년 말 기준)로 이미 Mass단계 진입</p>
			<p>1998년 사용거리기반 보험을 처음 시작한 美. Progressive는 2012년 $1billion 매출 기록</p>
			<p>2020년 미국은 자동차보험 가입자 중 25%가 사용기반 보험에 가입할 것으로 예측, $30 billion/년 규모 성장 예상. 통신사와 보험사의 공동상품형태로 보급 추진</p>
		</div>
		<br><br><br>
		<table class="table3">
			<colgroup>
				<col width="182" />
				<col width="*" />
				<col width="229" />
			</colgroup>
			<thead>
				<tr>
					<th>보험사</th>
					<th>결정요인</th>
					<th>할인율</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Progressive</td>
					<td>급제동, 주행거리, 운행시간대</td>
					<td>최대(30%)</td>
				</tr>
				<tr>
					<td>State Farm</td>
					<td>주행거리, 회전, 급가속, 급제동, 과속, 운행시간대</td>
					<td>최초(5%), 최대(50%)</td>
				</tr>
				<tr>
					<td>Allstate</td>
					<td>급제동, 과속, 주행거리, 운행시간</td>
					<td>최초(10%), 최대(30%)</td>
				</tr>
				<tr>
					<td>Hartford</td>
					<td>운행위치 및 시간대, 과속, 급가속, 급제동</td>
					<td>최초(5%), 최대(25%)</td>
				</tr>
			</tbody>
		</table>
		<br><br><br><br><br><br>
		<div class="sub_tit1">V Scoring 의 분석 방법 및 스코어링 유형</div>
		<img src="${pageContext.request.contextPath}/common/images/sub05_img50.jpg" alt="" />
	</div>