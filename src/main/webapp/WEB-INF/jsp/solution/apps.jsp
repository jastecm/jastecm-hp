<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_02");
});
</script>
	<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			스마트폰 어플리케이션
		</div>
		<div class="sub_title7_2">
			차량관리 스마트폰 APP 솔루션, VSAPP
		</div>
		<div class="mab90"><img src="${pageContext.request.contextPath}/common/images/sub05_img9.jpg" alt="" /></div>
		<div class="sub_tit1">세부기능</div>
		<div class="sub05_box4">
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img10.jpg" alt="" /></dt>
				<dd>
					<h1>편리한 타임라인</h1>
					<p>
						사용자가 사용한 모든 서비스 및 이벤트를
						날짜와 선택 옵션별로 간편하게 타임라인
						으로 표시합니다. <span>사용패턴 기반으로 학습</span>된 똑똑한 타임라인을 만나보세요.
					</p>
				</dd>
			</dl>
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img11.jpg" alt="" /></dt>
				<dd>
					<h1>에코ㆍ안전 운전 코칭</h1>
					<p>
						2016년 3월 도로교통공단(경찰청)과 공동
						개발한 VDAS를 통한 안전지수를 제공하여
						<span>공공성을 확보</span>했습니다. 운행정보는 기간
						별로 확인 할 수 있습니다.
					</p>
				</dd>
			</dl>
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img12.jpg" alt="" /></dt>
				<dd>
					<h1>도어 개폐</h1>
					<p>
						<span>세계최초</span>로 차량의 개조없이 OBD단자에
						단말기를 꽂기만 하면 어플과 위젯을 통해
						차량 도어 개폐를 할 수있습니다.
						(차량에 5~10m내로 접근)
					</p>
				</dd>
			</dl>
		</div>
		<div class="sub05_box4">
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img13.jpg" alt="" /></dt>
				<dd>
					<h1>주차위치확인ㆍ알림</h1>
					<p>
						차량의 시동이 꺼진 마지막 지점을 주차위치
						로 저장하여 지도에 표시합니다. 또한 원하
						는 시간만큼 설정하여 무료주차 알람도 받
						을 수 있습니다.
					</p>
				</dd>
			</dl>
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img14.jpg" alt="" /></dt>
				<dd>
					<h1>HUD</h1>
					<p>
						주행중 다양한 차량 센서정보 및 운행정보
						를 쉽게 파악 할 수 있게 HUD화면을 제공
						합니다. 세로와 가로버전 화면을 제공하여
						사용자가 선택 할 수 있습니다.
					</p>
				</dd>
			</dl>
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img15.jpg" alt="" /></dt>
				<dd>
					<h1>차량진단ㆍ마이카클리닉</h1>
					<p>
						차량진단 항목 클릭 실행 3~7천여 항목의
						<span>부품과 기능들의 이상유무 점검</span>하고 마이
						카클리닉을 통해 간편하게 정비예약 및
						서비스 문의를 할 수 있습니다.
					</p>
				</dd>
			</dl>
		</div>
		<div class="sub05_box4 mab35">
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img16.jpg" alt="" /></dt>
				<dd>
					<h1>소모품관리</h1>
					<p>
						주행거리에 따라 소모품 교체시기를 확인
						하고 커머스 서비스 연계를 통한 고객 편의
						및 혜택 정보를 제공 합니다.
					</p>
				</dd>
			</dl>
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img17.jpg" alt="" /></dt>
				<dd>
					<h1>긴급SOS (e-Call)</h1>
					<p>
						<span>세계최초</span>로 자동차 에어백 전개사고 발생시
						자동으로 긴급SOS 메세지 전송 합니다.
						당사 특허기준 ISO/NP20530 국제 표준화
						진행 중입니다.
					</p>
				</dd>
			</dl>
		</div>
		<div class="sub_tit1">APP 자동실행 기능</div>
		<ul class="sub05_box5">
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img18.jpg" alt="" /></div>
				<p>차량과 VID 연결</p>
			</li>
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img22.jpg" alt="" /></div>
			</li>
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img19.jpg" alt="" /></div>
				<p>엔진시동ㆍ정지 감지<br>(1~2초 이내 자동 감지)</p>
			</li>
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img22.jpg" alt="" /></div>
			</li>
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img20.jpg" alt="" /></div>
				<p>앱 자동 실행 및 종료</p>
			</li> 
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img22.jpg" alt="" /></div>
			</li>
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img21.jpg" alt="" /></div>
				<p>운행내역 및<br> 주차위치 기록</p>
			</li>
		</ul>
		<div class="sub05_box6 mab80">
			<p>자동실행 기능을 통해 사용자(운전자)는 안전운전에만 집중하실 수 있습니다.</p>
			<p>자동실행 기능을 통해 사용자에게는 다양하고 유용한 정보를 제공하고, 사업자에게는 사업모델에 알맞은 다양한 비즈니스모델을<br>발굴을 하실 수 있습니다.</p>
			<p>스마트폰 메모리 최적화 기술을 통해 사용자 스마트폰 배터리 사용이 적습니다.</p>
		</div>
		<div class="sub_tit1">VSAPP 특징</div>
		<div class="sub05_box6">
			<p>자동차  IoT 단말(Von-시리즈)를  완벽 지원하며  타사의 IoT 단말도 지원예정입니다.</p>
			<p>2015~2016년 미래창조과학부  K-Global 스마트·모바일 스타기업 육성사업 지원을 통해 기능 향상되었습니다.</p>
			<p>고객사의 솔루션 도입 및 유지보수 비용 절감을 위해 ASP(Application Service Provider)로 제공하고 있습니다.</p>
		</div>
	</div>
	