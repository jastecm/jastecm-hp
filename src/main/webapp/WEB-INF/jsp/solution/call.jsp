<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_07");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			V Call
		</div>
		<div class="sub_title7_2">
			e-Call(Emergency Call) 솔루션, V Call
		</div>
		<div><img src="${pageContext.request.contextPath}/common/images/sub05_img51.jpg" alt="" /></div>
		<div class="sub_tit1">서비스 개요</div>
		<div class="sub05_box6">
			<p>심각한 사고 또는 위급상황 발생시 자동으로 긴급구난을 요청하여 탑승자를 빠르게 구난하는 교통관련  핵심안전 서비스로서 당사의 IoT 제품 및 솔루션은 After Market에 특화되어 있습니다.</p>
			<p>ETSI·CEN IVS(BM용 e-Call) 관련표준 ISO 표준화 추진중, UN 산하 GRSG AECS informal Group AECD (Accident Emergency Call Device) Task Force를 구성하여 표준화 제정을 추진중입니다.</p>
			<p>한국의 경우, 국토교통부 교통/자동차 정책의 주요내용으로 언론보도에 의하면 2018~9년 법제화 예정입니다.</p>
			<p><strong class="s1">당사 보유 특허를 기반으로 국가기술표준원 승인을 거쳐 ISO/WD20530 국제표준화 절차가 진행중입니다.</strong></p>
		</div>
		<br><br><br><br><br>
		<div class="sub_tit1">핵심 추진 영역</div>
		<img style="margin-left:120px;" src="${pageContext.request.contextPath}/common/images/sub05_img52.jpg" alt="" />
		<br><br><br><br><br>
		<div class="sub_tit1">기대효과</div>
		<div class="sub05_box6">
			<p>자동차 1만대 당 사망자 수 2.6명이라는 OECD 국가 1위의 불명예 탈피</p>
			<p>2017년까지 교통사고 <strong class="s1">사망자 30% 감소</strong>를 위한 국토교통부 추진전략에 부합</p>
			<p>연간 <strong class="s1">교통사고 비용 절감</strong>(사회적 비용 23조원, 이중 사망자와 부상자 발생으로 인한 인적 비용은 13조원)</p>
			<p>응급상황 즉각 대처로 <strong class="s1">‘골든타임’ 이내 운전자 인명구조</strong>를 통한 사망자수 감소</p>
			<p>차량긴급구난체계(e-Call) 서비스를 통해 <strong class="s1">사고대응시간을 50% 수준으로 경감</strong></p>
		</div>
		<br><br><br><br><br><br>
		<div class="sub_tit1">기술개발의 최종목표</div>
		<img style="margin-left:120px;" src="${pageContext.request.contextPath}/common/images/sub05_img53.jpg" alt="" />
	</div>