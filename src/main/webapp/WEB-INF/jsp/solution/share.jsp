<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_05");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			V Share
		</div>
		<div class="sub_title7_2">
			카쉐어링 솔루션, V Share
		</div>
		<div><img src="${pageContext.request.contextPath}/common/images/sub05_img38.jpg" alt="" /></div>
		<div class="sub_tit1">국내 카쉐어링 솔루션의 현주소</div>
		<div class="sub05_box6">
			<p>복잡한 카쉐어링 솔루션 단말기 구성품 및 설치가 필요하고 (개조)작업을 통한 설치공임 증가 ∙ 차량 가격 하락</p>
		</div>
		<ul class="sub05_box8">
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img39.jpg" alt="" />
				<p>CSA Main</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img40.jpg" alt="" />
				<p>GPS 안테나</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img41.jpg" alt="" />
				<p>RFID 리더</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img42.jpg" alt="" />
				<p>Main 케이블</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img43.jpg" alt="" />
				<p>OBD 케이블</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img44.jpg" alt="" />
				<p>모뎀 안테나 </p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img45.jpg" alt="" />
				<p>키 고정 와이어</p>
			</li>
		</ul>
		<div class="sub05_box6">
			<p>스마트폰 어플과 단말기간의 높은 호환 오류율</p>
			<p>운전자 식별카드(RFID) 요구</p>
			<p>제한적 서비스 대상차량 모델</p>
			<p>잦은 차량센서 ∙ 운행정보 누락에 따른 RPM, 운행거리 부정확성</p>
		</div>
		<br><br><br><br><br><br><br>
		<div class="sub_tit1">카쉐어링 솔루션, V Share의 주요 특징</div>
		<p> </p>
		<div class="sub05_box6">
			<p>OBD-II단자에 V Share용 단말(통신형/비통신형 2종)장착만으로 카쉐어링 솔루션 완벽 대응</p>
		</div>
		<ul class="sub05_box8">
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img46.jpg" alt="" />
				<p>V Share용 단말</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img47.jpg" alt="" />
				<p>OBD-II 단자에 장착</p>
			</li>
		</ul>
		<div class="sub05_box6">
			<p>표준 MQTT프로토콜 및 <strong class="s1">단말내 고객사 프로그램 설치를 통한 다양한 고객 Needs 대응</strong></p>
			<p>스마트폰만으로 원격시동제어 ∙ 스마트키 ∙ 운전자 식별 기능 (RFID 카드 제거)</p>
			<p>OBD의 G센서 및 에어백 감지 센서로 긴급구난  e-Call 기능 구현</p>
			<p>다양한 서비스 호환 차량모델</p>
			<p>정확한 차량진단 및 15대 주요 소모품 내역 등</p>
		</div>
	</div>