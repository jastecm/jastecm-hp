<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_03");
});

$(document).ready(function(){
	$(".toggleMenu").on("click",function(){
		$(".toggleMenu").removeClass("active");
		$(this).addClass('active');
		$(".tabbable").find(".tab").hide();
		tab = $(this).data("menu");
		
		$(tab).show();
		
	});
	
	$(".toggleMenu").eq(0).trigger("click");
	
});
</script>
	<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			FMS
		</div>
		<div class="sub_title7_2">
			FMS (Fleet Management System) 솔루션, VDAS
		</div>
		<div><img src="${pageContext.request.contextPath}/common/images/sub05_img23.jpg" alt="" /></div>
		<div class="sub_tit1">서비스 개요</div>
		<div class="sub05_box6 mab80">
			<p>
				차량관제 서비스 시장의 확대 및 회사차량 과속방지, 운전습관 분석 Needs에 부응하기 위해 개발되었습니다.
				<span>
					- 2015~2016년 미래창조과학부  K-Global 스마트·모바일 스타기업 육성사업 지원<br>
					- 도로교통공단 교통사고분석시스템(TAAS)기반 위험운전 분석 솔루션 공동개발
				</span>
			</p>
			<p>국세청 업무용 승용차 운행기록부 완벽 대응을 위한 차량관제서비스 특화 솔루션입니다.</p>
			<p>고객사의 솔루션 도입 및 유지보수 비용 절감을 위해 ASP(Application Service Provider)로 제공하고 있습니다.</p>
		</div>
		<div class="sub_tit1">서비스 기대효과</div>
		<ul class="sub05_box6 mab70">
			<li>
				<p>에코운전  → <b>주유비 절감</b></p>
				<p>차량 운행 및 정차 모니터링 → <b>공회전 감소</b></p>
				<p>업무 외 운행감소 → <b>주행거리 감소</b></p>
				<p>차량운행시간 감소로 인한 차량배차 원활 → <b>차량 활용률 증가</b></p>
				<p>배차 및 차량관리 간소화 → <b>업무효율 증가</b></p>
			</li> 
			<li>
				<p>운전습관 모니터링 및 과태료 관리 → <b>과속감소</b></p>
				<p>운전습관 분석 → <b>좋은 운전습관 증가</b></p>
				<p>운행일지, 정비관리 → <b>차량관리 효율 증가</b></p>
				<p>운행률 관리로 효율적 차량보유 → <b>차량 증차 및 감차효율 증가</b></p>
			</li>
		</ul>
		<div class="sub_tit1">서비스 상세</div>

	<div class="tabbable"> 
		<ul class="tabs">
			<div class="sub_tab">
				<a data-menu="#menu1" class="toggleMenu" style="width: 130px">관리 플랫폼</a><a data-menu="#menu2" class="toggleMenu" style="width: 130px">운행일지 기록</a><a data-menu="#menu3" class="toggleMenu" style="width: 130px">운행경비 관리</a><a data-menu="#menu4" class="toggleMenu" style="width: 130px">차량운행 모니터링</a><a data-menu="#menu5" class="toggleMenu" style="width: 130px">차량배차 신청</a><a data-menu="#menu6" class="toggleMenu" style="width: 130px">운영 컨설팅</a>
			</div>
		</ul> 
		<div class="sub05_box7">
			<div class="tabcontent">
				<div id="menu1" class="tab" style="display:none">
					<ul class="sub05_box7_1" style="display:block;">
						<li>
							<img src="${pageContext.request.contextPath}/common/images/sub05_img24.jpg" alt="" />
							<div class="sub05_box6">
							<p>제조사·차종·연식별 에어백(운전석, 조수석, 측면에어백) SRS, ABS, 도난방지장치(Immobilizer), BCM(Body Control Module) 등 장착 유무 및 공인연비, 엔진형식, 배기량, 통신타입(OBD-II General/Manufacture Specific) 등을 파악한 <b>차종별 특성 및 차량 도어 개폐 등의 제어를 위한 DB 구축</b> (현대,기아,삼성 약 2,000여 트림 정보 DB)</p> 
							<p>에어백 전개유무 및 차량 고장진단(DTC) 확인을 위한 <b>국제표준(General)/제조사(Manufacture), 차량별 고장진단코드(Diag nostic Trouble Code) DB</b> 구축 (차종별 3,000~7,000여개 고장코드 진단)</p>
							<p>차량 정비내역 및 표준명칭 입력을 위한 <b>차량부품</b> DB 구축</p>
							<p>경찰청 <b>교통사고통계 DB 230여만건</b>, 도로별 속도 등의 국토교통부 <b>GIS MAP DB 47여만건</b> 구축</p>
							<p>생성되는 운전자별 차량 운행정보 및 사고정보 등의 빅데이터 저장·관리·응용을 위한 클라우드 서비스 구축</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div id="menu2" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img25.jpg" alt="" />
						<div class="sub05_box6">
						<p>차량-ICT 단말을 기반으로 정확한 거리 및 시간 기록</p>
						<p>VDAS 를 통해 <b>운행일지 자동 작성기능</b>으로 사용이 잦은 업무 차량 운전자의 편의 제공</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu3" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img26.jpg" alt="" />
						<div class="sub05_box6">
						<p>사용자, 차랑별 운행경비 기록 관리</p>
						<p>운전자, 차량별 운행경비 및 차량 관리 비용 비교분석을 통한 <b>불필요한 경비 및 차량운행 비용 절감</b></p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu4" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img27.jpg" alt="" />
						<div class="sub05_box6">
						<p>지속적인 차량 운행 및 상태 모니터링을 통한 <b>에코 ∙ 안전운전 코칭</b></p>
						<p>에코 ∙ 안전운전은 연비 절감, 사고 예방으로 이어져 회사의 비용절감 효과 기대</p>
						<p><b>차량의 고장 및 소모품을 자동 관리기능</b>으로 차량의 가치 및 건강상태 유지</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu5" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img28.jpg" alt="" />
						<div class="sub05_box6">
						<p>카쉐어링 서비스 기반의 VDAS 배차서비스는 WEB, APP 을 통해 언제든지 간편하게 배차신청이 가능하며 <b>차량개조없이 스마트폰 APP을 통해 도어 개폐</b></p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu6" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img29.jpg" alt="" />
						<div class="sub05_box6">
						
						</div>
					</li>
				</ul>
			</div>
			
		</div>
	</div>
</div>
		
		
		
		
		