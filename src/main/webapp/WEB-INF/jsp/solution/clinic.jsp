<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_04");
});


$(document).ready(function(){
	$(".toggleMenu").on("click",function(){
		$(".toggleMenu").removeClass("active");
		$(this).addClass('active');
		$(".tabbable").find(".tab").hide();
		tab = $(this).data("menu");
		
		$(tab).show();
		
	});
	
	$(".toggleMenu").eq(0).trigger("click");
	
});
</script>
	
<div class="sub_layout">
	<div class="sub_title1">
		<span></span>
		VClinic
	</div>
	<div class="sub_title7_2">
		차량 정비관리 솔루션, VClinic
	</div>
	<div><img src="${pageContext.request.contextPath}/common/images/sub05_img30.jpg" alt="" /></div>
	
	<div class="sub_tit1">서비스 상세</div>
		
	<div class="tabbable"> 
		<ul class="tabs">
			<div class="sub_tab">
				<a data-menu="#menu1" class="toggleMenu" style="width: 112px">정비등록</a><a data-menu="#menu2" class="toggleMenu" style="width: 112px">장부조회/관리</a><a data-menu="#menu3" class="toggleMenu" style="width: 112px">고객관리</a><a data-menu="#menu4" class="toggleMenu" style="width: 112px">소모품 관리</a><a data-menu="#menu5" class="toggleMenu" style="width: 112px">정비예약 조회</a><a data-menu="#menu6" class="toggleMenu" style="width: 112px">고객 메시지</a><a data-menu="#menu7" class="toggleMenu" style="width: 112px">커뮤니티</a>
			</div>
		</ul> 
		<div class="sub05_box7">
			<div class="tabcontent">
				<div id="menu1" class="tab" style="display:none">
					<ul class="sub05_box7_1" style="display:block;">
						<li>
							<img src="${pageContext.request.contextPath}/common/images/sub05_img31.jpg" alt="" />
							<div class="sub05_box6">
							<p>고객차량의 정비내역과 정비금액, 결제내역을 상세하게 입력하고 관리할 수 있습니다.</p>
							<p>이렇게 등록된 정비내역은 해당고객이 스마트폰 어플에서도 확인할 수 있게 됩니다.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div id="menu2" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img32.jpg" alt="" />
						<div class="sub05_box6">
						<p>기간별, 고객별, 결제수단별로 정비내역을 상세하게 확인 할 수 있습니다.(정비항목, 부품등급/금액, 공임, VAT처리, 정비총금액, 결제 수단별 금액, 미수금액, 할인 등)</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu3" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img33.jpg" alt="" />
						<div class="sub05_box6">
						<p>정비업소의 전체고객을 조회할 수 있으며 회원속성별로 뷰카회원만 보기, 일반회원만 보기 등을 통해 내 정비업소의 고객유형을 쉽게 파악할 수 있습니다.</p>
						<p>고객 이름, 차량번호 또는 전화번호로 고객조회가 가능하며 뷰카 회원인 경우 기본정보외에 최근 운행기록별 차량상태, 과거 정비이력 등을 확인 할 수 있습니다.</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu4" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img34.jpg" alt="" />
						<div class="sub05_box6">
						<p>뷰카회원 차량의 소모품 상태를 조회하고 관리할 수 있습니다.</p>
						<p>각 소모품의 교환주기와 주행거리를 실시간으로 비교하여 그래프와 숫자로 반영되며 소모품의 현재상태와 교환시점은 뷰카클리닉과 뷰카회원의 어플에서 동시에 알 수 있습니다</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu5" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img35.jpg" alt="" />
						<div class="sub05_box6">
						<p>뷰카회원 차량의 정비예약 상황을 조회할 수 있습니다. 뷰카회원은 스마트폰의 어플을 통해 지정해 놓은 정비업소에 정비예약을 할 수 있고 이 예약정보는 해당 정비업소의 뷰카클리닉 정비예약 조회 화면에 기록됩니다.</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu6" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img36.jpg" alt="" />
						<div class="sub05_box6">
						<p>고객유형, 고객그룹, 고객등급, 차종별, 차량번호/고객명 등으로 원하는 목적대로 검색한 후 전송대상을 선택하고 메시지를 작성하여 편리하게 전송할 수 있습니다.</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu7" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img37.jpg" alt="" />
						<div class="sub05_box6">
						<p>내 정비업소에 등록된 고객차량이 타 정비업소(뷰카클릭닉 가입업소)를 통해 정비한 이력이 있을 때 해당 타 정비업소의 정비내역을 비교해 볼 수 있습니다.</p>
						<p>뷰카클리닉을 사용하는 모든 정비업소 직원들이 자신만의 정비기술이나 정비관련 신규정보를 공유하고 논의할 수 있습니다.</p>
						<p>뷰카 정비업소를 방문하는 고객 중 블랙컨슈머에 해당 할 수 있는 고객을 정비업소가 공동으로 서로간에 공유하여 고객과의 불필요한 마찰이나 불미스러운 일을 예방할 수 있습니다.</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
	 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

		