<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM03");
	initMenuSel("LM03_01");
	
});

var pageUrl =  "/news/news";
$(document).ready(function(){

	
		
	$(".menu").each(function(){
		if($(this).data("menu") == "${reqVo.searchContentType}") $(this).addClass("on");
	})
	
		
	$("#btn_search").on("click",function(){
		var p = pageUrl;
		var f = "frm";
		var frmParam = $("#"+f).serializeArray();
		var param = {};
		for(var i= 0 ; i < frmParam.length ; i++){
			param[frmParam[i].name] = frmParam[i].value; 
		}
		
		param.callPage = p;
		var url = "/index.do";
		$JBIZ.instance_post(url,param);
	});
	
	$(".menu").on("click",function(){
		var menu = $(this).data("menu");
		$("#frm input[name=searchContentType]").val(menu);
		
		$("#btn_search").trigger("click");		
	});
});
</script>
<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			새소식
		</div>
		<div class="sub_title3">
			자스텍M에서 알려드립니다.
		</div>
<div class="tabbable">
	<ul class="tabs">	
		<div class="sub_tab mat53">
			<a data-menu="" class="menu">전체보기</a><a data-menu="1" class="menu">NEWS</a><a data-menu="2" class="menu">보도자료</a>
		</div>
	</ul>	
		<table id="table" class="table1 table1_faq">
			<colgroup>
				<col width="80" />
				<col width="*" />
				<col width="100" />
			</colgroup>
			<thead>
				<tr>
					<th>번호</th>
					<th>제목</th>
					<th>작성일</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${fn:length(rtvList) eq 0}">
					<tr class="f">
						<td colspan="2" style='text-align:center'>검색결과가 없습니다.</td>
					</tr>
				</c:if> 
				<c:if test="${fn:length(rtvList) > 0 }">
					<c:forEach var="vo" items="${rtvList}" varStatus="i">
						<tr class="f">
							<th>${vo.contentTypeNm }</th>
							<td style='text-align:center'>${vo.title }</td>
							<th>
							<fmt:parseDate var="d" value="${vo.regDate }" pattern="yyyy-MM-dd HH:mm:ss.S"/>
							<fmt:formatDate value="${d}" pattern="yyyy-MM-dd"/>
							</th> 
						</tr>
						<tr class="a">
							<th></th>
							<td style='text-align:center'>
								${vo.contents }
							</td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
	<form id="frm">
		<input type="hidden" name="searchBoardType" value="" />
		<input type="hidden" name="searchContentType" value="${reqVo.searchContentType}" />
		<div class="search_box mat53">
			<input type="text" name="searchText" placeholder="검색으로 빠르게 궁금한 점을 해결해보세요." value="${reqVo.searchText}"/><button id="btn_search">검색</button>
		</div>
	</form>
	<script type="text/javascript">	
		var pagingFieldData = new Array (
				{fieldName:"searchBoardType",fieldValue:"${reqVo.searchBoardType}"}
				,{fieldName:"searchContentType",fieldValue:"${reqVo.searchContentType}"}
				,{fieldName:"searchText",fieldValue:"${reqVo.searchText}"}
				,{fieldName:"callPage",fieldValue:pageUrl}
		);
		var pagingProperty = {
			urlPath: "${pageContext.request.contextPath}/index.do" 	//required
		}	
		</script>
		<%@ include file="/WEB-INF/jsp/common/PagingNavi.jsp"%>
	</div>
</div>