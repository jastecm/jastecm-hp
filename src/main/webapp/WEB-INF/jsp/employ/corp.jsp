<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM07");
	initMenuSel("LM07_01");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			회사생활
		</div>
		<div class="sub_title6 mab13">
			<span>성장제도</span><strong></strong>
		</div>
		<div class="sub_txt2">자스텍M은 소속원 <span>모두가 함께 역량을 높히고 성장</span>할 수 있는 프로그램을 운영하고 있습니다.</div>
		<ul class="sub05_box1">
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub07_img1.jpg" alt="" />
				<h1>세상의 중심을 향하여!</h1>
				<p>
				
					과학혁명의 새로운 사고전환이 필요하고 이뤄지는 시기입니다.<br>
					혼자서 해결하기 보다는 소속그룹의 토론과 협업으로 문제를 해결하고
					업무에 필요한 외부교육이 있다면 그 기회를 누릴 수 있도록 지원합니다.
					특히, 업무와 관련된 국내ㆍ해외 컨퍼런스 참여를 적극 지원합니다.
				</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub07_img2.jpg" alt="" />
				<h1>패러다임의 Shift! </h1>
				<p>
					작은 idea 라도 소중히 생각하겠습니다.<br>
					생소한 전혀 다른 분야가 만나 새로운 패러다임을 만듭니다.<br>
					소속원이 가진 작은 생각이 모여 문명의 변화를 이끌 것이라 믿습니다.<br>
					직군별 전문성을 강화하는 프로그램 이외 창의적 사고를 이끄는 역량강화 프로그램을 운영합니다.<br>
					또한 탄력근무제를 통해 원하는 시간 일하고  확보된 시간은 자신의 인사이트 함양을 위해 투자하십시오.
				</p>
			</li>
		</ul>
		<div class="sub_title6 mab13">
			<span>복지제도</span><strong></strong>
		</div>
		<div class="sub_txt2">자스텍M은 소속원의 <span>사기증진, 안정된 생활을 위해</span> 다양한 복지제도를 운영하고 있습니다. </div>
		<ul class="sub07_box1">
			<li>
				<span>근무시간 및<br>휴가</span>
				<p>
					건강하고 즐거운 직장생활을 위해 주5일 근무제를 시행중이며 소속원의 자기개발 및 자유로운 출퇴근을 위해 탄력근무제를 운영중입니다.<br>
					휴가는 14일/년을 기준으로 2년 근속시마다 1일 추가됩니다.
					또한 소속원의 리프레쉬를 위해 5년 근속시 마다 파격적인 안식휴가와 휴가비를 지원합니다. 
				</p>
			</li>
			<li>
				<span>편의시설</span>
				<p>
					회사 입주건물에 직원들의 휴식을 위한 카페테리아와 구내식당, 피트니스센터 등이 있습니다.<br>
					회사 주변 어디서나 자유로운 브레인스토밍을 통한 아이디어 공유, 서비스 기획 등을 할 수 있습니다.<br>
					제휴 서비스를 통해 콘도 등의 휴양지 시설 또한 이용할 수 있습니다.
				</p>
			</li>
			<li>
				<span>4대보험 및<br>단체상해보험</span>
				<p>전 소속원을 대상으로 4대보험과 단체상해보험을 통해 각종 상해, 사고, 질병에 대한 위험부담을 최소화 할 수 있도록 합니다.</p>
			</li>
			<li>
				<span>스톡옵션&<br>성과급</span>
				<p>
					직무 수행능력이 뛰어난 자 및 3년이상 근속자는 스톡옵션 권리를 부여합니다.<br>
					객관적이고 공정한 평가에 의한 성과급제를 시행합니다.
				</p>
			</li>
			<li>
				<span>인사특전</span>
				<p>
					매 1년마다 연봉계약에 의한 정기승급이 이뤄집니다.<br>
					창조적인 업무수행을 통한 직무수행능력이 뛰어난 자의 특급 승급도 보장합니다.
				</p>
			</li>
			<li>
				<span>회식 비용 및<br>식대ㆍ유류비 지원</span>
				<p>
					부서별 회식비용과 식대를 지원합니다.<br>
					또한 업무상 외근시 유류비도 지원합니다.
				</p>
			</li>
			<li>
				<span>경조금 지원 및<br>명절 선물</span>
				<p>
					경조 휴가를 부여하며, 회사에서는 축하 또는 조의의 마음을 표하기 위해 경조금과 경조화환(조화)를 전달하고 있습니다.<br>
					뜻깊은 명절을 가족과 보내는데 보탬이 되고자 소속원에게 명절선물을 지급하고 있습니다.
				</p>
			</li>
		</ul>
	</div>