<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM07");
	initMenuSel("LM07_04");
});

</script>
 
<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			채용공고
		</div>
		<div class="sub_txt2">자스텍M에서는 새로운 과학혁명을 이끌 참신하고 창조적인 인재를 채용하고 있습니다.</div>
		<div class="sub06_box1">
			<p>채용관련 문의는 이메일 <span>job@jastecM.com</span>으로 문의 주세요.</p>
			<p>창조적인 회사 자스텍M의 입사지원서는 정해진 양식이 없습니다. 자유롭게 자신의 포트폴리오를 구성해서 보내주세요.</p>
			<p>제출된 지원서는 입사 심사를 위해서만 활용되며, 제출된 서류는 일체 반환하지 않습니다.</p>
			<p>장애/보훈 대상자는 관계법령에 의거 우대합니다.</p>
		</div>
		<div class="sub_title6 mab13">
			<span>채용공고</span><strong></strong>
		</div>
		<br><br>
		<table id="table" class="table1 table1_faq">
			<colgroup>
				<col width="139" />
				<col width="*" />
			</colgroup>
			<thead>
				<tr>
					<th>모집분야</th>
					<th>제목</th>
					<th>모집기간</th>
					<th>작성일</th>
				</tr>
			</thead>
			<tbody>
				
				<c:if test="${fn:length(rtvList) > 0 }">
					<c:forEach var="vo" items="${rtvList}" varStatus="i">
						<tr class="f">
							<th>${vo.contentTypeNm }</th>
							<td style='text-align:center'>${vo.title }</td>
							<th>${vo.boardType }</th>
							<th><fmt:parseDate var="d" value="${vo.regDate }" pattern="yyyy-MM-dd HH:mm:ss.S"/>
							<fmt:formatDate value="${d}" pattern="yyyy-MM-dd"/></th>
						</tr>
						<tr class="a">
							<th></th>
							<td style='text-align:center'>
								${vo.contents }
							</td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
		<%-- <div class="paging">
			<a href="#">
				<img src="${pageContext.request.contextPath}/common/images/first.jpg" alt="">
			</a><a href="#">
				<img src="${pageContext.request.contextPath}/common/images/prev2.jpg" alt="">
			</a><span>
				<a href="#" class="on">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a><a href="#">6</a><a href="#">7</a><a href="#">8</a><a href="#">9</a><a href="#">10</a>
			</span><a href="#">
				<img src="${pageContext.request.contextPath}/common/images/next2.jpg" alt="">
			</a><a href="#">
				<img src="${pageContext.request.contextPath}/common/images/last.jpg" alt="">
			</a>
		</div> --%>
		<br><br><br><br><br><br>
		<div class="sub_title6">
			<span>채용 프로세스</span><strong></strong>
		</div>
		<div class="sub07_box7">
			<span>
				입사지원
			</span><img src="${pageContext.request.contextPath}/common/images/arrow2.jpg" alt="" /><span>
				서류심사
			</span><img src="${pageContext.request.contextPath}/common/images/arrow2.jpg" alt="" /><span>
				실무진 면접
			</span><img src="${pageContext.request.contextPath}/common/images/arrow2.jpg" alt="" /><span class="s1">
				경영진 및 인사면접
			</span><img src="${pageContext.request.contextPath}/common/images/arrow2.jpg" alt="" /><span class="last">
				최종 합격
			</span>
			<p>※ 일반적인 채용절차이며 채용내용에 따라 변동이 있을 수 있습니다.</p>
		</div>
	</div>