<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
function initMenuSel(m){
	$("#"+m).addClass("on");
}
</script>
<div class="left_menu">		
		<div class="lan">
			<a href="${pageContext.request.contextPath}/index.do" class="on">KOREAN</a><a href="http://www.jastecm.com/jbizen/index.do">ENGLISH</a>
		</div>
		<div class="logo"><a href="${pageContext.request.contextPath}/index.do" ><img src="${pageContext.request.contextPath}/common/images/logo.jpg" alt="" /></a></div>
		<ul class="menu">
			<li id="LM01">
				<a href="${pageContext.request.contextPath}/index.do" class="depth1 callPage">자스텍M</a>
			</li>
			
			<li id="LM02">
				<a href="${pageContext.request.contextPath}/corporate/vision.do" class="depth1 callPage">회사 소개</a>
				<div class="depth2">
					<a id="LM02_01" data-page="/corporate/vision" class="callPage">ㆍ 비전</a>
					<a id="LM02_02" data-page="/corporate/history" class="callPage">ㆍ 연혁</a>
					<a id="LM02_03" data-page="/corporate/ceomsg" class="callPage">ㆍ CEO인사말</a>
					<a id="LM02_04" data-page="/corporate/business" class="callPage">ㆍ 사업영역</a>
					<a id="LM02_05" data-page="/corporate/info" class="callPage">ㆍ 회사소개</a>
					<a id="LM02_06" data-page="/corporate/tech" class="callPage">ㆍ 보유기술현황</a>
					<a id="LM02_07" data-page="/corporate/partner" class="callPage">ㆍ 파트너센터</a>
					<a id="LM02_08" data-page="/corporate/partnership" class="callPage">ㆍ 제휴제안</a>
				</div>
			</li>
			
			
			<li id="LM03">
				<a href="${pageContext.request.contextPath}/news/news.do" class="depth1 callPage">새소식</a>
				<div class="depth2">
				<a id="LM03_01" data-page="/news/news" class="callPage">ㆍ 새소식</a>
				</div>
			</li>
			
			<li id="LM04">
				<a href="${pageContext.request.contextPath}/product/tech.do" class="depth1 callPage">제품 소개</a>
				<div class="depth2">
					<a id="LM04_01" data-page="/product/tech" class="callPage">ㆍ 주요특화기술</a>
					<a id="LM04_02" data-page="/product/type" class="callPage">ㆍ 자동차IoT단말
					
				</div>
			</li>
			
			<li id="LM05">
				<a href="${pageContext.request.contextPath}/solution/info.do" class="depth1 callPage">솔루션 소개</a>
				<div class="depth2">
					<a id="LM05_01" data-page="/solution/info" class="callPage">ㆍ 솔루션소개</a>
					<a id="LM05_02" data-page="/solution/apps" class="callPage">ㆍ 스마트폰APP</a>
					<a id="LM05_03" data-page="/solution/vdas" class="callPage">ㆍ VDAS</a>
					<a id="LM05_04" data-page="/solution/clinic" class="callPage">ㆍ VClinic</a>
					<a id="LM05_05" data-page="/solution/share" class="callPage">ㆍ VShare</a>
					<a id="LM05_06" data-page="/solution/scoring" class="callPage">ㆍ VScoring</a>
					<a id="LM05_07" data-page="/solution/call" class="callPage">ㆍ VCall</a>
					<a id="LM05_08" data-page="/solution/comm" class="callPage">ㆍ VComm</a>
				</div>
			</li>
			
			<li id="LM06">
				<a href="${pageContext.request.contextPath}/service/viewcar.do" class="depth1 callPage">서비스 소개</a>
				<div class="depth2">
					<a id="LM06_01" data-page="/service/viewcar" class="callPage">ㆍ ViewCAR</a>
					<a id="LM06_02" data-page="/service/vdas" class="callPage">ㆍ VDAS</a>
					<a id="LM06_03" data-page="/service/clinic" class="callPage">ㆍ ViewCAR클리닉</a>
					<a id="LM06_04" data-page="/service/carCompatibility" class="callPage">ㆍ 지원차종</a>
					<!-- <a id="LM06_05" data-page="/service/estimateSheet" class="callPage">ㆍ 견적서</a> -->
				</div>
			</li>
		
			
			<li id="LM07">
				<a href="${pageContext.request.contextPath}/employ/corp.do" class="depth1 callPage">인재채용</a>
				<div class="depth2">
					<a id="LM07_01" data-page="/employ/corp" class="callPage">ㆍ 회사생활</a>
					<a id="LM07_02" data-page="/employ/ceomsg" class="callPage">ㆍ 경영진의메시지</a>
					<a id="LM07_03" data-page="/employ/today" class="callPage">ㆍ 요즘우리는</a>
					<a id="LM07_04" data-page="/employ/noti" class="callPage">ㆍ 채용공고</a>
				</div>
			</li> 
			<li id="LM08">
				<a href="${pageContext.request.contextPath}/cs/faq.do" class="depth1 callPage">고객센터</a>
				<div class="depth2">
					<a id="LM08_01" data-page="/cs/faq" class="callPage">ㆍ 자주하는질문</a>
					<a id="LM08_02" data-page="/cs/inquiry" class="callPage">ㆍ 1:1 이용문의</a>
					<a id="LM08_03" data-page="/cs/download" class="callPage">ㆍ 다운로드자료실</a>
				</div>
			</li>
		</ul>
		<ul class="site">
			<li>관련 사이트</li>
			<li>
				<a href="http://viewcar.net/">ViewCAR</a>
				<a href="http://vdaspro.viewcar.co.kr">ViewCAR FMS</a>
				<a href="http://jastec.co.kr">자스텍</a>
			</li>
		</ul>
	</div>