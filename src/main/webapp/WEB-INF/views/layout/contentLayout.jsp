<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="description" content="자동차 IOT 디바이스(통신형 OBD, 블루투스 OBD) 개발·제조">
<meta property="og:title" content="자스텍M">
<meta property="og:description" content="자동차 IOT 디바이스(통신형 OBD, 블루투스 OBD) 개발·제조">
<title><tiles:insertAttribute name="title" ignore="true" /></title>

<tiles:insertAttribute name="css" ignore="true" />

<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script charset="utf-8" src="${pageContext.request.contextPath}/common/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/icheck.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-89283408-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- 전환페이지 설정 -->
<script type="text/javascript" src="//wcs.naver.net/wcslog.js"></script> 
<script type="text/javascript"> 
var _nasa={};
_nasa["cnv"] = wcs.cnv("5","100"); // 전환유형, 전환가치 설정해야함. 설치매뉴얼 참고
</script> 
<!-- 공통 적용 스크립트 , 모든 페이지에 노출되도록 설치. 단 전환페이지 설정값보다 항상 하단에 위치해야함 --> 
<script type="text/javascript" src="//wcs.naver.net/wcslog.js"> </script> 
<script type="text/javascript"> 
if (!wcs_add) var wcs_add={};
wcs_add["wa"] = "s_21c946d828a5";
if (!_nasa) var _nasa={};
wcs.inflow();
wcs_do(_nasa);
</script>

</head>
<body>


<div class="wrap">
	<tiles:insertAttribute name="left"/>
	
	<tiles:insertAttribute name="contents"/>			
	
	<tiles:insertAttribute name="footer"/> 
</div>

<script src="${pageContext.request.contextPath}/common/js/jquery.bxslider.min.js"></script>
<script src="${pageContext.request.contextPath}/common/js/base.js" language="javascript" type="text/javascript"></script>

</body>
</html>