package com.view;

import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

import com.util.json.JSONObject;

public class MultipartView extends AbstractView {

	public MultipartView() {
		this.setContentType("text/html");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void renderMergedOutputModel(Map model,HttpServletRequest request, HttpServletResponse response){
		String[] key = (String[]) model.keySet().toArray(new String[0]);
		response.setContentType("text/html;charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		JSONObject obj = new JSONObject();
		PrintWriter pw = null;
		try{
			pw = response.getWriter();
			for (int i = 0; i < key.length; i++) {
				obj.put(key[i], model.get(key[i]));
			}
			pw.print(obj.toString());
			pw.flush();
			pw.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pw != null){
				pw.close();
			}
		}
		
	}
}
