package com.util.string;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;

/**
 * Date의 처리를 쉽게 하기 위해 만든 wrapper클래스.
 * 사용하도록 한다.
 * 
 * @author
 * @version 1.0
 * @see GregorainCalendar
 */
public class DateUtil extends GregorianCalendar {

	/**
	 * Logger for this class
	 */

	/**
	 * serialVersionUID
	 */
	public static final long serialVersionUID = 1L;
	
	/**
	 * 1초
	 */
	public static final long ONE_SECOND = 1000l;

	/**
	 * 1분
	 */
	public static final long ONE_MINUTE = 60000l;

	/**
	 * 1시간
	 */
	public static final long ONE_HOUR = 3600000l;

	/**
	 * 1일
	 */
	public static final long ONE_DAY = 86400000l;

	/**
	 * 1달
	 */
	public static final long ONE_MONTH = 2592000000l;

	/**
	 * 1년
	 */
	public static final long ONE_YEAR = 31536000000l;

	/**
	 * SimpleCalendar()메서드는 year, month, date, hour, minute, second을 인자값으로 받아
	 * 상위클래스에게 파라메터로 설정하는 생성자이다.
	 * 
	 * @param year :
	 *            년
	 * @param month
	 *            :월
	 * @param date
	 *            :날짜
	 * @param hour
	 *            :시간
	 * @param minute :
	 *            분
	 * @param second
	 *            :초
	 */
	public DateUtil(int year, int month, int date, int hour, int minute, 
			int second) {
		super(year, month, date, hour, minute, second);
	}

	/**
	 * SimpleCalendar()메서드는 year, month, date, hour, minute을 인자값으로 받아 상위클래스에게
	 * 파라메터로 설정하는 생성자이다.
	 * 
	 * @param year :
	 *            년
	 * @param month
	 *            :월
	 * @param date
	 *            :날짜
	 * @param hour
	 *            :시간
	 * @param minute :
	 *            분
	 */
	public DateUtil(int year, int month, int date, int hour, int minute) {
		super(year, month, date, hour, minute);
	}

	/**
	 * SimpleCalendar()메서드는 year, month, date을 인자값으로 받아 상위클래스에게 파라메터로 설정하는
	 * 생성자이다.
	 * 
	 * @param year :
	 *            년
	 * @param month
	 *            :월
	 * @param date
	 *            :날짜
	 */
	public DateUtil(int year, int month, int date) {
		super(year, month, date);
	}

	/**
	 * SimpleCalendar()메서드는 Date의 Pattern을 인자값으로 받아 자신의 makeDate메서드에 파라메터로 설정하는
	 * 생성자이다.
	 * 
	 * @param pattern :
	 *            Date(날자)
	 */
	public DateUtil(String pattern) {
		this();
		makeDate(pattern);
	}

	/**
	 * SimpleCalendar()메서드는 milli second를 인자값으로 받아 자신의 setTimeInMillis()메서드에
	 * 파라메터로 설정해주는 생성자이다.
	 * 
	 * @param millis :
	 *            초 -> milli second 단위
	 */
	public DateUtil(long millis) {
		this();
		setTimeInMillis(millis);
	}

	/**
	 * SimpleCalendar()메서드는 Date를 인자값으로 받아 자신의 setTime()메서드에 파라메터로 설정해주는 생성자이다.
	 * 
	 * @param date :
	 *            날자
	 */
	public DateUtil(Date date) {
		this();
		setTime(date);
	}

	/**
	 * SimpleCalendar()메서드는 시간대와, locale을 인자값으로 받아 상위 클래스에 파라메터로 설정해주는 생성자이다.
	 * 
	 * @param zone :
	 *            시간대
	 * @param aLocale :
	 *            locale
	 */
	public DateUtil(TimeZone zone, Locale aLocale) {
		super(zone, aLocale);
	}

	/**
	 * SimpleCalendar()메서드는 locale을 인자값으로 받아 상위 클래스에 파라메터로 설정해주는 생성자이다.
	 * 
	 * @param aLocale
	 *            :locale
	 */
	public DateUtil(Locale aLocale) {
		super(aLocale);
	}

	/**
	 * * SimpleCalendar()메서드는 시간대만 인자값으로 받아 자신의 setTime()메서드에 파라메터로 설정해주는 생성자이다.
	 * 
	 * @param zone
	 *            TimeZone : 시간대
	 */
	public DateUtil(TimeZone zone) {
		super(zone);
	}

	/**
	 * SimpleCalendar()기본 생성자이다. 현재 시스템 시간을 기본 값으로 하여 인스턴스 생성.
	 */
	public DateUtil() {
		super();
	}

	/**
	 * setTimeInMillis()메서드는 milli second 단위로 시간을 반환한다.
	 * 
	 * @return long : long타입의 초로 milli second
	 */
	public long getTimeInMillis() {
		return super.getTimeInMillis();
	}

	/**
	 * setTimeInMillis()메서드는 milli second를 인자값으로 받아 상위 클래스에 파라메터로 설정해준다.
	 * 
	 * @param millis
	 *            :초로 milli second 단위
	 */
	public void setTimeInMillis(long millis) {
		super.setTimeInMillis(millis);
	}

	/**
	 * makeDate()메서드는 년,월,일,시간,분,초를 조합하여 Date를 생성하는 기능을 수행한다. Date를 만드는 포맷은 아래
	 * 형식으로 생성된다.<br>
	 * G era designator (Text) AD<br>
	 * y year (Number) 1996<br>
	 * M month in year (Text & Number) July & 07<br>
	 * d day in month (Number) 10<br>
	 * h hour in am/pm (1~12) (Number) 12<br>
	 * H hour in day (0~23) (Number) 0<br>
	 * m minute in hour (Number) 30<br>
	 * s second in minute (Number) 55<br>
	 * S millisecond (Number) 978<br>
	 * E day in week (Text) Tuesday<br>
	 * D day in year (Number) 189<br>
	 * F day of week in month (Number) 2 (2nd Wed in July)<br>
	 * w week in year (Number) 27<br>
	 * W week in month (Number) 2<br>
	 * a am/pm marker (Text) PM<br>
	 * k hour in day (1~24) (Number) 24<br>
	 * K hour in am/pm (0~11) (Number) 0<br>
	 * z time zone (Text) Pacific Standard Time
	 * 
	 * @param pattern :
	 *            날자 (Date)
	 */
	private void makeDate(String pattern) {
		try {
			// if(pattern.length() == 19)
			// 수정 : 2002.11.19.0930.
			// oracle의 date 형식에 맞추기 위하여 추가.(ms 단위)
			if (pattern.length() == 19 || pattern.length() == 21) {
				StringTokenizer stDate = new StringTokenizer(pattern.substring(
						0, pattern.indexOf(" ")), "-");
				StringTokenizer stTime = new StringTokenizer(pattern.substring(
						pattern.indexOf(" ") + 1, pattern.length()), ":");

				int year = Integer.parseInt(stDate.nextToken());
				int month = Integer.parseInt(stDate.nextToken()) - 1;
				int date = Integer.parseInt(stDate.nextToken());
				int hour = Integer.parseInt(stTime.nextToken());
				int minute = Integer.parseInt(stTime.nextToken());
				int second = Integer.parseInt(stTime.nextToken()
						.substring(0, 2));

				set(ERA, AD);
				set(YEAR, year);
				set(MONTH, month);
				set(DATE, date);
				set(HOUR_OF_DAY, hour);
				set(MINUTE, minute);
				set(SECOND, second);
				set(MILLISECOND, 0);
			} else if (pattern.length() == 10 && pattern.length() < 19) {
				StringTokenizer stDate = new StringTokenizer(pattern, "- :");
				int year = Integer.parseInt(stDate.nextToken());
				int month = Integer.parseInt(stDate.nextToken()) - 1;
				int date = Integer.parseInt(stDate.nextToken());
				set(YEAR, year);
				set(MONTH, month);
				set(DATE, date);
				set(HOUR_OF_DAY, 0);
				set(MINUTE, 0);
				set(SECOND, 0);
				set(MILLISECOND, 0);
			}
		} catch (Exception e) {
	
		}
	}
	
	/**
	 * 
	 */
	public String getSomeYear(int term) {
		String someYear;		
		Calendar cal = Calendar.getInstance();
		cal.add(cal.YEAR, term);
		someYear = cal.get(cal.YEAR) +"";
		return someYear;
	}

	/**
	 * getYear()메서드는 년도를 반환한다.
	 * 
	 * @return java.lang.String : 년도 YYYY-> 2005
	 */
	public String getYear() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
		return dateFormat.format(new java.util.Date(getTimeInMillis()))
				.toString();
	}

	/**
	 * getMonth()메서드는 월을 반환한다.
	 * 
	 * @return java.lang.String : 월 MM -> 11
	 */
	public String getMonth() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
		return dateFormat.format(new java.util.Date(getTimeInMillis()))
				.toString();
	}

	/**
	 * getDay()메서드는 일자를 반환한다.
	 * 
	 * @return java.lang.String : 일자 dd -> 20
	 */
	public String getDay() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
		return dateFormat.format(new java.util.Date(getTimeInMillis()))
				.toString();
	}

	/**
	 * getDayOfWeek()메서드는 날짜에 따른 주차를 반환한다.
	 * 
	 * @return java.lang.String : 주 (Week)
	 */
	public String getDayOfWeek() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEE");
		return dateFormat.format(new java.util.Date(getTimeInMillis()))
				.toString();
	}

	/**
	 * getHour()메서드는 시간을 반환한다.
	 * 
	 * @return java.lang.String : 시간
	 */
	public String getHour() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("hh");
		return dateFormat.format(new java.util.Date(getTimeInMillis()))
				.toString();
	}

	/**
	 * getHour24()메서드는 시간을 반환한다.
	 * 
	 * @return java.lang.String : 시간
	 */
	public String getHour24() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH");
		return dateFormat.format(new java.util.Date(getTimeInMillis()))
				.toString();
	}

	/**
	 * getMinute()메서드는 분을 반환한다.
	 * 
	 * @return java.lang.String : 분
	 */
	public String getMinute() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("mm");
		return dateFormat.format(new java.util.Date(getTimeInMillis()))
				.toString();
	}

	/**
	 * getSecond()메서드는 초를 반환하는데 milli second단위로 반환한다.
	 * 
	 * @return java.lang.String :초를 반환하는데 milli second
	 */
	public String getSecond() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("ss");
		return dateFormat.format(new java.util.Date(getTimeInMillis()))
				.toString();
	}

	/**
	 * getDate()메서드는 Date를 반환한다. 형식은 20051105 형식으로 반환한다.
	 * 
	 * @return java.lang.String : Date반환
	 */
	public String getDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(new java.util.Date(getTimeInMillis()))
				.toString();
	}

	/**
	 * getTimes()메서드는 시간을 반환한다. 형식은 09:57:05 형식으로 반환한다.
	 * 
	 * @return java.lang.String : 시간
	 */
	public String getTimes() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		return dateFormat.format(new java.util.Date(getTimeInMillis()))
				.toString();
	}

	/**
	 * getDateTime()메서드는 시간과 날짜를 반환한다.
	 * 
	 * @return java.lang.String : 날짜와 시간이 조합된 String 타입
	 */
	public String getDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		return dateFormat.format(new java.util.Date(getTimeInMillis()))
				.toString();
	}

	/**
	 * getDateTime()메서드는 날짜 포맷을 반환한다.
	 * 
	 * @param pattern :
	 *            날짜 패턴
	 * @return java.lang.String : 포멧된 날짜
	 */
	public String getDateTime(String pattern) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(new java.util.Date(getTimeInMillis()))
				.toString();
	}

	/**
	 * SimpleCalendar 객체를 Date 객체로 변환한다.
	 * 
	 * @return Date :날짜
	 */
	public Date convertToDate() {
		return new Date(getTimeInMillis());
	}

	/**
	 * SimpleCalendar 객체를 Timestamp 객체로 변환한다.
	 * 
	 * @return Timestamp : 타임 스탬프
	 */
	public Timestamp convertToTimestamp() {
		return new Timestamp(getTimeInMillis());
	}

	/**
	 * 스트링 타입의 시간과 날짜를 반환한다.
	 * 
	 * @return String : 시간날짜가 조합된 String 타입
	 */
	public String toString() {
		return getDateTime();
	}

	// public static void main(String[] args)
	// {
	// SimpleCalendar sc = new SimpleCalendar("2005-10-11");
	// System.out.println("sc:" +sc);
	// }
}
