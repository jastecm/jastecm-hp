package com.util.string;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StrUtil {
	
	private static Integer DoubleQuot = new Integer(34);
    private static  Integer SingleQuot = new Integer(39);
    private static  Integer Space      = new Integer(32);
    private static  Integer EnterUnix  = new Integer(13);
    private static  Integer Enter      = new Integer(10);    
	
	/**
	 * 스트링 값에 , 생성해줌
	 * @param sourceNum
	 * @return
	 */
	public static String stringtoMoney(String sourceNum){
        if(sourceNum.isEmpty())return "0";
        if(sourceNum.length() <= 3) return sourceNum;        
         double num = Double.parseDouble(sourceNum);
         DecimalFormat df = new DecimalFormat("#,###");        
        return df.format(num).toString();
    }
	
	
	/**
	 * 문자열이 null이거나 ""일 때, true를 리턴하고
	 * 그렇지 않을경우 false를 리턴한다.
	 * @param str  문자열
	 * @return boolean null이거나 ""일 때, true 반환
	 */
	public static boolean isNull(String str) {
		return (str == null || str.equals("") || "null".equals(str)) ? true : false;
	}
	
	/**
	 * 빈값 과 null 체크 
	 * @param checkVal
	 * @return
	 */
	public static boolean isNullToEmpty(String checkVal){
		if("".equals(checkVal) || null == checkVal)
			return true ; 
		else 
			return false; 
	}
	
	/**
	 * 빈값 과 null 일경우     값을 변경 
	 * @param checkVal
	 * @param changeVal
	 * @return
	 */
	public static String isNullToEmpty(String checkVal,String changeVal){
		if("".equals(checkVal) || null == checkVal)
			return changeVal ; 
		else 
			return checkVal;
	}
	
	/**
	 * 등록시 (varchar20) 신규ID 생성
	 * @return
	 */
	public static String getNewV20Id(){
		String rtn;
		Date dt = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss"); 
		int rand = (int)(Math.random()*1000);
		rtn = sdf.format(dt).toString() + rand;
		return rtn;
	}
	
	/**
	 * 문자의 3자리 마다 ,찍어준다. (소수점 포함으로 수정. 2011-07-19  배태호)
	 * @param str
	 * @return
	 */
	public static String getComma(String str){
		
		StringBuffer sb = new StringBuffer() ;
		String retStr = "";												// 결과값 리턴 변수
		
		if(str == null || "null".equals(str) || "".equals(str)){		// null 이나 공백이면 '0' 세팅.
			retStr = "0" ; 
		}else{															// 값이 있으면
		
			str = str.replaceAll(" ", "");								// 공백제거.
			if(".".equals(str.substring(0, 1))) str = "0"+str;			// 소수점 (.XXX --> 0.XXX)
			
			String tempStr = str.indexOf(".") == -1? str: str.substring(0, str.indexOf("."));					// 정수 부분
			String tailStr = str.indexOf(".") == -1? "": str.substring(str.indexOf("."), str.length());			// 소수점 부분
			
			char strChar ; 
			 
			int len = tempStr.length() ;
		    // System.out.print("#########" + tempStr +" : " + sb2.length()) ; 
			int commaIndex = (len-1)%3; 
			for (int i = 0; i < tempStr.length(); i++) {
				strChar = tempStr.charAt(i) ; 
				sb.append(strChar) ; 
				if(i==commaIndex && i<len-1){
					sb.append(","); 
					commaIndex +=3 ; 
				}
			}
			
			retStr = sb.toString()+tailStr;
			
			// 마이너스값 처리. (-,XXX,XXX,XXX ===> -XXX,XXX,XXX)
			if("-".equals(retStr.substring(0, 1))&& ",".equals(retStr.substring(1, 2)))
				retStr = "-"+retStr.substring(2, retStr.length());
			
			
			// 마이너스값 색상 처리. (빨간색)
			if("-".equals(retStr.substring(0, 1))) retStr = "<font color='red'>"+ retStr +"</font>";
			
		}
		
		return retStr ; 
	}
	
	/**
	 * 문자의 3자리 마다 ,찍어준다. (소수점 포함으로 수정. 2011-07-19  배태호)
	 * @param str, unit(단위)
	 * @return
	 */
	public static String getComma(String str, String unit){
		
		StringBuffer sb = new StringBuffer() ;
		String retStr = "";												// 결과값 리턴 변수
		
		if(str == null || "null".equals(str) || "".equals(str) || "0".equals(str)){		// null 이나 공백, '0' 이면 '0' 세팅.
			retStr = "0" ; 
		}else{															// 값이 있으면
		
			System.out.println("들어온 숫자값 ::: "+str);
			int str_length = str.length();			// 원 str 길이
			int unit_length = unit.length();		// 단위 길이
			int add_point = 0;						// 소수점 변경시에 0 추가할 길이.
			String first_str = "0.";
			if(unit_length > str_length){ 
				add_point = unit_length - str_length;
				System.out.println("반복 ::: "+add_point);
				for(int i=0; i< add_point; i++){
					first_str += "0";
					
				}
				str = (first_str+str);
				
			}
			else str = str.substring(0, str_length-unit_length+1);
			System.out.println("단위에 따른 숫자값 ::: "+str);
			
			
			
			str = str.replaceAll(" ", "");								// 공백제거.
			if(".".equals(str.substring(0, 1))) str = "0"+str;			// 소수점 (.XXX --> 0.XXX)
			
			String tempStr = str.indexOf(".") == -1? str: str.substring(0, str.indexOf("."));					// 정수 부분
			String tailStr = str.indexOf(".") == -1? "": str.substring(str.indexOf("."), str.length());			// 소수점 부분
			
			char strChar ; 
			 
			int len = tempStr.length() ;
		    // System.out.print("#########" + tempStr +" : " + sb2.length()) ; 
			int commaIndex = (len-1)%3; 
			for (int i = 0; i < tempStr.length(); i++) {
				strChar = tempStr.charAt(i) ; 
				sb.append(strChar) ; 
				if(i==commaIndex && i<len-1){
					sb.append(","); 
					commaIndex +=3 ; 
				}
			}
			
			retStr = sb.toString()+tailStr;
			
			// 마이너스값 처리. (-,XXX,XXX,XXX ===> -XXX,XXX,XXX)
			if("-".equals(retStr.substring(0, 1))&& ",".equals(retStr.substring(1, 2)))
				retStr = "-"+retStr.substring(2, retStr.length());
			
			
			// 마이너스값 색상 처리. (빨간색)
			if("-".equals(retStr.substring(0, 1))) retStr = "<font color='red'>"+ retStr +"</font>";
			
		}
		
		return retStr ; 
	}
	
	/**
	 * 문자의 3자리 마다 ,찍어준다. (소수점 포함으로 수정. 2013-03-05)
	 * null 일때 return value 지정 
	 * @param str
	 * @return
	 */
	public static String getComma(String str, String unit, String rtn){
		
		StringBuffer sb = new StringBuffer() ;
		String retStr = "";												// 결과값 리턴 변수
		
		if(str == null || "null".equals(str) || "".equals(str) || "0".equals(str)){		// null 이나 공백, '0' 이면 '0' 세팅.
			retStr = rtn ; 
		}else{															// 값이 있으면
		
			System.out.println("들어온 숫자값 ::: "+str);
			int str_length = str.length();			// 원 str 길이
			int unit_length = unit.length();		// 단위 길이
			int add_point = 0;						// 소수점 변경시에 0 추가할 길이.
			String first_str = "0.";
			if(unit_length > str_length){ 
				add_point = unit_length - str_length;
				System.out.println("반복 ::: "+add_point);
				for(int i=0; i< add_point; i++){
					first_str += "0";
					
				}
				str = (first_str+str);
				
			}
			else str = str.substring(0, str_length-unit_length+1);
			System.out.println("단위에 따른 숫자값 ::: "+str);
			
			
			
			str = str.replaceAll(" ", "");								// 공백제거.
			if(".".equals(str.substring(0, 1))) str = "0"+str;			// 소수점 (.XXX --> 0.XXX)
			
			String tempStr = str.indexOf(".") == -1? str: str.substring(0, str.indexOf("."));					// 정수 부분
			String tailStr = str.indexOf(".") == -1? "": str.substring(str.indexOf("."), str.length());			// 소수점 부분
			
			char strChar ; 
			 
			int len = tempStr.length() ;
		    // System.out.print("#########" + tempStr +" : " + sb2.length()) ; 
			int commaIndex = (len-1)%3; 
			for (int i = 0; i < tempStr.length(); i++) {
				strChar = tempStr.charAt(i) ; 
				sb.append(strChar) ; 
				if(i==commaIndex && i<len-1){
					sb.append(","); 
					commaIndex +=3 ; 
				}
			}
			
			retStr = sb.toString()+tailStr;
			
			// 마이너스값 처리. (-,XXX,XXX,XXX ===> -XXX,XXX,XXX)
			if("-".equals(retStr.substring(0, 1))&& ",".equals(retStr.substring(1, 2)))
				retStr = "-"+retStr.substring(2, retStr.length());
			
			
			// 마이너스값 색상 처리. (빨간색)
			if("-".equals(retStr.substring(0, 1))) retStr = "<font color='red'>"+ retStr +"</font>";
			
		}
		
		return retStr ; 
	}
	
	/**
	 * 숫자를 3자리마다 , 찍어준다 
	 * @param intValue
	 * @return String 형
	 */
	public static String getComma(int intValue){
		return getComma(String.valueOf(intValue)) ; 
	}
	/**
	 * 숫자를 3자리마다 , 찍어준다 
	 * @param longValue
	 * @return String 형
	 */
	public static String getComma(long longValue){
		return getComma(String.valueOf(longValue)) ; 
	}
	/**
	 * 숫자를 3자리마다 , 찍어준다 
	 * @param doubleValue
	 * @return String 형
	 */
	public static String getComma(double doubleValue){
		return getComma(String.valueOf(doubleValue)) ; 
	}
	/**
	 * 숫자를 3자리마다 , 찍어준다 
	 * @param Object
	 * @return String 형
	 */
	public static String getComma(Object objValue){
		return getComma(nvl(objValue,"")) ; 
	}
	/**
	 * 숫자를 3자리마다 , 찍어준다 
	 * @param intValue
	 * @return String 형
	 */
	public static String getComma(int intValue,String unit){
		return getComma(String.valueOf(intValue),unit) ; 
	}
	/**
	 * 숫자를 3자리마다 , 찍어준다 
	 * @param longValue
	 * @return String 형
	 */
	public static String getComma(long longValue,String unit){
		return getComma(String.valueOf(longValue),unit) ; 
	}
	/**
	 * 숫자를 3자리마다 , 찍어준다 
	 * @param doubleValue
	 * @return String 형
	 */
	public static String getComma(double doubleValue,String unit){
		return getComma(String.valueOf(doubleValue),unit) ; 
	}
	/**
	 * 숫자를 3자리마다 , 찍어준다 
	 * @param Object
	 * @return String 형
	 */
	public static String getComma(Object objValue,String unit){
		return getComma(nvl(objValue,""),unit) ; 
	}
	
	/**
	 * 문자의 왼쪽에 특정값을 채워놓고 원하는 길이 만큼 반환 한다 
	 * @param str	문자열 
	 * @param fill	채울 문자 
	 * @param len	원하는길이
	 * @return		String
	 */
	public static String leftFill(String str,String fill ,int len){
		StringBuffer sb = new StringBuffer() ;
		for (int i = 0; i < len; i++) {
			sb.append(fill) ; 
		}
		sb.append(str) ; 
		return sb.substring(sb.length()-len) ; 
	}
	
	
	/**
	 * 문자의 오른쪽 에 특정값을 채워놓고 원하는 길이 만큼 반환 한다 
	 * @param str	문자열 
	 * @param fill	채울 문자 
	 * @param len	원하는길이
	 * @return		String
	 */
	public static String rightFill(String str,String fill,int len){
		StringBuffer sb = new StringBuffer();
		sb.append(str) ; 
		for (int i = 0; i < len; i++) {
			sb.append(fill) ; 
		}
		return sb.substring(0,len) ; 
	}
	
	/**
	 * 문자열을 특정 길이보다 길면 . 왼쪽부터 특정 길이 까지 잘라낸 값을 반환한다
	 * @param str			문자열
	 * @param limitLength	한개 문자열 길이
	 * @return				String
	 */
	public static String leftCut(String str,int limitLength){
		String resultStr = str ; 
		if(str.length() > limitLength){
			resultStr = str.substring(0 , limitLength) ; 
		}
		return resultStr ;
	}
	
	/**
	 * 문자열을 특정 길이보다 길면 . 오른쪽부터 특정 길이 까지 잘라낸 값을 반환한다
	 * @param str			문자열
	 * @param limitLength	한개 문자열 길이
	 * @return				String
	 */
	public static String rigthCut(String str,int limitLength){
		String resultStr = str ; 
		if(str.length() > limitLength){
			resultStr = str.substring(0, limitLength) ; 
		}
		return resultStr ; 
	}
	
	/**
	 * 문자열을 특정 길이보다 길면 . 오른쪽부터 특정 길이 까지 잘라낸 후 ... 붙인후 문자열을 반환다
	 * @param str			문자열
	 * @param limitLength	한개 문자열 길이
	 * @return				String
	 */
	public static String rigthCutComma(String str,int limitLength){
		if(str == null){return "";} 
		if(str.length() == 0 ){return "";}
		if(str.length() <= limitLength ){return str;} 
		return rigthCut(str,limitLength) + "..." ; 
	}
	
	/**
	 * 사용자가 정한 길이 만큼 숫자의 앞에 0을 붙여준다 . 32 - > 000032
	 * @param num
	 * @param len
	 * @return
	 */
	public static String zeroStr(int num,int len){
		return leftFill(Integer.toString(num),"0",len) ; 
	}
	
	/**
	 * 문자열의 일부분을 다른 문자열로 치환한다.
	 * @param argSource   원본 문자열
	 * @param argQuery	  문자열에서 찾을 패턴
	 * @param argReplace  치환할 문자열
	 * @return 치환된 문자열
	 */
	public static String replace(String argSource, String argQuery,String argReplace) {
		if (argSource == null || argQuery == null || argReplace == null)
			return argSource;
		
		int index = 0;
		int iQueryLen = argQuery.length();
		int iStart = 0;
		StringBuffer sb = new StringBuffer();
		index = argSource.indexOf(argQuery);
		while (index > -1) {
			sb.append(argSource.substring(iStart, index));
			sb.append(argReplace);
			iStart = index + iQueryLen;
			index = argSource.indexOf(argQuery, iStart);
		}
		sb.append(argSource.substring(iStart, argSource.length()));
		return sb.toString();
	}
	
	/**
	 * html 테그를 html 화면 출력용으로 변환한다.
	 * @param str   문자열
	 * @return 		문자열
	 */
	public static String htmlToString(String str) {

		str = replace(str, "<", "&lt;");
		str = replace(str, ">", "&gt;");
		str = replace(str, "\"", "&quot;");
		str = replace(str, "\n", "<br>\n");
		str = replace(str, "  ", " &nbsp;");
		str = replace(str, "\t", " &nbsp; &nbsp;");

		return str;
	}
	
	/**
	 * html 을 문자열로 한것을 html 테그로 변경한다 . 
	 * @param str
	 * @return
	 */
	 public static String convertForHTML(String str) {
	        String res = null;
	        res = str.replace((char)DoubleQuot.byteValue(), (char)SingleQuot.byteValue());
	        res = res.replace((char)EnterUnix.byteValue(), (char)Space.byteValue());
	        res = res.replace((char)Enter.byteValue(), (char)Space.byteValue());
	        
	        return res;
	    }
	
	/**
	 * 오브젝트가 null 일때 다른 문자열을 리턴한다.
	 * @param obj	  Object
	 * @param ifNull  null일때 반환할 문자열
	 * @return String.valueOf(obj)
	 */
	public static String nvl(Object obj, String ifNull) {
		if (obj == null){return ifNull;}
		return String.valueOf(obj);
	}
		
	/**
	 * 문자열이 null 일때 다른 문자열을 리턴한다.
	 * @param str	  문자열
	 * @param ifNull  null일때 반환할 문자열
	 * @return 문자열
	 */
	public static String nvl(String str, String ifNull) {
		if (str == null || "".equals(str) || "null".equals(str) || "NULL".equals(str)){return ifNull;}
		return str;
	}
	
	/**
	 * 문자열이 null 일때 다른 숫자형으로 변환한다 
	 * @param str
	 * @param ifNull
	 * @return
	 */
	public static int nvl(String str,int ifNull){
		if(str == null || "".equals(str)){return ifNull;} 
		return Integer.parseInt(str) ; 
	}
	
	
	
	
	/**
	 * 문자열이 null일때 ""를 리턴한다.
	 * @param str  문자열
	 * @return 	   문자열
	 */
	public static String nvl(String str) {
		if (str == null || "".equals(str)){return "";}
		return str;
	}
    
}
