package com.util.mail;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.MimeMessageHelper;

public class EmailMessageHelper {

	/**
	 * Mail Validation
	 * Required : FROM, TO, SUBJECT, TEMPLATE_ID
	 * @throws MessagingException
	 */
	public static void mailValidation(EmailMessage message) throws MessagingException {
		if( message.getFrom()== null )						// from
			throw new MessagingException("FROM is not null");
		if( message.getTo().size() == 0 )					// to
			throw new MessagingException("TO is not null");
		if( message.getSubject() == null )					// subject
			throw new MessagingException("SUBJECT is not null");
		if( message.isTemplate() ) {
			if( message.getTemplateId() == null )			// template id
				throw new MessagingException("TEMPLATE ID is not null");
		} else {
			if( message.getSimpleContent() == null )
				throw new MessagingException("MAIL CONTENT is not null");
		}
	}	
	
	/**
	 * Create MimeMessageHelper
	 * @param emailMessage
	 * @param mimeMessage
	 * @return
	 * @throws MessagingException
	 */
	public static MimeMessageHelper createMimeMessageHelper(EmailMessage emailMessage, MimeMessage mimeMessage) throws MessagingException {
		MimeMessageHelper helper = null;
		if(emailMessage.getEncoding() != null)
			helper = new MimeMessageHelper(mimeMessage, true, emailMessage.getEncoding());
		else
			helper = new MimeMessageHelper(mimeMessage, true);
		helper.setValidateAddresses(true);
		return helper;
	}
	
	/**
	 * Add Attachment
	 * @param emailMessage
	 * @param helper
	 * @throws MessagingException
	 */
	public static void addAttachment(EmailMessage emailMessage, MimeMessageHelper helper) throws MessagingException {
		if( emailMessage.hasAttachment() ) {
			Map<String, Resource> attachments = emailMessage.getAttachments();
			
			Iterator<Entry<String, Resource>> iterator = attachments.entrySet().iterator();
			for( ; iterator.hasNext(); ) {
				Entry<String, Resource> entry = iterator.next();
				helper.addAttachment(entry.getKey(), entry.getValue());
			}
			
//			Set<String> keys = attachments.keySet();
//			for( Iterator<String> i = keys.iterator(); i.hasNext(); ) {
//				String key = i.next();
//				Resource value = attachments.get(key);
//				helper.addAttachment(key, value);
//			}
		}
	}

	/**
	 * Add Inline
	 * @param emailMessage
	 * @param helper
	 * @throws MessagingException
	 */
	public static void addInline(EmailMessage emailMessage, MimeMessageHelper helper) throws MessagingException {
		if( emailMessage.hasInline() ) {
			Map<String, Resource> inlines = emailMessage.getInlines();
			
			Iterator<Entry<String, Resource>> iterator = inlines.entrySet().iterator();
			for( ; iterator.hasNext(); ) {
				Entry<String, Resource> entry = iterator.next();
				helper.addInline(entry.getKey(),(FileSystemResource) entry.getValue());
			}
			
//			Set<String> keys = inlines.keySet();
//			for( Iterator<String> i = keys.iterator(); i.hasNext(); ) {
//				String key = i.next();
//				Resource value = inlines.get(key);
//				helper.addInline(key, value);
//			}
		}
	}

	/**
	 * Set Mail Bcc
	 * @param emailMessage
	 * @param helper
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public static void setBcc(EmailMessage emailMessage, MimeMessageHelper helper) throws MessagingException, UnsupportedEncodingException {
		if (emailMessage.getBcc().size() > 0) {
			for (EmailAddress address : emailMessage.getBcc()) {
				if (address.hasPersonal())
					helper.addBcc(address.getEmail(), address.getPersonal());
				else
					helper.addBcc(address.getEmail());
			}
		}
	}

	/**
	 * Set Mail CC
	 * @param emailMessage
	 * @param helper
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public static void setCc(EmailMessage emailMessage, MimeMessageHelper helper) throws MessagingException, UnsupportedEncodingException {
		if (emailMessage.getCc().size() > 0) {
			for (EmailAddress address : emailMessage.getCc()) {
				if (address.hasPersonal())
					helper.addCc(address.getEmail(), address.getPersonal());
				else
					helper.addCc(address.getEmail());
			}
		}
	}

	/**
	 * Set Mail Content
	 * @param emailMessage
	 * @param messageContent
	 * @param helper
	 * @throws MessagingException
	 */
	public static void setContent(EmailMessage emailMessage, String messageContent, MimeMessageHelper helper) throws MessagingException {
		if( emailMessage.isHtmlContent() )
			helper.setText(messageContent, true);
		else
			helper.setText(messageContent, false);
	}

	/**
	 * Set Mail From
	 * @param emailMessage
	 * @param helper
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public static void setFrom(EmailMessage emailMessage, MimeMessageHelper helper) throws MessagingException, UnsupportedEncodingException {
		if(emailMessage.getFrom().hasPersonal() )
			helper.setFrom(emailMessage.getFrom().getEmail(), emailMessage.getFrom().getPersonal());
		else
			helper.setFrom(emailMessage.getFrom().getEmail());
	}

	/**
	 * Set Mail ReplyTo
	 * @param emailMessage
	 * @param helper
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public static void setReplyTo(EmailMessage emailMessage, MimeMessageHelper helper) throws MessagingException, UnsupportedEncodingException {
		if( emailMessage.getReplyTo() != null ) {
			if(emailMessage.getReplyTo().hasPersonal() )
				helper.setReplyTo(emailMessage.getReplyTo().getEmail(), emailMessage.getReplyTo().getPersonal());
			else
				helper.setReplyTo(emailMessage.getReplyTo().getEmail());
		}
	}

	/**
	 * Set Mail Subject
	 * @param emailMessage
	 * @param helper
	 * @throws MessagingException
	 */
	public static void setSubject(EmailMessage emailMessage, MimeMessageHelper helper) throws MessagingException {
		helper.setSubject(emailMessage.getSubject());
	}

	/**
	 * Set Mail To
	 * @param emailMessage
	 * @param helper
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public static void setTo(EmailMessage emailMessage, MimeMessageHelper helper) throws MessagingException, UnsupportedEncodingException {
		for(EmailAddress address : emailMessage.getTo() ) {
			if( address.hasPersonal() )
				helper.addTo(address.getEmail(), address.getPersonal());
			else
				helper.addTo(address.getEmail());
		}
	}

}
