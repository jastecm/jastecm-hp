package com.util.mail;

import java.io.File;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.util.Assert;

public class TemplateEmailSender implements InitializingBean {

	private static JavaMailSenderImpl mailSender;
	private static TemplateHelper templateHelper;

	@SuppressWarnings("static-access")
	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
	}

	@SuppressWarnings("static-access")
	public void setTemplateHelper(TemplateHelper templateHelper) {
		this.templateHelper = templateHelper;
	}

	public void afterPropertiesSet() throws Exception {
		Assert.notNull(mailSender, "'mailSender' property is required");
		Assert.notNull(templateHelper, "'templateHelper' property is required");
	}

	// public static void sendEMail(EmailMessage emailMessage, Map<String,
	// Object> values) throws Exception {
	// // 1. EmailMessage valid check
	// // Required : From, To, Subject, Template ID
	// emailMessage.setFrom(mailSender.getUsername());
	//
	// EmailMessageHelper.mailValidation(emailMessage);
	// // 1. Mail Content create (Template -> Content)
	// String mailContent =
	// templateHelper.mergeTemplate(emailMessage.getTemplateId(), values,
	// emailMessage.getTemplateContent(), emailMessage.getEncoding());
	// // 2. MailPreparator create
	// ContentMessagePreparator preparator = new
	// ContentMessagePreparator(emailMessage, mailContent);
	// // 3. Mail Send
	// // mailSender.setUsername(emailMessage.getFrom().getEmail());
	// // mailSender.setPassword(emailMessage.getFromPw());
	// mailSender.send(preparator);
	// }

	public static void sendEMail(EmailMessage emailMessage,	Map<String, Object> values) throws Exception {
//		String host = "211.173.76.14"; // SMTP서비스가 설치된 컴터 IP(익명액세스 가능 상태)
		String host = "10.3.1.21";
		String name = "DreamJobGo"; // 보내는 사람명
		String subject = emailMessage.getSubject(); // 제목
		String from = "dreamjobgo@e-cluster.net"; // 보내는 사람Mail주소.
		String to = emailMessage.getTo().get(0).getEmail(); // 받는 사람 Mail주소.
		System.out.println("host = " + host);
		System.out.println("name = " + name);
		System.out.println("subject = " + subject);
		System.out.println("from = " + from);
		System.out.println("to = " + to);
		
		// 메일 호스트 설정.
		Properties props = System.getProperties();
		props.put("mail.smtp.host", host);

		// Session
		//Session session = Session.getDefaultInstance(props, new Authenticator(){
		//	public PasswordAuthentication getPasswordAuthentication() {
		//		return new PasswordAuthentication("dreamjobgo", "dreamjobgo");
		//   }
		//});
		
		Session session = Session.getInstance(props, new Authenticator(){
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("dreamjobgo", "dreamjobgo");
		    }
		});

        // Define message
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from, name));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        message.setSubject(subject);
        // Handle text
        String body = null;
        /** 상시채용 관리자 메일 */
        if(values.get("mailType").equals("type1")){
        	MimeBodyPart messageBodyPart = new MimeBodyPart();
            
    		Map<String, File> files = emailMessage.getAttatchsFile();
    		String[] key = files.keySet().toArray(new String[0]);
    		for (int i = 0; i < key.length; i++) {
    			File file = files.get(key[i]);
    			messageBodyPart = new MimeBodyPart();
    			messageBodyPart.attachFile(file);
    		}
    		
        	body = getMailHtml();
        	
        	MimeMultipart multipart = new MimeMultipart("mixed");
        	MimeBodyPart textPart = new MimeBodyPart();
            textPart.setHeader("Content-Type", "text/plain; charset=\"euc-kr\"");
            textPart.setContent(body, "text/html; charset=euc-kr");
            multipart.addBodyPart(textPart);
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            // Send message
            Transport.send(message);
        }
        /** 상시채용 입사지원 당사자 */
        else if(values.get("mailType").equals("type2")){
            message.setContent(getUserMailHtml(), "text/html; charset=euc-kr");
            // Send message
            Transport.send(message);
        }
        
	}

	/**
	 * 상시채용 입사지원 관리자 메일
	 * 
	 * @return
	 */
	private static String getMailHtml() {
		StringBuffer sb = new StringBuffer();
		sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"ko\" xml:lang=\"ko\">");
		sb.append("<html>");
		sb.append("<div id=\"contents\">");
		sb.append("<img src=\"http://dreamjobgo.or.kr/resource/images/EMail/email.jpg\" />");
		sb.append("</div>");
		sb.append("</body>");
		sb.append("</html>");
		return sb.toString();
	}
	
	/**
	 * 상시채용관 입사지원자 메일
	 * 
	 * @return
	 */
	private static String getUserMailHtml() {
		StringBuffer sb = new StringBuffer();
		sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"ko\" xml:lang=\"ko\">");
		sb.append("<html>");
		sb.append("<div id=\"contents\">");
		sb.append("<img src=\"http://dreamjobgo.or.kr/resource/images/EMail/email.jpg\" />");
		sb.append("</div>");
		sb.append("</body>");
		sb.append("</html>");
		return sb.toString();
	}
}
