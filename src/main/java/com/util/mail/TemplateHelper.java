/*=========================================================
*Copyright(c) 2010 CyberLogitec
*@FileName : TemplateHelper.java
*@FileTitle : TemplateHelper
*Open Issues :
*Change history :
*@LastModifyDate : 
*@LastModifier : 
*@LastVersion : 1.0
* 
* 1.0 Creation
=========================================================*/
package com.util.mail;

import java.util.Map;

public interface TemplateHelper {
	public String mergeTemplate(String templateId, Map<String, Object> values, String templateContent, String encoding) throws Exception;
}
