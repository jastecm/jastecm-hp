package com.util.mail;

public enum TemplateConstants {
	// TEMPLATE Type
	/** HTML */
	HTML("HTML"),
	
	/** PLAIN TEXT */
	TEXT("TEXT")
	;
	
	/** value */
	private String value;
	
	/**
	 * Constructor
	 * @param value
	 */
	TemplateConstants(String value) {
		this.value = value;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return this.value;
	}
	
	/**
	 * Compares this object with the specified object
	 * @param compareValue
	 * @return	true if this object is the same as the compareValue argument; false otherwise.
	 */
	public boolean compareTo(String compareValue) {
		return this.value.equalsIgnoreCase(compareValue);
	}
}
