package com.util.mail;

public class EmailAddress {

	private String email;
	private String personal;
	
	public EmailAddress(String email) {
		this.email = email;
	}
	
	public EmailAddress(String email, String personal) {
		this.email = email;
		this.personal = personal;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPersonal() {
		return personal;
	}
	
	public void setPersonal(String personal) {
		this.personal = personal;
	}
	
	public boolean hasPersonal() {
		boolean result = false;
		if(this.personal != null)
			result = true;
		
		return result;
	}
}
