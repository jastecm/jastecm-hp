package com.util.mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

public class ContentMessagePreparator implements MimeMessagePreparator {
	
	private EmailMessage emailMessage;
	private String messageContent;
	
	public ContentMessagePreparator(EmailMessage emailMessage, String mailContent) throws MessagingException {
		this.emailMessage = emailMessage;
		this.messageContent = mailContent;
	}
	
	public void prepare(MimeMessage mimeMessage) throws Exception {

		// create MimeMessageHelper ( encoding )
		MimeMessageHelper helper = EmailMessageHelper.createMimeMessageHelper(emailMessage, mimeMessage);

		// set From : REQUIRED 
		EmailMessageHelper.setFrom(emailMessage, helper);

		// set ReplyTo
		EmailMessageHelper.setReplyTo(emailMessage, helper);
		
		// add TO : REQUIRED
		EmailMessageHelper.setTo(emailMessage, helper);
		
		// add CC
		EmailMessageHelper.setCc(emailMessage, helper);
		
		// add BCC
		EmailMessageHelper.setBcc(emailMessage, helper);
		
		// setSubject : REQUIRED
		EmailMessageHelper.setSubject(emailMessage, helper);
		
		// setContent
		EmailMessageHelper.setContent(emailMessage, messageContent, helper);
		
		// add Attach Resource
		EmailMessageHelper.addAttachment(emailMessage, helper);

		// add Inline Resource
		EmailMessageHelper.addInline(emailMessage, helper);
	}	
}