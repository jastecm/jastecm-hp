package com.common;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


public class AdminSecurityFilter implements Filter {

	private List<String> nonSecureUrlList;

	public List<String> getNonSecureUrlList() {
		return nonSecureUrlList;
	}

	public void setNonSecureUrlList(List<String> nonSecureUrlList) {
		this.nonSecureUrlList = nonSecureUrlList;
	}

	public void destroy() {
		// TODO Auto-generated method stub
	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String servletPath = httpRequest.getServletPath();
		String reqURI = httpRequest.getRequestURI();
		System.out.println("서블릿패스"+ servletPath);
		System.out.println("reqURI"+ reqURI);
		
		chain.doFilter(httpRequest, response);
		/*
		if(user == null && !nonSecureUrlList.contains(httpRequest.getServletPath())){
			
			request.getRequestDispatcher("/login/page.do").forward(request, response);
			
		}else{	
			
		}
		*/
	}
	
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
	}
	
}
