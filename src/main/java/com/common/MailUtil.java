package com.common;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

public class MailUtil {
	
	/**
     * SMTP를 이용하여 메이을 발송한다. (인증을 위해 Gmail 계정정보가 필요하며, 첨부파일 처리는 없다.)
     *
     * @param from 보내는 사람 메일 주소
     * @param fromName 보내는 사람 표현 이름
     * @param to 받는 메일 주소
     * @param cc 참조 메일 주소
     * @param bcc 숨은 참조 메일 주소
     * @param subject 메일 제목
     * @param content 메일 내용
     * @param attachFiles 첨부파일
     * @throws Exception
     */
   public void sendSmtpMail(String from,
           String fromName, String[] to, String[] cc, String[] bcc,
           String subject, String content, File[] attachFiles)
           throws Exception {
       Message msg = createMessage(from, fromName, to, cc,
               bcc, attachFiles); // create message

       // subject & content
       msg.setSubject(MimeUtility.encodeText(subject, "UTF-8", "B"));
       msg.setSentDate(new Date());
       
       
       Object contentObj = msg.getContent();
       if (contentObj != null && contentObj instanceof Multipart) {
           MimeBodyPart contentPart = new MimeBodyPart();
           contentPart.setText(content,"utf-8","html");
           contentPart.setHeader("Content-Type", "text/html; charset=UTF-8");

           Multipart mPart = (Multipart) contentObj;
           mPart.addBodyPart(contentPart);
       } else {
           msg.setContent(content, "text/html; charset=UTF-8");
       }
	
       Transport.send(msg); // send mail
   }
   
// 메시지를 생성한다.
   private Message createMessage(
           String from, String fromName, String[] to, String[] cc,
           String[] bcc, File[] attachFiles) throws Exception {
	   Properties props = new Properties();
	   
	   EmailAuthenticator authenticator = new EmailAuthenticator();
	   
//	   props.put("mail.transport.protocol", protocol);
//	   props.put("mail.smtp.host", host);
//	   props.put("mail.smtp.port", port);
	   props.put("mail.smtp.host", "smtp.gmail.com");
	   props.put("mail.smtp.auth", "true");
	   props.put("mail.smtp.port", "465");
	   props.put("mail.smtp.socketFactory.fallback", "false");
	   props.put("mail.smtp.required", "true");
	   props.put("mail.smtp.enable", "true");
	   props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	   Session session = Session.getInstance(props, authenticator);
       // create message
       Message msg = new MimeMessage(session);

       if (fromName != null) {
           msg.setFrom(new InternetAddress(from, MimeUtility.encodeText(
                   fromName, "UTF-8", "B")));
       } else {
           msg.setFrom(new InternetAddress(from));
       }

       // add to
       InternetAddress[] toAddr = null;
       if (to != null) {
           toAddr = new InternetAddress[to.length];
           for (int i = 0; i < to.length; i++) {
               toAddr[i] = new InternetAddress(to[i]);
           }
       }
       
       msg.setRecipients(Message.RecipientType.TO, toAddr);

       // add cc (참조)
       if (cc != null) {
           InternetAddress[] ccAddr = new InternetAddress[cc.length];
           for (int i = 0; i < cc.length; i++) {
               ccAddr[i] = new InternetAddress(cc[i]);
           }

           msg.setRecipients(Message.RecipientType.CC, ccAddr);
       }

       // add bcc (숨은참조)
       if (bcc != null) {
           InternetAddress[] bccAddr = new InternetAddress[bcc.length];
           for (int i = 0; i < bcc.length; i++) {
               bccAddr[i] = new InternetAddress(bcc[i]);
           }

           msg.setRecipients(Message.RecipientType.BCC, bccAddr);
       }

       if (attachFiles != null) {
           // 파일 첨부
           Multipart multipart = new MimeMultipart();

           for (File attachFile : attachFiles) {
               if (attachFile != null && attachFile.exists()) {
                   MimeBodyPart mimeBodyPart = new MimeBodyPart();

                   mimeBodyPart.setDataHandler(new DataHandler(
                           new FileDataSource(attachFile)));
                   mimeBodyPart.setFileName(attachFile.getName());

                   multipart.addBodyPart(mimeBodyPart);
               }
           }

           msg.setContent(multipart, "text/html; charset=UTF-8");
       }

       return msg;
   }
   
   class EmailAuthenticator extends Authenticator {
	   
       private String id; 
       private String pw; 

       public EmailAuthenticator(){
           this.id = "upjjw2682jp@gmail.com";
           this.pw = "jjw26829524";
       }
        
       public EmailAuthenticator(String id, String pw) {
           this.id = id;
           this.pw = pw;
       }

       protected PasswordAuthentication getPasswordAuthentication() {
           return new PasswordAuthentication(id, pw);
       }

   }
}
