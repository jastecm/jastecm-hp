package jbiz.vo;

import com.common.PageNavigator;


public class BoardVO  {
	
	private PageNavigator pageNavi; 
	private String searchBoardType;
	private String searchContentType;
	private String searchText;
	
	private int rNum;
	
	private String id;
	private String boardType;
	private String regDate;
	private String title;
	private String contentType;
	private String contentTypeNm;
	private String contents;
	private String fileId;
	private String orgFileNm;
	
	public String getSearchContentType() {
		return searchContentType;
	}
	public void setSearchContentType(String searchContentType) {
		this.searchContentType = searchContentType;
	}
	public PageNavigator getPageNavi() {
		return pageNavi;
	}
	public void setPageNavi(PageNavigator pageNavi) {
		this.pageNavi = pageNavi;
	}
	public String getSearchBoardType() {
		return searchBoardType;
	}
	public void setSearchBoardType(String searchBoardType) {
		this.searchBoardType = searchBoardType;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public int getrNum() {
		return rNum;
	}
	public void setrNum(int rNum) {
		this.rNum = rNum;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBoardType() {
		return boardType;
	}
	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getContentTypeNm() {
		return contentTypeNm;
	}
	public void setContentTypeNm(String contentTypeNm) {
		this.contentTypeNm = contentTypeNm;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getOrgFileNm() {
		return orgFileNm;
	}
	public void setOrgFileNm(String orgFileNm) {
		this.orgFileNm = orgFileNm;
	}
 
	
}
