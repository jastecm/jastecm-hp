package jbiz.com.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import jbiz.com.dao.CommonDao;
import jbiz.com.service.CommonService;
import jbiz.service.web.ServiceController;
import jbiz.vo.FileVO;
import jbiz.vo.ParamVO;

import org.apache.commons.fileupload.FileItem;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.common.FileCopy;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfDestination;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Service("commonService")
@Transactional
public class CommonServiceImpl implements CommonService {
	@Resource(name = "commonDao")
	private CommonDao dao;
	
	public String createMd5Uuid() throws Exception{
		return dao.createMd5Uuid();
	}
	
	public String insertFileUpload(FileVO vo) throws Exception{
		
		String uuid = createMd5Uuid();
		vo.setUuid(uuid);
		dao.insertFileUpload(vo);
		return uuid;
	}
	
	public FileVO getFile(String vo) throws Exception{
		return dao.getFile(vo);
	}
	
	/**
     * 파일업로드 리퀘스트 파싱
     * @param iter
     * @param fMidir
     * @return
     */
	public HashMap<String,String> getFileUpload(Iterator iter, File fMidir) {
		HashMap file_map = new HashMap();
		String file_name = "";
		List fileList = new ArrayList();
		List fileNmList = new ArrayList();
		List file2List = new ArrayList();
		List file2NmList = new ArrayList();
		try{
			while(iter.hasNext()){
				FileItem item = (FileItem)iter.next();
				//파일이 아닌경우 (필드 파라메터)
				if(item.isFormField()){
					String key = item.getFieldName();    
					//한글 꺠지는거 방지 utf-8로 재설정
					String value = item.getString("utf-8");
					//System.out.println("key=" + key + " value=" + value);
					file_map.put(key, value);
				
				//파일처리 + 중복처리	
				}else{
					FileVO vo = new FileVO();
					
					String key = item.getFieldName();
					
					//한글 계속 깨짐 
					String tempfileName = new String(item.getName().getBytes("euc-kr"),"KSC5601");
					String orgFileName = "";
					String saveFileName = "";
					
					//파일이름이 존재하고 업로드할 파일이 존재한다면 업로드 시작
					if(!tempfileName.equals("") && tempfileName != null){
						//System.out.println("filefieldname=" + key + " fileName=" + tempfileName);
						//파일명 추출
						int idx = tempfileName.lastIndexOf(System.getProperty("file.separator"));
						
						String fileSize = String.valueOf(item.getSize());
						vo.setFileSize(fileSize);
						
						//파일 풀 name(확장자포함)
						orgFileName = tempfileName.substring(idx + 1);
						vo.setOrgFileNm(orgFileName);
						
						//확장자 추출
						String fileExt = orgFileName.substring(orgFileName.lastIndexOf("."));
						vo.setFileExt(fileExt);
						
						//파일명 추출
						orgFileName = orgFileName.substring(0,orgFileName.lastIndexOf("."));
						
						//저장될 파일명
						saveFileName = orgFileName+fileExt;
						
						//중복 파일 처리
						File save_file = new File(fMidir.getPath(),saveFileName);
						//파일이 존재한다면
						int i = 0;
						while(save_file.exists()){
							i++;
							//fileName_i
							saveFileName = orgFileName+"_"+i+fileExt;
							save_file = new File(fMidir.getPath(),saveFileName);
						}
							
						vo.setFilePath(fMidir.getPath());
						vo.setSaveFileNm(saveFileName);						
						item.write(save_file);
						//중복 파일 처리 End
						
						String uuid = insertFileUpload(vo);
						
						if(key.equals("file")){
							fileList.add(uuid);
							fileNmList.add(orgFileName+fileExt);
							file_map.put(key, fileList);
							file_map.put(key+"Nm", fileNmList);
						}
						else{
							file2List.add(uuid);
							file2NmList.add(orgFileName+fileExt);
							file_map.put(key, file2List);
							file_map.put(key+"Nm", file2NmList);
						}
						
						
						
					}else{
						file_map.put(key, null);
					}//End Of FileName if
				}                           
			}                
		}catch(Exception e){
		}finally{}
		
		return file_map;
	}

	@Override
	public List<String> getManufacture(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getManufacture(vo);
	}

	@Override
	public List<String> getModelMaster(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getModelMaster(vo);
	}

	@Override
	public List<String> getYear(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getYear(vo);
	}

	@Override
	public List<String> getModelHeader(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getModelHeader(vo);
	}

	@Override
	public List<String> getFuel(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getFuel(vo);
	}

	@Override
	public List<String> getTransmission(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getTransmission(vo);
	}

	@Override
	public List<String> getVolume(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getVolume(vo);
	}
	
	public String getBCMsurport(ParamVO vo) throws Exception{
		return dao.getBCMsurport(vo);
	}
	
	public static final String RELATIVE_PATH_PREFIX = ServiceController.class.getResource("").getPath().substring(0, ServiceController.class.getResource("").getPath().lastIndexOf("WEB-INF"))
    		+ "common" + System.getProperty("file.separator") + "ref/";
	
	public File exportEstimateSheet(String device, String vdas, String corp,
			String name, String mail) throws Exception{
		
		
		File out = null;
		
		try {
			String os = System.getProperty("os.name");
			boolean dev = os.equals("Windows 7");		
			String resource = RELATIVE_PATH_PREFIX+"estimateSheet.xlsx";
			
			String outDir = "c:/exportExcel/estimateSheet/";
			
			if(!dev){
				outDir = "/var/lib/viewcar/excelReport/jbiz/estimateSheet/";
			}
			
			FileCopy s = new FileCopy();
			
			java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyMMdd");
			String toDay = fmt.format(new Date());
			int outFileName = Integer.parseInt(toDay+"01");
			
			File f = new File(outDir);
			String[] files = f.list();
			for(String outFileChk : files){
				int outFileChkInt = Integer.parseInt(outFileChk.substring(0,8));
				if(outFileName <= outFileChkInt) outFileName = outFileChkInt+1;
			}
			
			
			String outSrc = outDir+outFileName+".xlsx";
			String outPdf = outDir+outFileName+".pdf";
			
			try {
				s.copy(resource, outSrc);
				out = new File(outSrc);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			XSSFWorkbook workBook = new XSSFWorkbook(new FileInputStream(out));
			XSSFSheet sheet = null;
			XSSFRow row = null;
			XSSFCell cell = null;
			/** 첫번째 시트*/
			sheet = workBook.getSheetAt(0);
			
			row = sheet.getRow(2);
			cell = row.getCell(0);
			cell.setCellValue(corp+" 귀하");
			
			row = sheet.getRow(3);
			cell = row.getCell(1);
			cell.setCellValue(name+" 님");
			
			fmt = new java.text.SimpleDateFormat("yyyy.MM.dd");
			
			row = sheet.getRow(2);
			cell = row.getCell(6);
			cell.setCellValue("작성일 : "+fmt.format(new Date()));
			
			row = sheet.getRow(3);
			cell = row.getCell(4);
			cell.setCellValue("엠전견 "+outFileName);
			
			int deviceInt = Integer.parseInt(device);
			int vdasInt = Integer.parseInt(vdas);
			
			row = sheet.getRow(14);
			cell = row.getCell(3);
			cell.setCellValue(deviceInt);
			
			row = sheet.getRow(15);
			cell = row.getCell(3);
			cell.setCellValue(vdasInt);
			
			
			int tot = (deviceInt*37950)+(vdasInt*5000);
			row = sheet.getRow(27);
			cell = row.getCell(6);
			cell.setCellValue(tot);
			
			sheet.protectSheet("jastecm0797");
			//out
			FileOutputStream ops = new FileOutputStream(out);
			workBook.write(ops);
			
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			
		}
		return out;
	}
	
}