package jbiz.com.web;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jbiz.com.service.CommonService;
import jbiz.vo.FileVO;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.common.BasicController;
import com.common.Globals;
import com.util.string.StrUtil;

@Controller
public class CommonController extends BasicController{

	private Log logger = LogFactory.getLog(getClass());	
	
	@Autowired
	private CommonService commonService;
	
	
	/**
	* 파일업로드 공통
	* @param model
	* @return
	*/
	@RequestMapping(value="/com/fileUpload.do")
	public String fileUpload(HttpServletRequest request, HttpServletResponse response ,ModelMap model) throws Exception {
		long sTime = System.currentTimeMillis();
		HashMap map = new HashMap();
		String rtvPage = "jsonView";
		
		try{
			
			//utf-8면 실질적으로jsp에서는 화면이 깨짐 서블릿에서는 euc-kr 로 생성
			response.setCharacterEncoding("euc-kr");
			boolean file_result;
			//default upload path
			File f_midir = new File(Globals.FILE_PATH+System.getProperty("file.separator"));
			
			String os = System.getProperty("os.name");
			if(!os.equals("Windows 7")) f_midir = new File("/var/lib/viewcar/excelReport"+System.getProperty("file.separator")+"jbiz");
				
			file_result =  f_midir.mkdirs();
   
			//----------------------------파일 저장에 필요한 객체 생성 부분------------------------------------
			//디스크 파일 아이템 factory
			DiskFileItemFactory  factory = new DiskFileItemFactory();
			//업로드시 사용할 임시 메모리
			factory.setSizeThreshold(4096);
			//업로드시 사용할 temp path
			factory.setRepository(f_midir);
			
			ServletFileUpload upload = new ServletFileUpload(factory);
			//사이즈 제한
			upload.setSizeMax(Integer.parseInt(Globals.FILE_SIZE));
			
			//멀티파트로 넘어온지 여부
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);
			//System.out.println("멀티파트로 넘어온지 여부=" + isMultipart);
			
			//이 시점에서 temp path에 파일이 업로드 (파일뿐만아니라 필드값들도 일단 파일형태로 저장된다.)
			List items = upload.parseRequest(request);
			//System.out.println("list size : " + items.size());

			String uploadType = "";
			Iterator iter = items.iterator();
			//업로드된 파일 파싱
			iter = items.iterator();
			map = commonService.getFileUpload(iter,f_midir);
			logger.debug(map.toString());
   
			model.addAttribute("rtv","S");
			model.addAttribute("rtvMap",map);
			
		}catch(Exception e){  
			logger.error("FileUpload Exception : " + e.getMessage());
			model.addAttribute("rtv","F");
		}finally{
			long endTime = System.currentTimeMillis();
			logger.debug("FileUpload Loading Time... : " + (endTime - sTime));
		}
		
		return rtvPage;
    }
	
	/**
	 * 파일다운로드
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/com/getFile.do")
	public String getFile(HttpServletRequest request,
    		ModelMap model) throws Exception  {
		
		String uuid = StrUtil.nvl(request.getParameter("uuid"));
		if(!uuid.equals("")){
			//userId로 img경로 겟
			FileVO file = commonService.getFile(uuid);
			if(file == null){
				model.addAttribute("MSG", "");
				return "/error";
			}else{
				String imgPath = "";
				imgPath = file.getFilePath()+System.getProperty("file.separator")+file.getSaveFileNm();
				
				
				try {		
					File img = new File(imgPath);
					
					model.addAttribute("downloadFile",img);
					model.addAttribute("downloadFileNm",file.getOrgFileNm());
					
				} catch (Exception e) {
					model.addAttribute("MSG", "");
					logger.error(e);
					return "/error";
				}
			}		
			
			
		}
		
		
		return "download";
	}
}
