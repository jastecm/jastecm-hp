package jbiz.CS.web;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import jbiz.board.service.BoardService;
import jbiz.com.service.CommonService;
import jbiz.vo.BoardVO;
import jbiz.vo.CSVO;
import jbiz.vo.FileVO;
import jbiz.vo.ParamVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.common.BasicController;
import com.common.Globals;
import com.common.MailUtil;
import com.common.MailUtilServer;
import com.common.PageNavigator;
import com.util.string.StrUtil;

@Controller
public class CSController extends BasicController{

	private Log logger = LogFactory.getLog(getClass());	
	
	@Autowired
	private BoardService boardService;

	@Autowired
	private CommonService commonService;
	
	@RequestMapping(value="/cs/faq.do")
	public String faq(HttpServletRequest request,
    		ModelMap model, @ModelAttribute BoardVO reqVo) throws Exception  {
		
		String rtvPage = "/cs/faq";
		
		try {
			//1:자주하는질문 , 2:다운로드 , 3:채용공고 , 4:경영진의메세지 , 5 :새소식			
			if(StrUtil.isNullToEmpty(reqVo.getSearchBoardType())) reqVo.setSearchBoardType("1");
			
			//CODE  - 자주하는질문: C013 , 다운로드 : C014 , 채용공고 : C015 , 새소식 : C016
			/*
			[CODE] [VALUE] [NAME]
			"C013"	"1"	"자동차 IoT제품"
			"C013"	"2"	"solution"
			"C013"	"3"	"service"
			"C014"	"1"	"자료"
			"C014"	"2"	"카탈로그"
			"C015"	"1"	"마케팅"
			"C016"	"1"	"NEWS"
			"C016"	"2"	"보도자료"
			*/
			//reqVo.setSearchContentType("")
			PageNavigator pageNavi = makePageNavigator(request);
			int totalCnt = boardService.getBoardListCnt(reqVo);
			pageNavi.setTotalSize(totalCnt);
			reqVo.setPageNavi(pageNavi);

			model.addAttribute("pageNavi", pageNavi);

			List<BoardVO> rtvList = boardService.getBoardList(reqVo);

			model.addAttribute("rtvList", rtvList);
 
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/cs/inquiry.do")
	public String inquiry(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/cs/inquiry";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/cs/inquiryReceipt.do")
	public String inquiryReceipt(HttpServletRequest request,
    		ModelMap model, @ModelAttribute CSVO reqVo) throws Exception  {
		
		String rtvPage = "/cs/inquiryReceipt";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/cs/inquiryReceiptFinish.do")
	public String inquiryReceiptFinish(HttpServletRequest request,
    		ModelMap model, @ModelAttribute CSVO reqVo) throws Exception  {
		
		String rtvPage = "/cs/inquiryReceiptFinish";
		
		try {
			File[] fList = null;
			
			if(!StrUtil.isNullToEmpty(reqVo.getFile())){
				String[] idList = reqVo.getFile().split(",");
				fList = new File[idList.length];
				for(int i = 0 ; i < idList.length ; i++){
					String uuid = StrUtil.nvl(idList[i]);
					FileVO file = commonService.getFile(uuid);
					
					if(file == null){
						
					}else{
						String imgPath = "";
						imgPath = file.getFilePath()+System.getProperty("file.separator")+file.getSaveFileNm();
						
						try {		
							File img = new File(imgPath);
							fList[i] = img;
							
						} catch (Exception e) {
							
						}
					}
				}
			}
			
			String os = System.getProperty("os.name");
			if(os.equals("Windows 7")){
				MailUtil mail = new MailUtil();
				String from = reqVo.getEmail();
				String fromName = reqVo.getName();
				String[] to = {"upjjw2682@naver.com"};
				String[] cc = {};
				String[] bcc = {};
				String subject = reqVo.getTitle();
				String content = reqVo.getContents();
				File[] attachFiles = fList;
				mail.sendSmtpMail(from, fromName, to, cc, bcc, subject, content, attachFiles);
			}else{
				MailUtilServer mail = new MailUtilServer();
				String from = reqVo.getEmail();
				String fromName = reqVo.getName();
				String[] to = {Globals.MAIL_SENDER};
				String[] cc = {};
				String[] bcc = {};
				String subject = reqVo.getTitle();
				String content = reqVo.getContents();
				File[] attachFiles = fList;
				mail.sendSmtpMail(from, fromName, to, cc, bcc, subject, content, attachFiles);
			}
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/cs/download.do")
	public String download(HttpServletRequest request,
    		ModelMap model, @ModelAttribute BoardVO reqVo) throws Exception  {
		
		String rtvPage = "/cs/download";
		
		try {
			//1:자주하는질문 , 2:다운로드 , 3:채용공고 , 4:경영진의메세지 , 5 :새소식			
			if(StrUtil.isNullToEmpty(reqVo.getSearchBoardType())) reqVo.setSearchBoardType("2");
			
			//CODE  - 자주하는질문: C013 , 다운로드 : C014 , 채용공고 : C015 , 새소식 : C016
			/*
			[CODE] [VALUE] [NAME]
			"C013"	"1"	"자동차 IoT제품"
			"C013"	"2"	"solution"
			"C013"	"3"	"service"
			"C014"	"1"	"자료"
			"C014"	"2"	"카탈로그"
			"C015"	"1"	"마케팅"
			"C016"	"1"	"NEWS"
			"C016"	"2"	"보도자료"
			*/
			//reqVo.setSearchContentType("")
			PageNavigator pageNavi = makePageNavigator(request);
			int totalCnt = boardService.getBoardListCnt(reqVo);
			pageNavi.setTotalSize(totalCnt);
			reqVo.setPageNavi(pageNavi);

			model.addAttribute("pageNavi", pageNavi);

			List<BoardVO> rtvList = boardService.getBoardList(reqVo);

			model.addAttribute("rtvList", rtvList);
 
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
}
