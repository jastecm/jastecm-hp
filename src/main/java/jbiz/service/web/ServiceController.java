package jbiz.service.web;

import java.io.File;
import java.io.FileInputStream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jbiz.com.service.CommonService;
import jbiz.vo.ParamVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.common.BasicController;
import com.common.MailUtil;
import com.common.MailUtilServer;


/*
 * @Class name : MainController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Controller
public class ServiceController extends BasicController{
		
	private Log logger = LogFactory.getLog(getClass());	
	
	@Resource(name = "commonService")
	private CommonService commonService;
	
	@RequestMapping(value="/service/viewcar.do")
	public String viewcar(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/service/viewcar";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/service/vdas.do")
	public String vdas(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/service/vdas";
		
		try {
			
	        model.addAttribute("reqVo", reqVo);
	        
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/service/clinic.do")
	public String clinic(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/service/clinic";
		
		try {
			
	        model.addAttribute("reqVo", reqVo);
	        
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/service/carCompatibility.do")
	public String carCompatibility(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/service/carCompatibility";
		
		try {
			
	        model.addAttribute("reqVo", reqVo);
	        
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/service/carCompatibilityFrame.do")
	public String carCompatibilityFrame(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/service/carCompatibilityFrame";
		
		try {
			
	        model.addAttribute("reqVo", reqVo);
	        
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/service/carCompatibilityFrame_keco.do")
	public String carCompatibilityFrame_keco(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/service/carCompatibilityFrameDown";
		
		try {
			
	        model.addAttribute("reqVo", reqVo);
	        
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/common/getVehicleLevSearch.do")
	public String getVehicleLevSearch(HttpServletRequest request,HttpServletResponse response,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "jsonView";
		response.setContentType("application/x-json; charset=UTF-8");
		
		try {
			
			if(reqVo.getLev().equals("2")) model.addAttribute("rtvList",commonService.getManufacture(reqVo));
			else if(reqVo.getLev().equals("3")) model.addAttribute("rtvList",commonService.getModelMaster(reqVo));
			else if(reqVo.getLev().equals("4")) model.addAttribute("rtvList",commonService.getYear(reqVo));
			else if(reqVo.getLev().equals("5")) model.addAttribute("rtvList",commonService.getModelHeader(reqVo));
			else if(reqVo.getLev().equals("6")) model.addAttribute("rtvList",commonService.getFuel(reqVo));
			else if(reqVo.getLev().equals("7")) model.addAttribute("rtvList",commonService.getVolume(reqVo));
	        
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/common/getBCMsurport.do")
	public String getBCMsurport(HttpServletRequest request,HttpServletResponse response,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "jsonView";
		response.setContentType("application/x-json; charset=UTF-8");
		
		try {
			
			model.addAttribute("data",commonService.getBCMsurport(reqVo));
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/service/estimateSheet.do")
	public String estimateSheet(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/service/estimateSheet";
		
		try {
			
	        model.addAttribute("reqVo", reqVo);
	        
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/service/estimateSheetSubmit.do")
	public String estimateSheetSubmit(HttpServletRequest request,HttpServletResponse response,
			ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "jsonView";
		response.setContentType("application/x-json; charset=UTF-8");
		
		try {
			
			String device = request.getParameter("model");
			String vdas = request.getParameter("vdas");
			String corp = request.getParameter("corp");
			String name = request.getParameter("name");
			String email = request.getParameter("mail");
			
			File f = commonService.exportEstimateSheet(device,vdas,corp,name,email);
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			String os = System.getProperty("os.name");
			boolean dev = os.equals("Windows 7");
			
			String subject = "ViewCAR 견적서_"+f.getName();
			String content = "";
			StringBuffer sb = new StringBuffer();
			
			
			sb.append("안녕하세요 뷰카입니다.");
			sb.append("<br/>요청하신 견적서 파일을 첨부하였습니다.");
			sb.append("<br/>감사합니다.");
			sb.append("<br/><br/>");
			sb.append("<a href='http://viewcar.net'>http://viewcar.net</a>");
			content = sb.toString();
			
			String[] to = { email,"james.jang@jastecm.com" };
			String[] cc = {};
			String[] bcc = {};
			File[] attachFiles = { f };
			
			if (dev) {
				MailUtil mail = new MailUtil();
				String from = "upjjw2682@gmail.com";
				String fromName = "정종웅";
				mail.sendSmtpMail(from, fromName, to, cc, bcc, subject,
						content, attachFiles);
			} else {
				MailUtilServer mail = new MailUtilServer();
				String from = "james.jang@jastecm.com";
				String fromName = "뷰카";
				mail.sendSmtpMail(from, fromName, to, cc, bcc, subject,
						content, attachFiles);
			}
			
			model.addAttribute("rtvCd", "1");
			
		} catch (Exception e) {
			model.addAttribute("rtvCd", "0");
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
}

