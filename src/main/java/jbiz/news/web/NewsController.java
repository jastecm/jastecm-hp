package jbiz.news.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.common.BasicController;
import com.common.PageNavigator;
import com.util.string.StrUtil;

import jbiz.board.service.BoardService;
import jbiz.vo.BoardVO;
import jbiz.vo.ParamVO;

@Controller
public class NewsController extends BasicController{
	
	private Log logger = LogFactory.getLog(getClass());	
	
	@Autowired
	private BoardService boardService;
	
	
	
	@RequestMapping(value="/news/news.do")
	public String news(HttpServletRequest request,
    		ModelMap model, @ModelAttribute BoardVO reqVo) throws Exception  {
		
		String rtvPage = "/news/news";
		
		try {  
			//1:자주하는질문 , 2:다운로드 , 3:채용공고 , 4:경영진의메세지 , 5 :새소식			
			if(StrUtil.isNullToEmpty(reqVo.getSearchBoardType())) reqVo.setSearchBoardType("5");
			
			//CODE  - 자주하는질문: C013 , 다운로드 : C014 , 채용공고 : C015 , 새소식 : C016
			/*
			[CODE] [VALUE] [NAME]
			"C013"	"1"	"자동차 IoT제품"
			"C013"	"2"	"solution"
			"C013"	"3"	"service"
			"C014"	"1"	"자료"
			"C014"	"2"	"카탈로그"
			"C015"	"1"	"마케팅"
			"C016"	"1"	"NEWS"
			"C016"	"2"	"보도자료"
			*/
			//reqVo.setSearchContentType("")
			PageNavigator pageNavi = makePageNavigator(request);

			int totalCnt = boardService.getBoardListCnt(reqVo);
			pageNavi.setTotalSize(totalCnt);
			reqVo.setPageNavi(pageNavi);

			model.addAttribute("pageNavi", pageNavi);

			List<BoardVO> rtvList = boardService.getBoardList(reqVo);

			model.addAttribute("rtvList", rtvList);
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	
	
}
