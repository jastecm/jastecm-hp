package jbiz.corporate.web;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.common.BasicController;
import com.common.Globals;
import com.common.MailUtil;
import com.common.MailUtilServer;
import com.util.string.StrUtil;

import jbiz.com.service.CommonService;
import jbiz.vo.CorporateVO;
import jbiz.vo.FileVO;
import jbiz.vo.ParamVO;


/*
 * @Class name : MainController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Controller
public class CorporateController extends BasicController{
		
	private Log logger = LogFactory.getLog(getClass());	
	
	@Autowired
	private CommonService commonService;
	
	@RequestMapping(value="/corporate/vision.do")
	public String vision(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/corporate/vision";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
	
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/corporate/history.do")
	public String history(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/corporate/history";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/corporate/ceomsg.do")
	public String ceomsg(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/corporate/ceomsg";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/corporate/business.do")
	public String business(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/corporate/business";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/corporate/info.do")
	public String info(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/corporate/info";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/corporate/tech.do")
	public String tech(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/corporate/tech";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/corporate/partner.do")
	public String partner(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/corporate/partner";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/corporate/partnership.do")
	public String partnership(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/corporate/partnership";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/corporate/ReceiptFinish.do")
	public String ReceiptFinish(HttpServletRequest request,
    		ModelMap model, @ModelAttribute CorporateVO reqVo ) throws Exception  {
		
		String rtvPage = "/corporate/ReceiptFinish";
		
		try {
			File[] fList = null;
			
			
			
			if(!StrUtil.isNullToEmpty(reqVo.getFile())){
				String[] idList = reqVo.getFile().split(",");
				fList = new File[idList.length];
				for(int i = 0 ; i < idList.length ; i++){
					String uuid = StrUtil.nvl(idList[i]);
					FileVO file = commonService.getFile(uuid);
					
					if(file == null){
						
					}else{
						String imgPath = "";
						imgPath = file.getFilePath()+System.getProperty("file.separator")+file.getSaveFileNm();
						
						try {		
							File img = new File(imgPath);
							fList[i] = img;
							
						} catch (Exception e) {
							
						}
					}
				}
			}
			
			String html = reqVo.getContents();
			
			html += "<br/>";
			html += "회사명 : "+reqVo.getCorpo();
			html += "<br/>";
			html += "제안자명 : "+reqVo.getPerson();
			html += "<br/>";
			html += "제안자 구분 : "+reqVo.getPersonType();
			html += "<br/>";
			html += "전화번호 : "+reqVo.getPhone();
			html += "<br/>";
			html += "이메일주소 : "+reqVo.getEmail();
		
			String os = System.getProperty("os.name");
			if(os.equals("Windows 7")){
				MailUtil mail = new MailUtil();
				String from = reqVo.getEmail();
				String fromName = reqVo.getPerson();
				String[] to = {"upjjw2682@naver.com"};
				String[] cc = {};
				String[] bcc = {};
				String subject = reqVo.getName();
				String content = html;
				File[] attachFiles = fList;
				mail.sendSmtpMail(from, fromName, to, cc, bcc, subject, content, attachFiles);
			}else{
				MailUtilServer mail = new MailUtilServer();
				String from = reqVo.getEmail();
				String fromName = reqVo.getPerson();
				String[] to = {Globals.MAIL_SENDER};
				String[] cc = {};
				String[] bcc = {};
				String subject = reqVo.getName();
				String content = html;
				File[] attachFiles = fList;
				mail.sendSmtpMail(from, fromName, to, cc, bcc, subject, content, attachFiles);
			}
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/corporate/partnershipReceipt.do")
	public String partnershipReceipt(HttpServletRequest request,
			ModelMap model, @ModelAttribute CorporateVO reqVo ) throws Exception  {
		
		String rtvPage = "/corporate/partnershipReceipt";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	
}

