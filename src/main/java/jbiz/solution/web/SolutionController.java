package jbiz.solution.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.common.BasicController;

import jbiz.vo.ParamVO;

@Controller
public class SolutionController extends BasicController{
private Log logger = LogFactory.getLog(getClass());	
	
	
	@RequestMapping(value="/solution/info.do")
	public String info(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/solution/info";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/solution/apps.do")
	public String apps(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/solution/apps";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/solution/clinic.do")
	public String clinic(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/solution/clinic";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/solution/vdas.do")
	public String vdas(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/solution/vdas";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/solution/share.do")
	public String share(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/solution/share";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/solution/scoring.do")
	public String scoring(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/solution/scoring";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/solution/call.do")
	public String call(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/solution/call";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/solution/comm.do")
	public String comm(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/solution/comm";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
}
