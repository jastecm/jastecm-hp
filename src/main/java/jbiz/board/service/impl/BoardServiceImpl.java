package jbiz.board.service.impl;

import java.util.List;

import javax.annotation.Resource;

import jbiz.board.dao.BoardDAO;
import jbiz.board.service.BoardService;
import jbiz.vo.BoardVO;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("boardService")
@Transactional
public class BoardServiceImpl implements BoardService {
	@Resource(name = "boardDao")
	private BoardDAO dao;
	
	public int getBoardListCnt(BoardVO vo) throws Exception{
		return dao.getBoardListCnt(vo);
	}
	public List<BoardVO> getBoardList(BoardVO vo) throws Exception{
		return dao.getBoardList(vo);
	}
	
	
	
}

